
QUnit.test('[Planning page][model][getters] Test init model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        assert.equal(model.getCategoryCount(),0,"There should be no categories at initialization.");
        assert.deepEqual(model.getTable(), {}, "Table should be empty at initialization.");
        assert.deepEqual(model.getRemainingCurrencyOptions(), [], "There should be no currency at init.")
    }
);

QUnit.test('[Planning page][model][addCategory] Test added catgegory model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        model.addCategory('CAT');
        assert.equal(model.getCategoryCount(),1,"There should be one categories.")
    }
);

QUnit.test('[Planning page][model][addCurrency] Test added currency model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        model.addCurrencyOption('HUF');
        assert.deepEqual(model.getRemainingCurrencyOptions(),['HUF']);
    }
);

QUnit.test('[Planning page][model][removeCurrency] Test add then remove currency model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        model.addCurrencyOption('HUF');
        model.removeCurrencyOption('HUF');
        assert.deepEqual(model.getRemainingCurrencyOptions(),[]);

        model.addCurrencyOption('USD');
        model.removeCurrencyOption('HUF');
        assert.deepEqual(model.getRemainingCurrencyOptions(),['USD']);
    }
);

QUnit.test('[Planning page][model][updateTableFromView] Test view update model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        model.updateTableFromView('CAT', 'HUF:1200,EUR:50', '');
        assert.deepEqual(model.getTable(),{
            'CAT': {
                'Expenditure':{
                    'HUF':1200,
                    'EUR':50
                },
                'Income':{}
            }
        });

        model.updateTableFromView('CAT', 'HUF:1200', 'EUR:50');
        assert.deepEqual(model.getTable(),{
            'CAT': {
                'Expenditure':{ 'HUF':1200 },
                'Income':{ 'EUR':50 }
            }
        });
    }
);

QUnit.test('[Planning page][model][clearTable] Test cleared table model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        model.updateTableFromView('CAT', 'HUF:1200,EUR:50', '');
        model.clearTable();
        assert.deepEqual(model.getTable(),{});
    }
);

QUnit.test('[Planning page][model][updateTableData] Test update from persistence model',
    assert =>{
        const persistence = new PlanningPersistenceMock();
        const model = new PlanningModel(2022, 5);
        model.setPersistence(persistence);
        persistence.setResultTable(
            {
                'CAT': {
                    'Expenditure':{ 'HUF':1200 },
                    'Income':{ 'EUR':50 }
                },
                'CAT1': {
                    'Expenditure':{ 'HUF':200, 'EUR':10 },
                    'Income':{ 'EUR':50 }
                }
            }
        );
        assert.deepEqual(model.getTable(), {});
        document.addEventListener('update', ()=>{
            assert.deepEqual(model.getTable(),{
                'CAT': {
                    'Expenditure':{ 'HUF':1200 },
                    'Income':{ 'EUR':50 }
                },
                'CAT1': {
                    'Expenditure':{ 'HUF':200, 'EUR':10 },
                    'Income':{ 'EUR':50 }
                }
            }, "Table after updated");
        });
        model.updateTableData();
    }
);