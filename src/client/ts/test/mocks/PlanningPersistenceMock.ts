
class PlanningPersistenceMock extends PlanningPersistence {
    private tableRes: {[index:string]:{[index:string]:{[index:string]:number}}} = {};
    private numRes: Number = 0;
    public setResultTable(value: {[index:string]:{[index:string]:{[index:string]:number}}}): void {
        this.tableRes = value;
    }

    public setResultNumber(value: Number): void {
        this.numRes = value;
    }

    public async fetchTable(year: number, month: number): Promise<{[index:string]:{[index:string]:{[index:string]:number}}}> {
        let m = this;
        return Promise.resolve(m.tableRes);
    }

    public async saveTable(table: {[index:string]:{[index:string]:{[index:string]:number}}}, year: number, month: number): Promise<Number> {
        let m = this;
        return Promise.resolve(m.numRes);
    }
}