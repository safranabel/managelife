class PlanningView {
    private model:PlanningModel;
    private addCurrencyBtn:HTMLButtonElement;
    private addCategoryBtn:HTMLButtonElement;
    private table:HTMLTableElement;
    private currencySelect:HTMLSelectElement;
    private saveBtn:HTMLButtonElement;
    private saveMsg:HTMLSpanElement;

    constructor(){
        let year = Number((document.querySelector("#year")! as HTMLSpanElement).textContent);
        let month = Number((document.querySelector("#month")! as HTMLSpanElement).textContent);
        this.model = new PlanningModel(year, month);
        this.addCurrencyBtn = document.querySelector("#addCurrencyBtn")! as HTMLButtonElement;
        this.addCategoryBtn = document.querySelector("#addCategoryBtn")! as HTMLButtonElement;
        this.table = document.querySelector("#plannerTable")! as HTMLTableElement;
        this.currencySelect = document.querySelector("#currencySelect")! as HTMLSelectElement;
        this.saveBtn = document.querySelector("#saveBtn")! as HTMLButtonElement;
        this.saveMsg = document.querySelector("#saveMsg")! as HTMLDivElement;

        this.addCurrencyBtn.addEventListener('click', this.addSelectedCurrency);
        this.addCategoryBtn.addEventListener('click', this.addCategory);
        this.saveBtn.addEventListener('click', this.saveTable);
        document.addEventListener('update', this.updateViewTable);
        document.addEventListener('savesuccess', this.saved);
        document.addEventListener('saveerror1', this.saveError1);
        document.addEventListener('saveerror2', this.saveError2);
        document.addEventListener('saveerror3', this.saveError3);
        document.addEventListener('saveerror4', this.saveError4);
        document.addEventListener('saveerroru', this.saveErrorUnknown);
        document.querySelectorAll('#plannerTable td input').forEach(e => e.addEventListener("change", this.tableInputChanged));


        for(let i = 0; i < this.currencySelect.options.length; i++) {
            let opt = this.currencySelect.options[i].value;
            this.model.addCurrencyOption(opt);
        }

        for(let i = 0; i < this.table.rows.length-1; i+=2) {
            this.model.addCategory(this.table.rows[i].firstElementChild?.innerHTML ! as String);
        }
    }

    public init(): void {
        this.model.updateTableData();
        this.updateViewTable();
    }

    public updateViewTable(): void {
        let t = planningView.model.getTable();
        let keys = Object.keys(t);
        planningView.table.innerHTML = "";
        let newTable = "";
        for (let i = 0; i < keys.length; i++) {
            let inc = t[keys[i]]["Income"];
            let inKeys = Object.keys(inc);
            let exp = t[keys[i]]["Expenditure"];
            let exKeys = Object.keys(exp);
            let allKeys:string[] = [];
            allKeys = Array.from(new Set(allKeys.concat(exKeys).concat(inKeys))).sort();
            newTable += '<tr>';
            newTable += '<td rowspan="2">' + keys[i] + '</td>';
            newTable += '<td>Income</td>';
            for(let j = 0; j < allKeys.length; j++) {
                let tmpCur = allKeys[j];
                let tmpVal = 0;
                if(inKeys.includes(tmpCur)) {
                    tmpVal = inc[tmpCur];
                }
                newTable += '<td>';
                newTable += '<input type="number" placeholder="Expected amount" value="' + tmpVal + '">';
                newTable += '<span>' + tmpCur + '</span>';
                newTable += '</td>';
            }
            if(i == 0) {
                let remcur = planningView.model.getRemainingCurrencyOptions();
                if(remcur.length > 0) {
                    newTable += '<td rowspan="100%">Add new currency: ';
                    newTable += '<select name="" id="currencySelect" class="me-2">';
                    for(let i in remcur) {
                        newTable += '<option value="'+remcur[i]+'">'+remcur[i]+'</option>';
                    }
                    newTable += '</select><button id="addCurrencyBtn" title="Add selected currency" class="btn btn-primary">+</button></td>';
                }
            }
            newTable += '</tr>';
            newTable += '<tr>';
            newTable += '<td>Expenditure</td>';
            for(let j = 0; j < allKeys.length; j++) {
                let tmpCur = allKeys[j];
                let tmpVal = 0;
                if(exKeys.includes(tmpCur)) {
                    tmpVal = exp[tmpCur];
                }
                newTable += '<td>';
                newTable += '<input type="number" placeholder="Expected amount" value="' + tmpVal + '">';
                newTable += '<span>' + tmpCur + '</span>';
                newTable += '</td>';
            }
            newTable += '</tr>';
        }
        newTable += '<tr><td colspan="100%">';
        newTable += 'Add new category: <button id="addCategoryBtn" title="Add new category" class="btn btn-primary">+</button>';
        newTable += '</td></tr>';

        planningView.table.innerHTML = newTable;

        if(document.querySelectorAll("#addCurrencyBtn").length > 0) {
            planningView.addCurrencyBtn = document.querySelector("#addCurrencyBtn")! as HTMLButtonElement;
            planningView.addCurrencyBtn.addEventListener('click', planningView.addSelectedCurrency);
            planningView.currencySelect = document.querySelector("#currencySelect")! as HTMLSelectElement;
        }
        planningView.addCategoryBtn = document.querySelector("#addCategoryBtn")! as HTMLButtonElement;
        planningView.addCategoryBtn.addEventListener('click', planningView.addCategory);
        document.querySelectorAll('#plannerTable td input').forEach(e => e.addEventListener("change", planningView.tableInputChanged));
    }

    private saveTable(e: Event): void {
        e.preventDefault();
        planningView.saveMsg.innerHTML =  '<div class="spinner-border" role="status">'
        planningView.saveMsg.innerHTML += '<span class="visually-hidden">Loading...</span>'
        planningView.saveMsg.innerHTML += '</div>';
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.classList.remove('bg-danger-light');
        planningView.updateModel();
        planningView.model.saveTable();
    }

    private addSelectedCurrency(e: Event): void {
        e.preventDefault();
        let selIndex = planningView.currencySelect.options.selectedIndex
        let option = planningView.currencySelect.options.item(selIndex) ! as HTMLOptionElement;
        let selectedCurrency = option.value;
        planningView.model.removeCurrencyOption(selectedCurrency);
        for(let i=0;i < planningView.table.rows.length-1; i++) {
            let row = planningView.table.rows[i];
            if(i == 0) {
                row.removeChild(row.cells[row.cells.length-1]);
            }
            let newCell = '<td><input type="number" name="" id="" placeholder="Expected amount" value="0">';
            newCell += '<span>'+selectedCurrency+'</span></td>';
            row.innerHTML += newCell;
        }
        let remcur = planningView.model.getRemainingCurrencyOptions();
        if(remcur.length > 0) {
            let addCell = '<td rowspan="100%">Add new currency: ';
            addCell += '<select name="" id="currencySelect" class="me-2">';
            for(let i in remcur) {
                addCell += '<option value="'+remcur[i]+'">'+remcur[i]+'</option>';
            }

            addCell += '</select><button id="addCurrencyBtn" title="Add selected currency" class="btn btn-primary">+</button></td>';
            planningView.table.rows[0].innerHTML += addCell;
            planningView.addCurrencyBtn = document.querySelector("#addCurrencyBtn")! as HTMLButtonElement;
            planningView.addCurrencyBtn.addEventListener('click', planningView.addSelectedCurrency);
            planningView.currencySelect = document.querySelector("#currencySelect")! as HTMLSelectElement;
            document.querySelectorAll('#plannerTable td input').forEach(e => e.addEventListener("change", planningView.tableInputChanged));
        }
    }

    private changeValueToZero(input: string): string {
        let txt = input;
        for(let i = 0; i < txt.length-8; i++) {
            if(txt.substring(i, i+7) == 'value="') {
                let val = "";
                let j = i+7;
                while(j < txt.length && txt[j] != '"') {
                    val += txt[j];
                    j++;
                }
                if(txt[j] == '"') {
                    txt = txt.replace('value="' + val + '"', 'value="0"');
                }
            }
        }
        return txt;
    }

    private addCategory(e: Event): void {
        e.preventDefault();
        let inTxt = "";
        if(planningView.table.rows.length > 1) {
            inTxt = '<tr><td rowspan="2"><input type="text"></td>';
            for(let i = 1; i < planningView.table.rows[0].cells.length - document.querySelectorAll("#addCurrencyBtn").length; i++) {
                let c = planningView.table.rows[0].cells[i];
                inTxt += '<td>' + planningView.changeValueToZero(c.innerHTML) + '</td>';
            }
            inTxt += '</tr><tr>';
            for(let i = 0; i < planningView.table.rows[1].cells.length; i++) {
                let c = planningView.table.rows[1].cells[i];
                inTxt += '<td>' + planningView.changeValueToZero(c.innerHTML) + '</td>';
            }
            inTxt += '</tr>';
        } else {
            inTxt += '<tr><td rowspan="2"><input type="text"></td><td>Income</td>';
            let remcur = planningView.model.getRemainingCurrencyOptions();
            if(remcur.length > 0) {
                inTxt += '<td rowspan="100%">Add new currency: ';
                inTxt += '<select name="" id="currencySelect" class="me-2">';
                for(let i in remcur) {
                    inTxt += '<option value="'+remcur[i]+'">'+remcur[i]+'</option>';
                }
                inTxt += '</select><button id="addCurrencyBtn" title="Add selected currency" class="btn btn-primary">+</button></td>';
            }
            inTxt += '</tr>';
            inTxt += '<tr><td>Expenditure</td></tr>';
        }
        inTxt += '<tr><td colspan="100%">Add new category: <button id="addCategoryBtn" title="Add new category" class="btn btn-primary">+</button></td></tr>';
        planningView.table.deleteRow(planningView.table.rows.length - 1);
        let tbody = planningView.table.firstElementChild! as HTMLElement;
        tbody.innerHTML += inTxt;

        planningView.model.addCategory("");

        if(document.querySelectorAll("#addCurrencyBtn").length > 0) {
            planningView.addCurrencyBtn = document.querySelector("#addCurrencyBtn")! as HTMLButtonElement;
            planningView.addCurrencyBtn.addEventListener('click', planningView.addSelectedCurrency);
            planningView.currencySelect = document.querySelector("#currencySelect")! as HTMLSelectElement;
        }
        planningView.addCategoryBtn = document.querySelector("#addCategoryBtn")! as HTMLButtonElement;
        planningView.addCategoryBtn.addEventListener('click', planningView.addCategory);
        document.querySelectorAll('#plannerTable td input').forEach(e => e.addEventListener("change", planningView.tableInputChanged));
    }

    private tableInputChanged(e: Event): void {
        planningView.updateModel();
        (e.target ! as HTMLInputElement).setAttribute("value", (e.target ! as HTMLInputElement).value);
    }

    private updateModel(): void {
        this.model.clearTable();
        let rs = planningView.table.rows;
        for(let i = 0; i < rs.length-1; i+=2) {
            let cat = "";
            if(rs[i].firstElementChild?.firstElementChild != null) {
                cat = (rs[i].firstElementChild?.firstElementChild! as HTMLInputElement).value;
            } else {
                cat = rs[i].firstElementChild?.innerHTML ! as string;
            }
            let inc = "";
            let j = 2;
            while(j < rs[i].cells.length && rs[i].cells[j].id != "currencySelectTD") {
                inc += rs[i].cells[j].lastElementChild?.innerHTML + ":";
                let tmp = (rs[i].cells[j].firstElementChild! as HTMLInputElement).value;
                if(tmp == null || tmp == "") {
                    tmp = "0";
                }
                inc += tmp + ",";
                j++;
            }
            let exp = "";
            let k = 1;
            while(k < rs[i+1].cells.length && rs[i+1].cells[k].id != "currencySelectTD") {
                exp += rs[i+1].cells[k].lastElementChild?.innerHTML + ":";
                let tmp = (rs[i+1].cells[k].firstElementChild! as HTMLInputElement).value;
                if(tmp == null || tmp == "") {
                    tmp = "0";
                }
                exp += tmp + ",";
                k++;
            }
            planningView.model.updateTableFromView(cat, exp, inc);
        }
    }

    private saved(e: Event): void {
        planningView.saveMsg.classList.remove('bg-danger-light');
        planningView.saveMsg.innerHTML = "Plans saved successfully";
        planningView.saveMsg.classList.add('bg-success-light');
        planningView.saveMsg.classList.add('text-dark');
        planningView.init();
    }

    private saveError1(e: Event): void {
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.innerHTML = "Error: Connection error, saving failed - check your connection and try again";
        planningView.saveMsg.classList.add('bg-danger-light');
        planningView.saveMsg.classList.add('text-dark');
    }

    private saveError2(e: Event): void {
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.innerHTML = "Error: Invalid category name, saving failed - check your category names (length of a category name should be between 3-99 character and should contain only letters)";
        planningView.saveMsg.classList.add('bg-danger-light');
        planningView.saveMsg.classList.add('text-dark');
    }

    private saveError3(e: Event): void {
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.innerHTML = "Error: Invalid currency, saving failed - Check your currencies";
        planningView.saveMsg.classList.add('bg-danger-light');
        planningView.saveMsg.classList.add('text-dark');
    }

    private saveError4(e: Event): void {
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.innerHTML = "Error: Database error, saving failed - Check your connection and try again later";
        planningView.saveMsg.classList.add('bg-danger-light');
        planningView.saveMsg.classList.add('text-dark');
    }

    private saveErrorUnknown(e: Event): void {
        planningView.saveMsg.classList.remove('bg-success-light');
        planningView.saveMsg.innerHTML = "Error: Unknown error, saving failed - Try again later";
        planningView.saveMsg.classList.add('bg-danger-light');
        planningView.saveMsg.classList.add('text-dark');
    }

}