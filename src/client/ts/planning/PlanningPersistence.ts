class PlanningPersistence {
    /**
     * fetchTable
     * @param year
     * @param month
     * @returns the faetched table from the server: Promise<{[index:string]:{[index:string]:{[index:string]:number}}}>
     */
    public async fetchTable(year: number, month: number): Promise<{[index:string]:{[index:string]:{[index:string]:number}}}> {
        let res = {};
        let response = await fetch('./controllers/manageplans.php?type=fetch&year=' + year.toString() + '&month=' + month.toString());
        res = await response.json();
        return res;
    }

    /**
     * saveTable
     * This function sends the plans to the server for saving.
     *
     * @param table
     * @param year
     * @param month
     * @returns the response number from the server: Promise<Number>
     */
    public async saveTable(table: {[index:string]:{[index:string]:{[index:string]:number}}}, year: number, month: number): Promise<Number> {
        let response = await fetch(
            './controllers/manageplans.php?type=save&year=' + year.toString() + '&month=' + month.toString(), {
                method: "POST",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'pplication/x-www-form-urlencoded; charset=UTF-8'
                },
                body: JSON.stringify(table)
            }
        );
        let res = await response.text();
        return new Number(res);
    }
}