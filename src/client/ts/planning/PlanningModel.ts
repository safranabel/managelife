const updateEvent = new Event('update');
const savingSuccess = new Event('savesuccess');
const savingFail1 = new Event('saveerror1');
const savingFail2 = new Event('saveerror2');
const savingFail3 = new Event('saveerror3');
const savingFail4 = new Event('saveerror4');
const savingFailUnknown = new Event('saveerroru');
class PlanningModel{
    private remainingCurrencyOptions:String[] = [];
    private categories:String[] = [];
    private table:{[index:string]:{[index:string]:{[index:string]:number}}} = {};
    private persistence:PlanningPersistence;
    private year:number;
    private month:number;
    /**
     * constructor
     *
     * @param year
     * @param month
     */
    public constructor(year: number, month: number) {
        this.persistence = new PlanningPersistence();
        this.year = year;
        this.month = month;
    }

    /**
     * setPersistence
     * This function sets new persistence to the object.
     *
     * @param pers
     * @returns void
     */
    public setPersistence(pers: PlanningPersistence): void {
        this.persistence = pers;
    }

    /**
     * clearTable
     * This function clears the plan table.
     *
     * @returns void
     */
    public clearTable(): void {
        this.table = {};
    }

    /**
     * updateTableFromView
     * This function updates the plan table with data from the user.
     *
     * @param category
     * @param exp
     * @param inc
     * @returns void
     */
    public updateTableFromView(category: string, exp: string, inc: string): void {
        if(category == null || category == "") {
            return;
        }
        this.table[category] = {"Expenditure":{}, "Income":{}};
        let e = exp.split(',');
        for(let i = 0; i < e.length; i++) {
            let tmp = e[i].split(':');
            let key = tmp[0];
            let value = Number(tmp[1]);
            if(0 <= value) {
                this.table[category]["Expenditure"][key] = value;
            }
        }
        let i1 = inc.split(',');
        for(let i = 0; i < i1.length; i++) {
            let tmp = i1[i].split(':');
            let key = tmp[0];
            let value = Number(tmp[1]);
            if(0 <= value) {
                this.table[category]["Income"][key] = value;
            }
        }
    }

    /**
     * removeCurrencyOption
     * This function removes the given currency from the remaining currency options.
     *
     * @param opt
     * @returns void
     */
    public removeCurrencyOption(opt:String): void {
        const index = this.remainingCurrencyOptions.indexOf(opt);
        if(-1 < index){
            this.remainingCurrencyOptions.splice(index, 1);
        }
    }

    /**
     * addCurrencyOption
     * This function adds currency option to the remaining currency options.
     *
     * @param opt
     * @returns void
     */
    public addCurrencyOption(opt:String): void {
        this.remainingCurrencyOptions.push(opt);
        this.remainingCurrencyOptions.sort();
    }

    /**
     * getRemainingCurrencyOptions
     * @returns the remaining currency options: String[]
     */
    public getRemainingCurrencyOptions(): String[] {
        return this.remainingCurrencyOptions;
    }

    /**
     * getCategoryCount
     * @returns the number of categories: number
     */
    public getCategoryCount(): number {
        return this.categories.length;
    }

    /**
     * addCategory
     * This function adds a category to the categories.
     * @param cat
     * @returns void
     */
    public addCategory(cat:String): void {
        this.categories.push(cat);
    }

    /**
     * updateTableData
     * This function updates the plan table from the server.
     *
     * @returns void
     */
    public updateTableData(): void {
        let model = this;
        this.persistence.fetchTable(this.year, this.month).then((value) => {
            model.updateWithFetchedTable(value);
            let keys = Object.keys(this.table);
            for(let i = 0; i < keys.length; i++) {
                let inKeys = Object.keys(model.table[keys[i]]["Income"]);
                let exKeys = Object.keys(model.table[keys[i]]["Expenditure"]);
                let allKeys:string[] = [];
                allKeys = Array.from(new Set(allKeys.concat(exKeys).concat(inKeys))).sort();
                for(let j = 0; j < allKeys.length; j++) {
                    model.removeCurrencyOption(allKeys[j]);
                }
            }
            document.dispatchEvent(updateEvent);
        });
    }

    /**
     * updateWithFetchedTable
     * This function updates the plan table with the fetched data.
     *
     * @param newTable
     * @returns void
     */
    private updateWithFetchedTable(newTable: {[index:string]:{[index:string]:{[index:string]:number}}}): void {
        this.table = newTable;
        let keys = Object.keys(this.table);
        for(let i = 0; i < keys.length; i++) {
            let inKeys = Object.keys(this.table[keys[i]]["Income"]);
            let exKeys = Object.keys(this.table[keys[i]]["Expenditure"]);
            let allKeys:string[] = [];
            allKeys = Array.from(new Set(allKeys.concat(exKeys).concat(inKeys))).sort();
            for(let j = 0; j < allKeys.length; j++) {
                this.removeCurrencyOption(allKeys[j]);
            }
        }
    }

    /**
     * getTable
     * @returns the plan table: {[index:string]:{[index:string]:{[index:string]:number}}}
     */
    public getTable(): {[index:string]:{[index:string]:{[index:string]:number}}} {
        return this.table;
    }

    /**
     * saveTable
     * This function sends the new plans for saving.
     *
     * @returns void
     */
    public saveTable(): void {
        this.persistence.saveTable(this.table, this.year, this.month).then((response)=>{
            console.log(response);
            if(response == 0) {
                document.dispatchEvent(savingSuccess);
            } else if(response == 1) {
                document.dispatchEvent(savingFail1);
            } else if(response == 2) {
                document.dispatchEvent(savingFail2);
            } else if(response == 3) {
                document.dispatchEvent(savingFail3);
            } else if(response == 4) {
                document.dispatchEvent(savingFail4);
            } else {
                document.dispatchEvent(savingFailUnknown);
            }

        });
    }
}