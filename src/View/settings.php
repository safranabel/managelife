<?php

if (!isset($viewModel)) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="./assets/images/logo_01_bg_sqr.png" type="image/png">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">

    <title>Settings page at ManagE-Life</title>
</head>
<body>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded" <?= ($viewModel->getMessage() == '') ? "hidden" : ""?>>
        <?= $viewModel->getMessage() ?>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Change password</h1>
        <form action="./controllers/updatepass.php" method="post" class="border p-2 rounded">
            <label for="oldpass-in">Old password</label> <br>
            <input type="password" name="oldpass" id="oldpass-in" placeholder="enter your password"  required> <br>
            <label for="newpass-in">New password</label> <br>
            <input type="password" name="newpass" id="newpass-in" placeholder="enter your new password" 
            minlength="6" required>
            <br>
            <small>
                Password must contain at least one number, one letter and 6 characters.
            </small>
            <br>
            <label for="newpass-again">New password again</label> <br>
            <input type="password" name="passagain" id="newpass-again" placeholder="enter password again" 
            minlength="6" required>
            <br><br>
            <input type="submit" value="Change password" class="btn btn-primary">
        </form>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Notification settings</h1>
        <p>
            Here you can set up (daily/weekly/monthly) email notifications. 
            In the notifications, you can get information about your plans and finances.
        </p>
        <form action="./controllers/addnotification.php" method="post" class="border p-2 rounded">
            <label for="freqSelector">How often do you want to be notified?</label> <br>
            <select name="type" id="freqSelector">
                <option value="n">Never</option>
                <option value="m" <?= $viewModel->selectNotifOpt('monthly') ?>>Monthly</option>
                <option value="w" <?= $viewModel->selectNotifOpt('weekly') ?>>Weekly</option>
                <option value="d" <?= $viewModel->selectNotifOpt('daily') ?>>Daily</option>
            </select>
            <br><br>
            <input type="submit" value="Submit notification settings" class="btn btn-primary">
        </form>
    </article>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
