<?php

if (!isset($viewModel)) {
    die('Forbidden: check the url!');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">
    <link rel="shortcut icon" href="./assets/images/logo_01_bg.svg" type="image/x-icon">

    <title>Your time management page at ManagE-Life</title>
</head>
<body>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Time management</h1>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Your ToDo list</h1>
        <section>
            <?= $viewModel->getErrorList() ?>
        </section>
        <div class="table-responsive">
            <table id="plannerTable" class="table table-dark table-striped table-hover">
                <tr>
                    <th>Title</th>
                    <th>Longer description</th>
                    <th>Deadline - date</th>
                    <th>Deadline - time</th>
                    <th>.</th>
                </tr>
                <?= $viewModel->getToDoRows() ?>
                <tr>
                    <form action="./controllers/addtodo.php" method="post">
                        <td>
                            <input type="text" name="todotitle" id="" 
                            value="<?=$_GET['title'] ?? ""?>" required>
                        </td>
                        <td>
                            <input type="text" name="tododesc" id="" 
                            value="<?=$_GET['desc'] ?? ""?>">
                        </td>
                        <td>
                            <input type="date" name="tododate" id="transaction-date" 
                            value="<?=$_GET['date'] ?? $viewModel->getDate()?>" required>
                        </td>
                        <td>
                            <input type="time" name="todotime" id="transaction-time" 
                            value="<?=$_GET['time'] ?? '23:59'?>" required>
                        </td>
                        <td><input type="submit" value="Save" class="btn btn-primary"></td>
                    </form>
                </tr>
            </table>
        </div>
    </article>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
