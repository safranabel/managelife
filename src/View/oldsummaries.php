<?php

if (!isset($viewModel)) {
    die('Forbidden: check the url!');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">
    <link rel="shortcut icon" href="./assets/images/logo_01_bg.svg" type="image/x-icon">
    <title>Summaries page at ManagE-Life</title>
</head>
<body>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Summary</h1>
        <h2>
            Month: <?=$viewModel->getYear()?>-<?=$viewModel->getMonth()?>
        </h2>
        <span id="year" hidden><?=$viewModel->getYear()?></span>
        <span id="month" hidden><?=$viewModel->getMonth()?></span>
        <br>
        <div class="d-flex justify-content-between">
            <a href="oldsummaries.php?year=<?=$viewModel->getPrevYear()?>&month=<?=$viewModel->getPrevMonth()?>">
                Previous month
            </a>
            <a href="oldsummaries.php?year=<?=$viewModel->getNextYear()?>&month=<?=$viewModel->getNextMonth()?>">
                Next month
            </a>
        </div>
        <br>
        <div class="px-3">
            <h3>Monthly balance: <?=$viewModel->getMothlyBalance()?></h3>
        </div>
        <h4>Income</h4>
        <?= $viewModel->getIncomesInTable(); ?>
        <h4>Expenditure</h4>
        <?= $viewModel->getExpendituresInTable(); ?>
    </article>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
