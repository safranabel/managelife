<?php

if (!isset($viewModel) || !isset($fromIndexController) || !$fromIndexController) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="./assets/images/logo_01_bg_sqr.png" type="image/png">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">

    <title><?=$viewModel->getOwnerForm()?> index page at ManagE-Life</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <button
                class="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="navlink" href="planning.php">Financial Planning</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="time.php">My Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="health.php">My Health</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="settings.php">Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="navlink" href="./controllers/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Hello <?=$viewModel->getForename()?>!</h1>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 rounded">
        <h2>Register Transactions</h2>
        <?= $viewModel->successMessage() ?>
        <section>
            <?= $viewModel->getErrorList() ?>
        </section>
        <form action="./controllers/transaction.php" method="post" class="border p-2 rounded">
            <label for="transaction-date">Transaction date and time</label> <br>
            <input type="date" name="trandate" id="transaction-date" 
            value="<?=$_GET['date'] ?? $viewModel->getDate()?>" required>
            <input type="time" name="trantime" id="transaction-time" 
            value="<?=$_GET['time'] ?? $viewModel->getTime()?>" required>
            <br>
            <small>
                Correct forms for date: yyyy-mm-dd, yyyy/mm/dd, yyyy. mm. dd. <br>
                Correct form for time: hh:mm
            </small>
            <br> <br>
            <label for="transaction-amount">Amount and currency</label> <br>
            <input type="number" name="tranamount" id="transaction-amount" 
                   placeholder="enter a number" min="0" step="0.01" 
                   value="<?=$_GET['amount'] ?? ''?>" required> 
            <select name="trancurrency" title="select currency for the transaction">
                <?= $viewModel->getCurrencyOptions($_GET['currency'] ?? 'EUR') ?>
            </select> <br> <br>
            <label for="transaction-categories">Category</label> <br>
            <span>choose: </span>
            <select name="trancat" id="transaction-categories">
                <?= $viewModel->getUserCategories($_GET['category'] ?? ''); ?>
            </select>
            <span> or add category: </span> 
            <input type="text" name="trannewcat" id="transaction-newcat" placeholder="New category" 
            value="<?=$_GET['newcat'] ?? ''?>"> <br>
            <small>
                If you fill the new category field, than your transaction will go under that new category.
            </small> <br> <br>
            <label for="transaction-comment">Comment</label> <br>
            <input type="text" name="trancomment" id="transaction-comment" placeholder="optional identifier" 
            value="<?=$_GET['comment'] ?? ''?>"> <br> <br>
            <label for="transaction-type">Type: </label>
            <select name="trantype" id="transaction-type">
                <option value="in" <?=(isset($_GET['income']) && $_GET['income'] == '1') ? 'selected' : '' ?>>
                    Income (+)
                </option>
                <option value="out" <?=(isset($_GET['income']) && $_GET['income'] == '0') ? 'selected' : '' ?>>
                    Expenditure (-)
                </option>
            </select> <br>
            <small>
                Choose income if you recieved money and expenditure if you spent!
            </small> <br> <br>
            <input type="submit" value="Register transaction" class="btn btn-primary">
        </form>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 rounded">
        <div class="rounded border p-1 mb-2">
            <h2>Summary</h2>
            <div class="px-3">
                <h3>Total balance: <?php echo $viewModel->getBalance()?></h3>
            </div>
        </div>
        <div class="rounded border p-1">
            <h3>Date: <?=$viewModel->getDate()?></h3>
            <div class="px-3">
                <h3>Monthly balance: <?=$viewModel->getMothlyBalance()?></h3>
            </div>
            <h4>Income</h4>
            <?= $viewModel->getIncomesInTable(); ?>
            <h4>Expenditure</h4>
            <?= $viewModel->getExpendituresInTable(); ?>
            <a href="oldsummaries.php">Click here for your old summaries</a>
        </div>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 rounded">
        <h2>History</h2>
        <div class="table-responsive">
            <?= $viewModel->getHistoryTable() ?>
        </div>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
