<?php

if (!isset($viewModel)) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="./assets/images/logo_01_bg_sqr.png" type="image/png">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">

    <title>ManagE-Life registration page</title>
</head>
<body>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <form action="" method="post" class="border p-2 rounded">
            <label for="user-id-in">Username (nickname)</label> <br>
            <input type="text" name="userid" id="user-id-in" required 
            class="<?=($viewModel->usernameIncorrect()) ? 'input-incorrect' : ''?>"
            value="<?=
            (isset($_POST['userid']) && !$viewModel->usernameIncorrect()) ? $_POST['userid'] : ''
            ?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->usernameIncorrect()) ? '' : 'hidden'?>>
                Incorrect username (username should not contain any space and should be individual)
            </small> <br>

            <label for="email-in">email</label> <br>
            <input type="email" name="email" id="email-in" required
            class="<?=(  $viewModel->emailIncorrect()) ? 'input-incorrect' : ''?>"
            value="<?=
            (isset($_POST['email']) && !$viewModel->emailIncorrect()) ? $_POST['email'] : ''
            ?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->emailIncorrect()) ? '' : 'hidden'?>>
                Incorrect email (email should not contain any space and should be individual and contain dot and at). 
                <br>
                You may have registered with this email before.
            </small> <br>

            <label for="forename-in">Forename (first name)</label> <br>
            <input type="text" name="forename" id="forename-in" required
            value="<?= $_POST['forename'] ?? '' ?>"> <br> <br>

            <label for="surname-in">Surname (last name)</label> <br>
            <input type="text" name="surname" id="surname-in" required
            value="<?= $_POST['surname'] ?? '' ?>"> <br> <br>

            <label for="password-in">Password</label> <br>
            <input type="password" name="password" id="password-in" minlength="6" required
            class="<?=($viewModel->passwordIncorrect()) ? 'input-incorrect' : ''?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->passwordIncorrect()) ? '' : 'hidden'?>>
                Incorrect password (password should contain at least one number, one letter and 6 characters).
            </small> <br>

            <label for="password-again-in">Password again</label> <br>
            <input type="password" name="passagain" id="password-again-in" minlength="6" required
            class="<?=($viewModel->passAgainIncorrect()) ? 'input-incorrect' : ''?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->passAgainIncorrect()) ? '' : 'hidden'?>>
                The two passwords are not the same.
            </small> <br>

            <label for="regkey-in">Registration key</label> <br>
            <input type="password" name="regkey" id="regkey-in" required
            class="<?=($viewModel->regKeyIncorrect()) ? 'input-incorrect' : ''?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->regKeyIncorrect()) ? '' : 'hidden'?>>
                Registration key does not exist.
            </small> <br>

            <input type="submit" value="Registration" class="btn btn-primary">
        </form>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <a href="login.php" title="Go to login">Do you already have an account?</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
