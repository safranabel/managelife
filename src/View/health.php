<?php

if (!isset($viewModel)) {
    die('Forbidden: check the url!');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">
    <link rel="shortcut icon" href="./assets/images/logo_01_bg.svg" type="image/x-icon">

    <title>Your health management page at ManagE-Life</title>
</head>
<body>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Health management</h1>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Your goals</h1>
        <section class="border border-dark p-2 rounded">
            <form action="./controllers/sethealthgoal.php?type=weight" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="weightgoalin">Weight goal: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.5" min="0" name="amount" id="weightgoalin" 
                        placeholder="Targeted weight" value="<?= $viewModel->getWeightGoal() ?>"> kg
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save weight goal" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
        <section class="border border-dark p-2 mt-2 rounded">
            <form action="./controllers/sethealthgoal.php?type=water" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="watergoalin">Daily water goal: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.01" name="amount" id="watergoalin" 
                        placeholder="Targeted daily water" value="<?= $viewModel->getWaterGoal()?>"> l
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save water goal" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
        <section class="border border-dark p-2 mt-2 rounded">
            <form action="./controllers/sethealthgoal.php?type=calorie" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="caloriegoalin">Daily calorie intake goal: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.5" name="amount" id="caloriegoalin" 
                        placeholder="Targeted calorie intake" value="<?= $viewModel->getCalorieGoal()?>"> kcal
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save calorie goal" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Status report</h1>
        <section class="border border-dark rounded p-2">
            <h2>
                Water level: <?= $viewModel->getActualWaterLevel()?> / 
                <?= $viewModel->getWaterGoal()?> l
            </h2>
            <div class="progress">
                <div class="progress-bar bg-primary" role="progressbar" 
                style="width: <?= $viewModel->getWaterProgress() ?>%" 
                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>

            <h2 class="mt-5">
                Calorie level: <?= $viewModel->getActualCalorieLevel()?> / 
                <?= $viewModel->getCalorieGoal()?> kcal
            </h2>
            <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" 
                style="width: <?= $viewModel->getCalorieProgress() ?>%" aria-valuenow="0" 
                aria-valuemin="0" aria-valuemax="100"></div>
            </div>

            <h2 class="mt-5">
                Weight: 
                <span class="<?=$viewModel->getWeightSuccesClass()?>">
                    <?= $viewModel->getActualWeight()?>
                </span> / 
                <?= $viewModel->getWeightGoal()?> kg
            </h2>
        </section>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <h1>Food / Water /  Weight Registration</h1>
        <section class="border border-dark p-2 rounded">
            <form action="./controllers/healthchange.php?type=weight" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="actweightin">Weight: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.5" name="amount" id="actweightin" placeholder="Actual weight"> kg
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save actual weight" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
        <section class="border border-dark p-2 mt-2 rounded">
            <form action="./controllers/healthchange.php?type=water" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="waterin">Add water: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.01" name="amount" id="waterin" placeholder="Drinked amount"> l
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save water intake" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
        <section class="border border-dark p-2 mt-2 rounded">
            <form action="./controllers/healthchange.php?type=calorie" method="post">
                <div class="d-flex align-middle align-items-center w-100 flex-sm-row flex-column">
                    <div class="p-2 align-bottom h-100">
                        <label for="caloriein">Add calorie: </label>
                    </div>
                    <div class="p-2 align-middle h-100">
                        <input type="number" step="0.5" name="amount" id="caloriein" 
                        placeholder="Calorie intake"> kcal
                    </div>
                    <div class="p-2 flex-grow-1 d-flex justify-content-end">
                        <input type="submit" value="Save calorie intake" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </section>
    </article>
    <article class="bg-dark shadow-lg p-3 mb-3 rounded">
        <a href="index.php" class="btn btn-warning">Go back to index</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
