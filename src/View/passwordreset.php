<?php

if (!isset($viewModel)) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="./assets/images/logo_01_bg_sqr.png" type="image/png">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">

    <title>ManagE-Life password reset page</title>
</head>
<body>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded" <?= ($viewModel->getMessage() == '') ? "hidden" : ""?>>
        <?= $viewModel->getMessage() ?>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <form action="./controllers/newpass.php" method="post" class="border p-2 rounded">
            <label for="newpass-email">email</label> <br>
            <input type="email" name="email" id="newpass-email" placeholder="enter your email address" required>
            <br>
            <small>
                We will send an email to the given address, where you can reset your password. <br>
                Please check the spam folder, if you cannot find our mail!
            </small>
            <br>
            <input type="submit" value="Send email" class="btn btn-primary">
        </form>
    </article>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <a href="login.php">Go back to login</a> <br>
        <a href="registration.php">Sign up</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
