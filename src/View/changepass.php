<?php

if (!isset($viewModel)) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Password changing site</title>
</head>
<body>
    <article>
        <?= $viewModel->getMessage() ?>
    </article>
    <article>
        <form action="./changepass.php" method="post">
            <label for="newpass-in">New password</label> <br>
            <input type="password" name="newpass" id="newpass-in" placeholder="enter your new password" 
            minlength="6" required>
            <br>
            <small>
                Password must contain at least one number, one letter and 6 characters.
            </small>
            <br>
            <label for="newpass-again">New password again</label> <br>
            <input type="password" name="passagain" id="newpass-again" placeholder="enter password again" 
            minlength="6" required>
            <br> <br>
            <input type="text" name="username" value="<?=$_GET['username'] ?? ''?>" hidden>
            <input type="text" name="passkey" value="<?=$_GET['passkey'] ?? ''?>" hidden>
            <input type="text" name="type" value="<?=isset($_GET['forgotten']) ? 'forgotten' : ''?>" hidden>
            <input type="submit" value="Change password">
        </form>
    </article>
    <article>
        <a href="login.php">Go back to login</a>
    </article>
</body>
</html>
