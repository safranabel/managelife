<?php
if (!isset($viewModel)) {
    die('Forbidden page');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="./assets/images/logo_01_bg_sqr.png" type="image/png">
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
    >
    <link rel="stylesheet" href="./assets/css/base.css">

    <title>ManagE-Life login page</title>
</head>
<body>
    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <form action="" method="post" class="border p-2 rounded">
            <label for="user-id-in">Username or email</label> <br>
            <input type="text" name="userid" id="user-id-in" required 
            class="<?=( $viewModel->usernameOrEmailIncorrect()) ? 'input-incorrect' : ''?>"
            value="<?php
            if (isset($_POST['userid']) && !$viewModel->usernameOrEmailIncorrect()) {
                echo $_POST['userid'];
            }
            ?>"
            > <br>
            <small class="text-incorrect"
            <?=( $viewModel->usernameOrEmailIncorrect()) ? '' : 'hidden'?>>
                Incorrect username or email
                <a href="registration.php">(if you have not registered yet, click here!)</a>
            </small> <br>
            
            <label for="password-in">Password</label> <br>
            <input type="password" name="password" id="password-in" minlength="6" required
            class="<?=($viewModel->passwordIncorrect()) ? 'input-incorrect' : ''?>"
            > <br>
            <small class="text-incorrect" <?=($viewModel->passwordIncorrect()) ? '' : 'hidden'?>>
                Incorrect password
            </small> <br>

            <input type="submit" value="Login" class="btn btn-primary">
        </form>
        <br>
        <a href="passwordreset.php" title="Open the link in case of a forgotten password">Forgotten password?</a>
    </article>

    <article class="bg-light shadow-lg p-3 mb-3 mt-3 rounded">
        <a href="registration.php" title="Sign up here">Not signed up yet?</a>
    </article>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"
    ></script>
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"
    ></script>
</body>
</html>
