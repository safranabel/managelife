<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\PassUpdate;

use Managelife\PassReset\PassResetPersistence;
use Managelife\UserCheck\UserCheckModel;
use Managelife\Validator\Validator;

/**
 * PassUpdateModel
 * This class is responsible for logic during password updating.
 */
class PassUpdateModel extends UserCheckModel
{
    /**
     * @var array<string>
     */
    private array $errorFields;
    private bool $hasError;
    private string $passwordHash;
    private string $username;
    private bool $isValidated;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->errorFields = [];
        $this->hasError = false;
        $this->persistence = new PassResetPersistence();
        $this->passwordHash = '';
        $this->username = '';
        $this->isValidated = false;
    }

    /**
     * isValid
     * This function validates the password update attempt.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        if ($_POST) {
            if (
                !isset($_POST['newpass']) ||
                !Validator::validatePassword($_POST['newpass'])
            ) {
                $this->errorFields[] = 'PasswordError';
                $this->hasError = true;
            } elseif (!isset($_POST['passagain']) || is_null($_POST['passagain'])) {
                $this->errorFields[] = 'PassAgainError';
                $this->hasError = true;
            } elseif ($_POST['passagain'] != $_POST['newpass']) {
                $this->errorFields[] = 'PassAgainError';
                $this->hasError = true;
            } else {
                $this->passwordHash = hash("sha256", $_POST['newpass']);
            }

            $this->username = $_SESSION['username'] ?? '';

            if (
                !isset($_POST['oldpass']) ||
                !Validator::validateUserPassword($this->username, $_POST['oldpass'], $this->persistence)
            ) {
                $this->errorFields[] = 'OldPassError';
                $this->hasError = true;
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
        }
        $this->isValidated = true;
        return !$this->hasError;
    }

    /**
     * goBack
     * This function redirects the user to the settings page.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goBack(): void
    {
        $url = '../settings.php?';
        if ($this->hasError) {
            $errors = 'errors=';
            foreach ($this->errorFields as $error) {
                $errors .= $error . ',';
            }
            $errors = substr($errors, 0, -1);
            $url .= $errors;
        } else {
            $url .= 'success=1';
        }
        header('Location: ' . $url);
    }

    /**
     * saveNewPass
     * This function saves the new password.
     *
     * @return bool
     */
    public function saveNewPass(): bool
    {
        if ($this->hasError || !$this->isValidated) {
            return false;
        }

        if (
            !$this->persistence->updateUserFieldTo(
                $this->username,
                'password_hash',
                $this->passwordHash,
                's'
            )
        ) {
            return false;
        }

        return true;
    }
}
