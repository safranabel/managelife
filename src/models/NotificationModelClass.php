<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Notification;

use Managelife\Persistence\UserHandlerPersistence;
use Managelife\UserCheck\UserCheckModel;

/**
 * NotificationModel
 * This class is responsible for logic during handling notifications.
 */
class NotificationModel extends UserCheckModel
{
    protected UserHandlerPersistence $persistence;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new NotificationPersistence();
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * getSelectedNotificationOption
     * This function returns the selected notification frequency of the user.
     * Possible values: no, monthly, weekly, daily
     *
     * @return string
     */
    public function getSelectedNotificationOption(): string
    {
        $selected = $this->persistence->getSelectedNotifOpt($this->getUserid());
        if (!$selected) {
            return 'no';
        }
        if (
            $selected != 'no' &&
            $selected != 'monthly' &&
            $selected != 'weekly' &&
            $selected != 'daily'
        ) {
            return 'no';
        }
        return $selected;
    }

    /**
     * notifSettingValid
     * This function returns whether the given notification setting is valid.
     *
     * @return bool
     */
    public function notifSettingValid(): bool
    {
        if (!isset($_POST['type'])) {
            return false;
        }

        if (
            $_POST['type'] != 'n' &&
            $_POST['type'] != 'm' &&
            $_POST['type'] != 'w' &&
            $_POST['type'] != 'd'
        ) {
            return false;
        }

        return true;
    }

    /**
     * saveNotifSetting
     * This function saves the notification setting if it is valid.
     *
     * @return bool
     */
    public function saveNotifSetting(): bool
    {
        if (!$this->notifSettingValid()) {
            return false;
        }


        return $this->persistence->setUserNotifTo(
            $this->getUserid(),
            $this->getTypeFromShort($_POST['type'])
        );
    }

    /**
     * getTypeFromShort
     * This function converts the short form of the notification setting to the long-form.
     *
     * @param  string $short
     * @return string
     */
    private function getTypeFromShort(string $short): string
    {
        $out = '';
        switch ($short) {
            case 'm':
                $out = 'monthly';
                break;
            case 'w':
                $out = 'weekly';
                break;
            case 'd':
                $out = 'daily';
                break;
            default:
                $out = 'no';
        }
        return $out;
    }
}
