<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\UserIndex;

use Managelife\Model\BaseModel;
use Managelife\UserIndex\UserPersistence;
use DateTime;
use Managelife\Persistence\UserHandlerPersistence;

/**
 * UserModel
 * This class is responsible for logic in features across the user's index page.
 */
class UserModel extends BaseModel
{
    private UserHandlerPersistence $persistence;
    private string $userid;
    private string $email;
    private string $forename;
    private string $surname;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new UserPersistence();
        $this->userid = '';
        $this->email = '';
        $this->forename = '';
        $this->surname = '';
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * setUserData
     * This function sets the given user data.
     *
     * @param  string $userid
     * @param  string $email
     * @param  string $forename
     * @param  string $surname
     * @return void
     */
    public function setUserData(string $userid, string $email, string $forename, string $surname): void
    {
        $this->userid = $userid;
        $this->email = $email;
        $this->forename = $forename;
        $this->surname = $surname;
    }

    /**
     * getForename
     * This function returns the user's forename.
     *
     * @return string
     */
    public function getForename(): string
    {
        return $this->forename;
    }

    /**
     * getUserCategories
     * This function returns the user's categories.
     *
     * @return array<string>
     */
    public function getUserCategories(): array
    {
        $categories = $this->persistence->getUserCategories($this->userid);
        sort($categories, SORT_STRING);
        return $categories;
    }

    /**
     * getUserCurrencies
     * This function returns the user's currencies.
     *
     * @return array<string>
     */
    public function getUserCurrencies(): array
    {
        return $this->persistence->getUserCurrencies($this->userid);
    }

    /**
     * getUserCategoriesWithIncome
     * This function returns the categories that the user had incomes/expenditures in.
     * It returns the ones that had expenditures in if the $income equals 0.
     * It returns the ones that had incomes in if the $income equals 1.
     *
     * @param  int $income
     * @return array<string>
     */
    public function getUserCategoriesWithIncome(int $income): array
    {
        $categories = $this->persistence->getUserCategoriesWhereIncome($this->userid, $income);
        sort($categories, SORT_STRING);
        return $categories;
    }

    /**
     * getUserCurrenciesWithIncome
     * This function returns the currencies that the user had incomes/expenditures in.
     * It returns the ones that had expenditures in if the $income equals 0.
     * It returns the ones that had incomes in if the $income equals 1.
     *
     * @param  int $income
     * @return array<string>
     */
    public function getUserCurrenciesWithIncome(int $income): array
    {
        return $this->persistence->getUserCurrenciesWhereIncome($this->userid, $income);
    }

    /**
     * getIncome
     * This function returns the sum of the incomes in the given category and currency, between the given dates.
     *
     * @param  string $startDate
     * @param  string $finalDate
     * @param  string $currency
     * @param  string $category
     * @return float
     */
    public function getIncome(string $startDate, string $finalDate, string $currency, string $category): float
    {
        $result = $this->persistence->getUserSumBetweenDatesWithCurrency(
            $this->userid,
            $startDate,
            $finalDate,
            $currency,
            $category,
            1
        );
        return $result;
    }

    /**
     * getExpenditure
     * This function returns the sum of the expenditures in the given category and currency, between the given dates.
     *
     * @param  string $startDate
     * @param  string $finalDate
     * @param  string $currency
     * @param  string $category
     * @return float
     */
    public function getExpenditure(string $startDate, string $finalDate, string $currency, string $category): float
    {
        $result = $this->persistence->getUserSumBetweenDatesWithCurrency(
            $this->userid,
            $startDate,
            $finalDate,
            $currency,
            $category,
            0
        );
        return $result;
    }

    /**
     * getMonthlyBalance
     * This function returns the monthly balance of the user in the given month, in the given currency.
     *
     * @param  DateTime $date
     * @param  string $currency
     * @return float
     */
    public function getMonthlyBalance(DateTime $date, string $currency): float
    {
        $date->setDate(intval($date->format('Y')), intval($date->format('m')), 1);
        $startDate = strval($date->format('Y-m-d'));
        $date->modify('+1 month');
        $finalDate = strval($date->format('Y-m-d'));
        $result = 0;
        foreach ($this->getUserCategories() as $category) {
            $result += $this->persistence->getUserSumBetweenDatesWithCurrency(
                $this->userid,
                $startDate,
                $finalDate,
                $currency,
                $category,
                1
            );
            $result -= $this->persistence->getUserSumBetweenDatesWithCurrency(
                $this->userid,
                $startDate,
                $finalDate,
                $currency,
                $category,
                0
            );
        }
        return $result;
    }

    /**
     * getUserTransactions
     * This function returns the user's transactions.
     *
     * @return array<int, array<string, float|string>>
     */
    public function getUserTransactions(): array
    {
        $res = [];
        if (!($res = $this->persistence->getUserTransactions($this->userid))) {
            $res = [];
        }
        return $res;
    }

    /**
     * getBalance
     * This function returns the user's all-time balance in the given currency.
     *
     * @param  string $currency
     * @return float
     */
    public function getBalance(string $currency): float
    {
        $res = $this->persistence->getSUM($this->userid, $currency, 1);
        $res -= $this->persistence->getSUM($this->userid, $currency, 0);
        return $res;
    }

    /**
     * getAllCurrencies
     * This function returns all the available currencies in the application.
     *
     * @return array<string>
     */
    public function getAllCurrencies(): array
    {
        return $this->getCurrencies($this->persistence);
    }

    /**
     * getEmail
     * This function returns the user's email address.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * getSurname
     * This function returns the user's surname.
     *
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * getPlan
     * This function returns the amount for the given plan.
     *
     * @param  int $year
     * @param  int $month
     * @param  bool $income
     * @param  string $category
     * @param  string $currency
     * @return float
     */
    public function getPlan(int $year, int $month, bool $income, string $category, string $currency): float
    {
        $res = $this->persistence->getPlanAmount($this->userid, $year, $month, $income, $category, $currency);
        if (!$res) {
            return 0;
        }
        return $res;
    }
}
