<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Notification;

use Managelife\Model\BaseModel;
use Managelife\Notification\NotificationPersistence;
use Managelife\Notification\NotificationModel;
use DateTime;

/**
 * NotificationUpdateModel
 * This class is responsible for logic during updating the notifications of the user.
 */
class NotificationUpdateModel extends BaseModel
{
    /**
     * update
     * This function updates the notifications on the notification server according to the user preferences.
     *
     * @codeCoverageIgnore
     * @return bool
     */
    public static function update(): bool
    {
        $persistence = new NotificationPersistence();
        $model = new NotificationModel();
        $model->refreshUserData();
        if (!$persistence->deleteNotifications($model->getEmail())) {
            return false;
        }
        $freq = $persistence->getSelectedNotifOpt($model->getUserid());
        if ($freq == 'no') {
            return true;
        }

        $date = new DateTime(strval(date('Y')) . '-' . strval(date('m')) . '-01');
        $date->setDate(intval($date->format('Y')), intval($date->format('m')), 1);
        $startDate = strval($date->format('Y-m-d'));
        $date->modify('+1 month');
        $finalDate = strval($date->format('Y-m-d'));
        $year = intval(date('Y', intval(strtotime($startDate))));
        $month = intval(date('n', intval(strtotime($startDate))));
        /**
        * @var array<string, array<string, array<string, string | float>>> $importantData
        */
        $importantData = [
            'in' => [
                '100' => [],
                '75' => [],
                '25' => [],
                '0' => []
            ],
            'ex' => [
                '100' => [],
                '75' => [],
                '25' => [],
                '0' => []
            ]
        ];

        NotificationUpdateModel::loadImportant(
            $importantData,
            $startDate,
            $finalDate,
            $year,
            $month,
            false,
            $model,
            $persistence
        );

        NotificationUpdateModel::loadImportant(
            $importantData,
            $startDate,
            $finalDate,
            $year,
            $month,
            true,
            $model,
            $persistence
        );

        $text = "";
        $text .= "EXPENDITURES: ";
        if (count($importantData['ex']['100']) > 0) {
            $text .= "\n\n";
            $text .= "You have spent a lot on these categories: \n";
            foreach ($importantData['ex']['100'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['ex']['75']) > 0) {
            $text .= "\n\n";
            $text .= "You have spent quite a bit on the following categories: \n";
            foreach ($importantData['ex']['75'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['ex']['25']) > 0) {
            $text .= "\n\n";
            $text .= "You are fine with the categories below: \n";
            foreach ($importantData['ex']['25'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['ex']['0']) > 0) {
            $text .= "\n\n";
            $text .= "You have not spent on these categories yet: \n";
            foreach ($importantData['ex']['0'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        $text .= "\n\n";
        $text .= "INCOME: ";

        if (count($importantData['in']['0']) > 0) {
            $text .= "\n\n";
            $text .= "Your revenue from these categories has not yet been entered: \n";
            foreach ($importantData['in']['0'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['in']['25']) > 0) {
            $text .= "\n\n";
            $text .= "Only a small portion of these expected revenues were received: \n";
            foreach ($importantData['in']['25'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['in']['75']) > 0) {
            $text .= "\n\n";
            $text .= "You will soon have enough revenue from the categories below: \n";
            foreach ($importantData['in']['75'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        if (count($importantData['in']['100']) > 0) {
            $text .= "\n\n";
            $text .= "All (or more than) the expected revenue has arrived from the following categories: \n";
            foreach ($importantData['in']['100'] as $key => $value) {
                $text .= "--  " . $value['cat'] . ": " . $value['act'] . "/" . $value['pla'] . $value['cur'] . "\n";
            }
        }

        $subject = "";
        $date = new DateTime();
        switch ($freq) {
            case 'monthly':
                $subject .= "[ManagE-life] Monthly Summary";
                $date->modify('last day of this month');
                break;
            case 'weekly':
                $subject .= "[ManagE-life] Weekly Summary";
                $date->modify('next monday');
                break;
            case 'daily':
                $subject .= "[ManagE-life] Daily Summary";
                $date->modify('+1 day');
                break;
        }

        if (
            !$persistence->addNotification(
                $model->getEmail(),
                $subject,
                $text,
                date_format($date, 'Y-m-d'),
                '08:00'
            )
        ) {
            return false;
        }

        return true;
    }

    /**
     * loadImportant
     * This function loads the interesting data to $important.
     *
     * @codeCoverageIgnore
     * @param array<string, array<string, array<string, string | float>>> $important
     * @param string $startDate
     * @param string $finalDate
     * @param int $year
     * @param int $month
     * @param bool $income
     * @param NotificationModel $model
     * @param NotificationPersistence $persistence
     * @return void
     */
    private static function loadImportant(
        array &$important,
        string $startDate,
        string $finalDate,
        int $year,
        int $month,
        bool $income,
        NotificationModel $model,
        NotificationPersistence $persistence
    ): void {
        $innum = 0;
        if ($income) {
            $innum = 1;
        }
        $categories = $persistence->getUserCategoriesWhereIncome($model->getUserid(), $innum);
        $currencies = $persistence->getUserCurrenciesWhereIncome($model->getUserid(), $innum);
        sort($categories, SORT_STRING);
        foreach ($currencies as $currency) {
            foreach ($categories as $category) {
                $actual = 0;
                $planned = $persistence->getPlanAmount(
                    $model->getUserid(),
                    $year,
                    $month,
                    $income,
                    $category,
                    $currency
                );
                $innum = 0;
                if ($income) {
                    $innum = 1;
                }
                $actual = $persistence->getUserSumBetweenDatesWithCurrency(
                    $model->getUserid(),
                    $startDate,
                    $finalDate,
                    $currency,
                    $category,
                    $innum
                );
                if (!$income) {
                    if ($planned > 0 && $actual / $planned >= 1) {
                        $important['ex']['100'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual == 0) {
                        $important['ex']['0'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual / $planned < 0.25) {
                        $important['ex']['25'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual / $planned > 0.75) {
                        $important['ex']['75'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned <= 0 && $actual > 0) {
                        $important['ex']['100'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    }
                } else {
                    if (($planned > 0 && $actual >= $planned) || ($planned <= 0 && $actual > 0)) {
                        $important['in']['100'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual / $planned >= 0.75) {
                        $important['in']['75'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual == 0) {
                        $important['in']['0'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    } elseif ($planned > 0 && $actual / $planned < 0.25) {
                        $important['in']['25'][] = [
                            'cat' => $category,
                            'cur' => $currency,
                            'act' => $actual,
                            'pla' => $planned
                        ];
                    }
                }
            }
        }
    }
}
