<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\UserCheck;

use Managelife\Model\BaseModel;
use Managelife\Persistence\UserHandlerPersistence;
use Managelife\UserCheck\UserCheckPersistence;
use DateTime;

/**
 * UserCheckModel
 * This class is responsible for logic during user validation and user data refreshing.
 */
class UserCheckModel extends BaseModel
{
    protected UserHandlerPersistence $persistence;
    private string $userid;
    private string $email;
    private string $forename;
    private string $surname;
    private bool $isAdmin;
    private bool $validated;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new UserCheckPersistence();
        $this->userid = '';
        $this->email = '';
        $this->forename = '';
        $this->surname = '';
        $this->isAdmin = false;
        $this->validated = false;
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * refreshUserData
     * This function refreshes the user data with the current values.
     *
     * @return void
     */
    public function refreshUserData(): void
    {
        if (
            isset($_SESSION['username']) && $this->persistence->userExists($_SESSION['username'])
            && isset($_SESSION['token']) && ($_SESSION['token'] == UserCheckPersistence::USER_TOKEN
            || $_SESSION['token'] == UserCheckPersistence::ADMIN_TOKEN)
        ) {
            $this->userid = $_SESSION['username'];
            $this->email = strval($this->persistence->getFieldOfUser('email', $this->userid));
            $this->forename = strval($this->persistence->getFieldOfUser('forename', $this->userid));
            $this->surname = strval($this->persistence->getFieldOfUser('surname', $this->userid));
            $this->isAdmin = $this->persistence->isAdmin($this->userid) && isset($_SESSION['token'])
                            && $_SESSION['token'] == UserCheckPersistence::ADMIN_TOKEN;
            $this->validated = true;
        }
    }

    /**
     * isAdmin
     * This function returns whether the user has admin permissions.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * userValid
     * This function returns whether the user is valid.
     *
     * @return bool
     */
    public function userValid(): bool
    {
        return $this->validated;
    }

    /**
     * getUserid
     * This function returns the current user's username.
     *
     * @return string
     */
    public function getUserid(): string
    {
        return $this->userid;
    }

    /**
     * getEmail
     * This function returns the current user's email address.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * getForename
     * This function returns the current user's forename.
     *
     * @return string
     */
    public function getForename(): string
    {
        return $this->forename;
    }

    /**
     * getSurname
     * This function returns the current user's surname.
     *
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * validateDate
     * This function returns whether the date is valid in the given format.
     *
     * @param  string $date
     * @param  string $format
     * @return bool
     */
    public function validateDate(string $date, string $format = 'Y-m-d'): bool
    {
        if ($d = DateTime::createFromFormat($format, $date)) {
            return $d->format($format) == $date;
        } else {
            return false;
        }
    }

    /**
     * validateTime
     * This function returns whether the given time is valid.
     *
     * @param  string $time
     * @return bool
     */
    public function validateTime(string $time): bool
    {
        $timearr = explode(':', $time);
        if (count($timearr) != 2) {
            return false;
        }
        if (!preg_match("/^\d+$/", $timearr[0])) {
            return false;
        }
        if (!preg_match("/^\d+$/", $timearr[1])) {
            return false;
        }
        if (intval($timearr[0]) < 0 || 23 < intval($timearr[0])) {
            return false;
        }
        if (intval($timearr[1]) < 0 || 59 < intval($timearr[1])) {
            return false;
        }
        return true;
    }
}
