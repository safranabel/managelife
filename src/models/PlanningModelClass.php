<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Planning;

use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Planning\PlanningPersistence;
use Managelife\UserCheck\UserCheckModel;
use Managelife\Validator\Validator;

/**
 * PlanningModel
 * This class is responsible for logic during financial planning.
 */
class PlanningModel extends UserCheckModel
{
    protected UserHandlerPersistence $persistence;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new PlanningPersistence();
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * getTableJSON
     * This function returns the plans for the given date in JSON format.
     *
     * @return string
     */
    public function getTableJSON(): string
    {
        if (!isset($_GET['year']) || !isset($_GET['month'])) {
            return "{}";
        }
        $table = $this->persistence->getUserPlans(
            $this->getUserid(),
            intval($_GET["year"]),
            intval($_GET["month"])
        );
        if (!$table) {
            return "{}";
        }
        $result = json_encode($table);
        return strval($result);
    }

    /**
     * saveTable
     * This function:
     * Returns 0 if the saving is succesful (positive numbers if it is not)
     * Return 1: invalid JSON
     * Return 2: invalid Category
     * Return 3: invalid currency
     * Return 4: data issue
     *
     * @codeCoverageIgnore
     * @return int
     */
    public function saveTable(): int
    {
        $json = file_get_contents("php://input");
        if (!$json) {
            return 1;
        }
        /**
        * @var array<string, array<string, array<string, float>>>
        */
        $table = json_decode(strval($json), true);
        foreach ($table as $category => $v) {
            if (!Validator::validateCategoryName($category)) {
                return 2;
            }
        }
        if (!$this->checkAllCurrencies($table)) {
            return 3;
        }
        if (
            !$this->persistence->saveUserPlans(
                $this->getUserid(),
                $table,
                intval($_GET['year']),
                intval($_GET['month'])
            )
        ) {
            return 4;
        }
        return 0;
    }

    /**
     * checkAllCurrencies
     * This function checks whether the currencies are valid in the given table.
     *
     * @codeCoverageIgnore
     * @param  array<string, array<string, array<string, float>>> $table
     * @return bool
     */
    private function checkAllCurrencies(array $table): bool
    {
        foreach ($table as $cat => $ei) {
            foreach ($table[$cat]["Expenditure"] as $currency => $a) {
                if (!Validator::validateCurrency($currency, $this->persistence)) {
                    return false;
                }
            }
            foreach ($table[$cat]["Income"] as $currency => $a) {
                if (!Validator::validateCurrency($currency, $this->persistence)) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * getYear
     * This function returns the right date for the plans.
     *
     * @return int
     */
    public function getYear(): int
    {
        if (isset($_GET['year'])) {
            return intval($_GET['year']);
        }
        return intval(date('Y'));
    }

    /**
     * getMonth
     * This function returns the right date for the plans.
     *
     * @return int
     */
    public function getMonth(): int
    {
        if (isset($_GET['month'])) {
            return intval($_GET['month']);
        }
        return intval(date('m'));
    }
}
