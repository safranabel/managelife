<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Transaction;

use Managelife\UserCheck\UserCheckModel;
use DateTime;
use Managelife\UserCheck\UserCheckPersistence;
use Managelife\Validator\Validator;

/**
 * TransactionModel
 * This class is responsible for logic during transaction management.
 */
class TransactionModel extends UserCheckModel
{
    /**
     * @var array<string>
     */
    private array $errorFields = ['NonValidated'];
    private bool $hasError = true;
    private string $date = '';
    private string $time = '';
    private float $amount = 0;
    private string $currency = '';
    private string $category = '';
    private string $comment = '-';
    private int $income = -1;
    private bool $newcat = false;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->persistence = new UserCheckPersistence();
    }

    /**
     * transactionValid
     * This function validates the sent transaction.
     *
     * @return bool
     */
    public function transactionValid(): bool
    {
        $this->hasError = false;
        $this->errorFields = [];
        if ($_POST) {
            if (!isset($_POST['trandate']) || is_null($_POST['trandate'])) {
                $this->errorFields[] = 'DateError';
                $this->hasError = true;
            } elseif (
                !$this->validateDate($_POST['trandate'], 'Y-m-d') &&
                !$this->validateDate($_POST['trandate'], 'Y. m. d.') &&
                !$this->validateDate($_POST['trandate'], 'Y/m/d')
            ) {
                $this->errorFields[] = 'DateError';
                $this->hasError = true;
            } else {
                $format = 'Y-m-d';
                if ($this->validateDate($_POST['trandate'], 'Y. m. d.')) {
                    $format = 'Y. m. d.';
                } elseif ($this->validateDate($_POST['trandate'], 'Y/m/d')) {
                    $format = 'Y/m/d';
                }
                if ($d = DateTime::createFromFormat($format, $_POST['trandate'])) {
                    $this->date = $d->format('Y-m-d');
                }
            }


            if (!isset($_POST['trantime']) || is_null($_POST['trantime'])) {
                $this->errorFields[] = 'TimeError';
                $this->hasError = true;
            } elseif (!$this->validateTime($_POST['trantime'])) {
                $this->errorFields[] = 'TimeError';
                $this->hasError = true;
            } else {
                $this->time = $_POST['trantime'];
            }

            if (!isset($_POST['tranamount']) || is_null($_POST['tranamount'])) {
                $this->errorFields[] = 'AmountError';
                $this->hasError = true;
            } elseif (!filter_var($_POST['tranamount'], FILTER_VALIDATE_FLOAT) || intval($_POST['tranamount']) < 0) {
                $this->errorFields[] = 'AmountError';
                $this->hasError = true;
            } else {
                $this->amount = floatval($_POST['tranamount']);
            }

            if (
                !isset($_POST['trancurrency']) ||
                !Validator::validateCurrency($_POST['trancurrency'], $this->persistence)
            ) {
                $this->errorFields[] = 'CurrencyError';
                $this->hasError = true;
            } else {
                $this->currency = $_POST['trancurrency'];
            }

            if (!isset($_POST['trannewcat']) || $_POST['trannewcat'] == "") {
                if (!isset($_POST['trancat']) || is_null($_POST['trancat'])) {
                    $this->errorFields[] = 'CatError';
                    $this->hasError = true;
                } elseif (!in_array($_POST['trancat'], $this->persistence->getUserCategories($this->getUserid()))) {
                    $this->errorFields[] = 'CatError';
                    $this->hasError = true;
                } else {
                    $this->category = $_POST['trancat'];
                }
            } else {
                if (
                    !isset($_POST['trannewcat']) ||
                    is_null($_POST['trannewcat']) ||
                    strlen(trim($_POST['trannewcat'])) <= 0
                ) {
                    $this->errorFields[] = 'NewCatError';
                    $this->hasError = true;
                } else {
                    if (strlen($_POST['trannewcat']) > 99 || !Validator::validateCategoryName($_POST['trannewcat'])) {
                        $this->errorFields[] = 'NewCatError';
                        $this->hasError = true;
                    } else {
                        $this->category = $_POST['trannewcat'];
                        $this->newcat = true;
                    }
                }
            }

            if (isset($_POST['trancomment'])  && strlen($_POST['trancomment']) > 250) {
                $this->errorFields[] = 'CommentError';
                $this->hasError = true;
            } else {
                if (isset($_POST['trancomment'])  && strlen($_POST['trancomment']) > 0) {
                    $this->comment = $_POST['trancomment'];
                }
            }

            if (!isset($_POST['trantype']) || is_null($_POST['trantype'])) {
                $this->errorFields[] = 'TypeError';
                $this->hasError = true;
            } elseif (!in_array($_POST['trantype'], ['in', 'out'])) {
                $this->errorFields[] = 'TypeError';
                $this->hasError = true;
            } else {
                $this->income = ($_POST['trantype'] == 'in') ? 1 : 0;
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
        }
        return !$this->hasError;
    }

    /**
     * saveTransaction
     * This function saves the transaction.
     *
     * @return bool
     */
    public function saveTransaction(): bool
    {
        if (!$this->hasError) {
            if ($this->newcat) {
                $this->persistence->addCategory($this->category, $this->getUserid());
            }
            return $this->persistence->addTransaction(
                $this->getUserid(),
                $this->date,
                $this->time,
                $this->category,
                $this->comment,
                $this->income,
                $this->amount,
                $this->currency
            );
        } else {
            return false;
        }
    }

    /**
     * goToIndex
     * This function redirects the user to the index page.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goToIndex(): void
    {
        $url = '../index.php?';
        if ($this->hasError) {
            if (!in_array('DateError', $this->errorFields) && $this->date != '') {
                $url .= '&date=' . $this->date;
            }
            if (!in_array('TimeError', $this->errorFields) && $this->time != '') {
                $url .= '&time=' . $this->time;
            }
            if (!in_array('AmountError', $this->errorFields) && $this->amount != '') {
                $url .= '&amount=' . $this->amount;
            }
            if (!in_array('CurrencyError', $this->errorFields) && $this->currency != '') {
                $url .= '&currency=' . $this->currency;
            }
            if (!in_array('CatError', $this->errorFields) && $this->category != '' && !$this->newcat) {
                $url .= '&category=' . $this->category;
            }
            if (!in_array('NewCatError', $this->errorFields) && $this->newcat) {
                $url .= '&newcat=' . $this->category;
            }
            if (!in_array('CommentError', $this->errorFields) && $this->comment != '-') {
                $url .= '&comment=' . $this->comment;
            }
            if (!in_array('TypeError', $this->errorFields)) {
                $url .= '&income=' . strval($this->income);
            }
            $errors = '&errors=';
            foreach ($this->errorFields as $error) {
                $errors .= $error . ',';
            }
            $errors = substr($errors, 0, -1);
            $url .= $errors;
        } else {
            $url .= 'success=1';
        }
        header('Location: ' . $url);
    }
}
