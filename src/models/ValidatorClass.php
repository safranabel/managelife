<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Validator;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * Validator
 * This class is responsible for basic validations.
 */
abstract class Validator
{
    /**
     * validatePassword
     *
     * @param  string|null $password
     * @return bool
     */
    public static function validatePassword(string|null $password): bool
    {
        if (!isset($password)) {
            return false;
        }
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        if (!($uppercase || $lowercase) || !$number || strlen($password) < 6) {
            return false;
        }
        return true;
    }

    /**
     * validateExistingUsername
     * This function returns whether the username exists already.
     *
     * @param string|null $username
     * @param UserHandlerPersistence $persistence
     * @return bool
     */
    public static function validateExistingUsername(
        string|null $username,
        UserHandlerPersistence $persistence
    ): bool {
        if (!isset($username)) {
            return false;
        }
        if (str_contains($username, ' ') || !$persistence->userExists($username)) {
            return false;
        }
        return true;
    }

    /**
     * validateUserPassword
     * This function validates the password of the given user.
     *
     * @param string|null $username
     * @param string|null $password
     * @param UserHandlerPersistence $persistence
     * @return bool
     */
    public static function validateUserPassword(
        string|null $username,
        string|null $password,
        UserHandlerPersistence $persistence
    ): bool {
        if (!isset($username)) {
            return false;
        }
        if (!isset($password)) {
            return false;
        }

        if (!$persistence->userExists($username)) {
            return false;
        }
        $userPass = $persistence->getFieldOfUser('password_hash', $username);
        if ($userPass != hash("sha256", $password)) {
            return false;
        }
        return true;
    }

    /**
     * validateCategoryName
     * This function validates the given category name.
     *
     * @param  string|null $name
     * @return bool
     */
    public static function validateCategoryName(string|null $name): bool
    {
        if (!isset($name)) {
            return false;
        }
        $name = trim($name);
        if (strlen($name) < 3) {
            return false;
        }
        if (!preg_match('@[a-z]@', $name) && !preg_match('@[A-Z]@', $name)) {
            return false;
        }
        return true;
    }

    /**
     * validateCurrency
     * This function returns whether the given name is an existing currency.
     *
     * @param string|null $name
     * @param UserHandlerPersistence $persistence
     * @return bool
     */
    public static function validateCurrency(
        string|null $name,
        UserHandlerPersistence $persistence
    ): bool {
        if (!isset($name)) {
            return false;
        }
        $name = trim($name);
        if (strlen($name) < 2 || strlen($name) > 5) {
            return false;
        }
        if (!preg_match('@[a-z]@', $name) && !preg_match('@[A-Z]@', $name)) {
            return false;
        }
        $currencies = $persistence->getCurrencies();
        if (!$currencies) {
            return false;
        }
        if (!in_array($name, $currencies)) {
            return false;
        }
        return true;
    }
}
