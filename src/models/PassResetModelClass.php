<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\PassReset;

use Managelife\Model\BaseModel;
use Managelife\PassReset\PassResetPersistence;
use Managelife\Mailer\MailerModel;
use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Validator\Validator;

/**
 * PassResetModel
 * This class is responsible for logic during password resetting.
 */
class PassResetModel extends BaseModel
{
    /**
     * @var array<string>
     */
    private array $errorFields;
    private bool $hasError;
    private UserHandlerPersistence $persistence;
    private string $email;
    private string $passwordHash;
    private string $username;
    private string $passkey;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->errorFields = [];
        $this->hasError = false;
        $this->persistence = new PassResetPersistence();
        $this->email = '';
        $this->passwordHash = '';
        $this->username = '';
        $this->passkey = '';
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * hasError
     * This function returns whether there was an error in the validation.
     *
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->hasError;
    }

    /**
     * isValid
     * This function validates the request for a new password.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        if ($_POST) {
            if (!isset($_POST['email']) || is_null($_POST['email'])) {
                $this->errorFields[] = 'EmailError';
                $this->hasError = true;
            } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $this->errorFields[] = 'EmailError';
                $this->hasError = true;
            } elseif (!$this->persistence->fieldExistsWithValue('email', $_POST['email'], 's')) {
                $this->errorFields[] = 'EmailError';
                $this->hasError = true;
            } else {
                $this->email = $_POST['email'];
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
        }
        return !$this->hasError;
    }

    /**
     * sendMail
     * This function sends an email to the user with the password reset link.
     *
     * @codeCoverageIgnore
     * @return bool
     */
    public function sendMail(): bool
    {
        if ($this->hasError) {
            return false;
        }
        $mailer = new MailerModel();
        $htmlMessage = '';
        $pureMessage = '';
        $subject = '[ManagE-Life] Password reset email';
        $username = '';
        if ($username = $this->persistence->getUserWhereEmail($this->email)) {
            $name = '';
            if ($name = $this->persistence->getFieldOfUser('forename', $username)) {
                $htmlMessage .= '<p>Dear ' . $name . ', </p>';
                $pureMessage .= 'Dear ' . $name . ', ';
            } else {
                $htmlMessage .= '<p>Dear Cutomer, </p>';
                $pureMessage .= 'Dear Customer, ';
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
            return false;
        }
        $passkey = '';
        if (!$passkey = $this->generateAndSavePassKey($username)) {
            return false;
        }
        $htmlMessage .= '<p>you can reset your password by following the next link: ' .
                        '<a href="' . $_SERVER['SERVER_NAME'] . '/managelife/app/changepass.php?username=' . $username .
                        '&passkey=' . $passkey . '&forgotten=1">link for reset your password</a></p>';
        $pureMessage .= 'you can reset your password by following the next link:' .
                        $_SERVER['SERVER_NAME'] . '/managelife/app/changepass.php?username=' .
                        $username . '&passkey=' . $passkey . '&forgotten=1 ';

        $htmlMessage .= '<p>Your faithfully, <br>ManagE-Life service team</p>';
        $pureMessage .= 'Your faithfully, ManagE-Life service team ';

        $htmlMessage .= '<br><br><i>Please do not reply to this email!</i>';
        $pureMessage .= 'Please do not reply to this email!';

        $mailer->setAddress($this->email);
        $mailer->setSubject($subject);
        $mailer->setHTMLBody($htmlMessage);
        $mailer->setNonHTMLBody($pureMessage);
        if ($mailer->send()) {
            return true;
        }
        $this->errorFields[] = 'ServerError';
        $this->hasError = true;
        return false;
    }

    /**
     * goBack
     * This function redirects the user to the passwordreset.php page.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goBack(): void
    {
        $url = '../passwordreset.php?';
        if ($this->hasError) {
            $errors = 'errors=';
            foreach ($this->errorFields as $error) {
                $errors .= $error . ',';
            }
            $errors = substr($errors, 0, -1);
            $url .= $errors;
        } else {
            $url .= 'success=1';
        }
        header('Location: ' . $url);
    }

    /**
     * validatePassChange
     * This function validates the new given password.
     *
     * @return bool
     */
    public function validatePassChange(): bool
    {
        $this->errorFields = [];
        if ($_POST) {
            if (
                !isset($_POST['newpass']) ||
                !Validator::validatePassword($_POST['newpass'])
            ) {
                $this->errorFields[] = 'PasswordError';
                $this->hasError = true;
            } elseif (!isset($_POST['passagain']) || is_null($_POST['passagain'])) {
                $this->errorFields[] = 'PassAgainError';
                $this->hasError = true;
            } elseif ($_POST['passagain'] != $_POST['newpass']) {
                $this->errorFields[] = 'PassAgainError';
                $this->hasError = true;
            } else {
                $this->passwordHash = hash("sha256", $_POST['newpass']);
            }

            if (
                !isset($_POST['username']) ||
                !Validator::validateExistingUsername($_POST['username'], $this->persistence)
            ) {
                $this->errorFields[] = 'UserError';
                $this->hasError = true;
            } else {
                $this->username = $_POST['username'];
            }

            if (!isset($_POST['passkey']) || is_null($_POST['passkey'])) {
                $this->errorFields[] = 'KeyError';
                $this->hasError = true;
            } elseif (
                !$this->persistence->fieldExistsWithValue(
                    'password_reset',
                    hash('sha256', $_POST['passkey']),
                    's'
                )
            ) {
                $this->errorFields[] = 'KeyError';
                $this->hasError = true;
            } else {
                $this->passkey = $_POST['passkey'];
            }

            if (!isset($_POST['type']) || is_null($_POST['type'])) {
                $this->errorFields[] = 'TypeError';
                $this->hasError = true;
            } elseif ($_POST['type'] != 'forgotten') {
                $this->errorFields[] = 'TypeError';
                $this->hasError = true;
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
        }
        return !$this->hasError;
    }

    /**
     * saveNewPass
     * This function saves the new password.
     *
     * @return bool
     */
    public function saveNewPass(): bool
    {
        if ($this->hasError) {
            return false;
        }

        if (
            !$this->persistence->updateUserFieldTo(
                $this->username,
                'password_hash',
                $this->passwordHash,
                's'
            )
        ) {
            return false;
        }

        if (!$this->persistence->setUserFieldToNull($this->username, 'password_reset')) {
            return false;
        }

        return true;
    }

    /**
     * goChangePass
     * This function redirects the user to the changepass.php page.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goChangePass(): void
    {
        $url = './changepass.php?';
        if ($this->hasError) {
            $errors = 'errors=';
            foreach ($this->errorFields as $error) {
                $errors .= $error . ',';
            }
            $errors = substr($errors, 0, -1);
            $url .= $errors;

            $fields = '&username=' . $this->username;
            $fields .= '&passkey=' . $this->passkey;
            $fields .= '&forgotten=1';

            $url .= $fields;
        } else {
            $url .= 'success=1';
        }
        header('Location: ' . $url);
    }

    /**
     * generateAndSavePassKey
     * This function generates a new key for password resetting.
     *
     * @codeCoverageIgnore
     * @param  string $username
     * @return string
     */
    private function generateAndSavePassKey(string $username): string | false
    {
        $alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $key = '';
        do {
            $key = '';
            for ($i = 0; $i < 16; $i++) {
                $key .= $alphabet[rand(0, strlen($alphabet) - 1)];
            }
        } while ($this->persistence->fieldExistsWithValue('password_reset', hash('sha256', $key), 's'));

        if (!$this->persistence->updateUserFieldTo($username, 'password_reset', hash('sha256', $key), 's')) {
            return false;
        }
        return $key;
    }
}
