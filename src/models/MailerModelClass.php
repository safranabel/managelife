<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Mailer;

use Managelife\Model\BaseModel;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * MailerModel
 * This class is responsible for logic during sending emails.
 */
class MailerModel extends BaseModel
{
    private string $htmlBody = '';
    private string $nonHtmlBody = '';
    private string $address = '';
    private string $subject = '';

    /**
     * setHTMLBody
     * This function sets the HTML body for the email.
     *
     * @param string $text
     * @return void
     */
    public function setHTMLBody(string $text): void
    {
        $this->htmlBody = $text;
    }

    /**
     * setNonHTMLBody
     * This function sets the pure (not HTML) body for the email.
     *
     * @param  string $text
     * @return void
     */
    public function setNonHTMLBody(string $text): void
    {
        $this->nonHtmlBody = $text;
    }

    /**
     * setAddress
     * This function sets the address of the receiver.
     *
     * @param  string $address
     * @return void
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * setSubject
     * This function sets the subject of the email.
     *
     * @param  string $subject
     * @return void
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * send
     * This function sends the email to the user.
     *
     * @return bool
     */
    public function send(): bool
    {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'managelifeteam3@gmail.com';
            $mail->Password   = 'uu4Rtx7Uklmm12307kkk&kjjjA';
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('managelifeteam3@gmail.com', 'ManagE-Life Service Team');
            $mail->addAddress($this->address);


            //Content
            $mail->isHTML(true);
            $mail->Subject = $this->subject;
            $mail->Body    = $this->htmlBody;
            $mail->AltBody = $this->nonHtmlBody;

            $mail->send();
            return true;
        } catch (Exception $e) {
            return mail(
                $this->address,
                $this->subject,
                $this->htmlBody . "\n\n\n" . $this->nonHtmlBody
            );
        }
    }
}
