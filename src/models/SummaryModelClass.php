<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Summaries;

use DateTime;
use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Planning\PlanningModel;
use Managelife\Summaries\SummaryPersistence;
use Managelife\UserCheck\UserCheckModel;

/**
 * SummaryModel
 * This class is responsible for logic during generating summaries.
 */
class SummaryModel extends PlanningModel
{
    protected UserHandlerPersistence $persistence;
    private UserCheckModel $userCheckModel;

    /**
     * __construct
     *
     * @param  UserCheckModel $userCheckModel
     * @return void
     */
    public function __construct(UserCheckModel $userCheckModel)
    {
        $this->userCheckModel = $userCheckModel;
        $this->persistence = new SummaryPersistence();
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * getUserCurrencies
     * This function returns the currencies that have been used by the user.
     *
     * @return array<string>
     */
    public function getUserCurrencies(): array
    {
        return $this->persistence->getUserCurrencies($this->userCheckModel->getUserid());
    }

    /**
     * getUserCategories
     * This function returns the categories that have been used by the user.
     *
     * @return array<string>
     */
    public function getUserCategories(): array
    {
        $categories = $this->persistence->getUserCategories($this->userCheckModel->getUserid());
        sort($categories, SORT_STRING);
        return $categories;
    }

    /**
     * getMonthlyBalance
     * This function returns the monthly balance of the user in the given month, in the given currency.
     *
     * @param  DateTime $date
     * @param  string $currency
     * @return float
     */
    public function getMonthlyBalance(DateTime $date, string $currency): float
    {
        $date->setDate(intval($date->format('Y')), intval($date->format('m')), 1);
        $startDate = strval($date->format('Y-m-d'));
        $date->modify('+1 month');
        $finalDate = strval($date->format('Y-m-d'));
        $result = 0;
        foreach ($this->getUserCategories() as $category) {
            $result += $this->persistence->getUserSumBetweenDatesWithCurrency(
                $this->userCheckModel->getUserid(),
                $startDate,
                $finalDate,
                $currency,
                $category,
                1
            );
            $result -= $this->persistence->getUserSumBetweenDatesWithCurrency(
                $this->userCheckModel->getUserid(),
                $startDate,
                $finalDate,
                $currency,
                $category,
                0
            );
        }
        return $result;
    }

    /**
     * getUserCategoriesWithIncome
     * This function returns the categories that the user had expenditures/incomes in.
     * It returns the ones where the user had expenditures if 0 given to $income as value.
     * It returns the ones where the user had incomes if 1 given to $income as value.
     *
     * @param  int $income
     * @return array<string>
     */
    public function getUserCategoriesWithIncome(int $income): array
    {
        $categories = $this->persistence->getUserCategoriesWhereIncome($this->userCheckModel->getUserid(), $income);
        sort($categories, SORT_STRING);
        return $categories;
    }

    /**
     * getUserCurrenciesWithIncome
     * This function returns the currencies that the user had expenditures/incomes in.
     * It returns the ones where the user had expenditures if 0 given to $income as value.
     * It returns the ones where the user had incomes if 1 given to $income as value.
     *
     * @param  int $income
     * @return array<string>
     */
    public function getUserCurrenciesWithIncome(int $income): array
    {
        return $this->persistence->getUserCurrenciesWhereIncome($this->userCheckModel->getUserid(), $income);
    }

    /**
     * getPlan
     * This function returns the amount for the given plan.
     *
     * @param  int $year
     * @param  int $month
     * @param  bool $income
     * @param  string $category
     * @param  string $currency
     * @return float
     */
    public function getPlan(int $year, int $month, bool $income, string $category, string $currency): float
    {
        $res = $this->persistence->getPlanAmount(
            $this->userCheckModel->getUserid(),
            $year,
            $month,
            $income,
            $category,
            $currency
        );
        if (!$res) {
            return 0;
        }
        return $res;
    }

    /**
     * getIncome
     * This function returns the sum of the incomes in the given category and currency, between the given dates.
     *
     * @param  string $startDate
     * @param  string $finalDate
     * @param  string $currency
     * @param  string $category
     * @return float
     */
    public function getIncome(string $startDate, string $finalDate, string $currency, string $category): float
    {
        $result = $this->persistence->getUserSumBetweenDatesWithCurrency(
            $this->userCheckModel->getUserid(),
            $startDate,
            $finalDate,
            $currency,
            $category,
            1
        );
        return $result;
    }

    /**
     * getExpenditure
     * This function returns the sum of the expenditures in the given category and currency, between the given dates.
     *
     * @param  string $startDate
     * @param  string $finalDate
     * @param  string $currency
     * @param  string $category
     * @return float
     */
    public function getExpenditure(string $startDate, string $finalDate, string $currency, string $category): float
    {
        $result = $this->persistence->getUserSumBetweenDatesWithCurrency(
            $this->userCheckModel->getUserid(),
            $startDate,
            $finalDate,
            $currency,
            $category,
            0
        );
        return $result;
    }
}
