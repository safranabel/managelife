<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Login;

use Managelife\Model\BaseModel;
use Managelife\Login\LoginPersistence;
use Managelife\Persistence\UserHandlerPersistence;

/**
 * LoginModel
 * This class is responsible for logic during the login process.
 */
class LoginModel extends BaseModel
{
    /**
     * @var array<string>
     */
    private array $errorFields;
    private bool $hasError;
    private UserHandlerPersistence $persistence;
    private string $userid;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->errorFields = [];
        $this->hasError = true;
        $this->persistence = new LoginPersistence();
        $this->userid = '';
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * getErrorFields
     * This function returns the error codes for the errors from the validation.
     *
     * @return array<string>
     */
    public function getErrorFields(): array
    {
        return $this->errorFields;
    }

    /**
     * hasError
     * This function returns whether there was an error in the validation.
     *
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->hasError;
    }

    /**
     * validate
     * This function validates the login attempt.
     *
     * @return void
     */
    public function validate(): void
    {
        $this->hasError = false;
        if ($_POST) {
            if (!isset($_POST['userid']) || is_null($_POST['userid'])) {
                $this->errorFields[] = 'userid';
                $this->hasError = true;
                return;
            } else {
                if (
                    !$this->persistence->userExists($_POST['userid']) &&
                    !$this->persistence->fieldExistsWithValue('email', $_POST['userid'], 's')
                ) {
                    $this->errorFields[] = 'userid';
                    $this->hasError = true;
                    return;
                } else {
                    if ($this->persistence->userExists($_POST['userid'])) {
                        $this->userid = $_POST['userid'];
                    } elseif ($this->persistence->fieldExistsWithValue('email', $_POST['userid'], 's')) {
                        if ($this->persistence->getUserWhereEmail($_POST['userid'])) {
                            $this->userid = strval($this->persistence->getUserWhereEmail($_POST['userid']));
                        } else {
                            $this->errorFields[] = 'userid';
                            $this->hasError = true;
                            return;
                        }
                    } else {
                        $this->errorFields[] = 'userid';
                        $this->hasError = true;
                        return;
                    }
                }
            }
            if (!isset($_POST['password']) || is_null($_POST['password']) || strlen($_POST['password']) < 6) {
                $this->errorFields[] = 'password';
                $this->hasError = true;
                return;
            } else {
                $hash = hash('sha256', $_POST['password']);
                if ($hash != $this->persistence->getFieldOfUser('password_hash', $this->userid)) {
                    $this->errorFields[] = 'password';
                    $this->hasError = true;
                    return;
                }
            }
        } else {
            $this->hasError = true;
        }
    }

    /**
     * loginUser
     * This function logs the user in.
     *
     * @return void
     */
    public function loginUser(): void
    {
        $this->setSessionTokens($this->userid, $this->persistence);
        $_SESSION['username'] = $this->userid;
    }
}
