<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Health;

use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Health\HealthPersistence;
use Managelife\UserCheck\UserCheckModel;

/**
 * HealthModel
 * This class is responsible for managing logic in health management.
 */
class HealthModel extends UserCheckModel
{
    protected UserHandlerPersistence $persistence;
    private bool $hasError = true;
    private string $error = 'Has not validated.';

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new HealthPersistence();
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * hasError
     * This function returns whether there was an error in the validation.
     *
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->hasError;
    }

    /**
     * setHealthGoal
     * This function sets the given health goal to the user.
     * It returns whether the saving was successful.
     *
     * @return bool
     */
    public function setHealthGoal(): bool
    {
        if ($_POST) {
            if (!isset($_GET['type'])) {
                $this->hasError = true;
                $this->error = 'Invalid form';
                return false;
            }
            if ($_GET['type'] == 'weight') {
                if (!$this->validatePosNumericAmount()) {
                    return false;
                }
                if (!$this->persistence->setWeightGoal($this->getUserid(), floatval($_POST['amount']))) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } elseif ($_GET['type'] == 'water') {
                if (!$this->validatePosNumericAmount()) {
                    return false;
                }
                if (!$this->persistence->setWaterGoal($this->getUserid(), floatval($_POST['amount']))) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } elseif ($_GET['type'] == 'calorie') {
                if (!$this->validatePosNumericAmount()) {
                    return false;
                }
                if (!$this->persistence->setCalorieGoal($this->getUserid(), floatval($_POST['amount']))) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } else {
                $this->hasError = true;
                $this->error = 'Invalid form';
                return false;
            }
        } else {
            return false;
        }
        $this->hasError = false;
        $this->error = '';
        return true;
    }

    /**
     * validatePosNumericAmount
     * This function validates the given amount as a positive number.
     *
     * @return bool
     */
    private function validatePosNumericAmount(): bool
    {
        if (!isset($_POST['amount']) || !is_numeric($_POST['amount'])) {
            $this->hasError = true;
            $this->error = 'Invalid amount';
            return false;
        }
        if (floatval($_POST['amount']) < 0) {
            $this->hasError = true;
            $this->error = 'A negative amount is not acceptable';
            return false;
        }
        return true;
    }

    /**
     * validateNumericAmount
     * This function validates the given amount as a number.
     *
     * @return bool
     */
    private function validateNumericAmount(): bool
    {
        if (!isset($_POST['amount']) || !is_numeric($_POST['amount'])) {
            $this->hasError = true;
            $this->error = 'Invalid amount';
            return false;
        }

        return true;
    }

    /**
     * goBackToHealth
     * This function returns the user to the health page with the right error messages.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goBackToHealth(): void
    {
        if ($this->hasError) {
            header('Location: ../health.php?error=' . $this->error);
        } else {
            header('Location: ../health.php');
        }
    }

    /**
     * changeState
     * This function sets the given health state to the user.
     * It returns whether the saving was successful.
     *
     * @return bool
     */
    public function changeState(): bool
    {
        if ($_POST) {
            if (!isset($_GET['type'])) {
                $this->hasError = true;
                $this->error = 'Invalid form';
                return false;
            }
            if ($_GET['type'] == 'weight') {
                if (!$this->validatePosNumericAmount()) {
                    return false;
                }
                if (!$this->persistence->setActualWeight($this->getUserid(), floatval($_POST['amount']))) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } elseif ($_GET['type'] == 'water') {
                if (!$this->validateNumericAmount()) {
                    return false;
                }
                if (
                    !$this->persistence->incrementWater(
                        $this->getUserid(),
                        floatval($_POST['amount']),
                        strval(date("Y-m-d"))
                    )
                ) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } elseif ($_GET['type'] == 'calorie') {
                if (!$this->validateNumericAmount()) {
                    return false;
                }
                if (
                    !$this->persistence->incrementCalorie(
                        $this->getUserid(),
                        floatval($_POST['amount']),
                        strval(date("Y-m-d"))
                    )
                ) {
                    $this->hasError = true;
                    $this->error = 'Could not connect to database, try again later';
                    return false;
                }
            } else {
                $this->hasError = true;
                $this->error = 'Invalid form';
                return false;
            }
        } else {
            return false;
        }
        $this->hasError = false;
        $this->error = '';
        return true;
    }

    /**
     * getWeightGoal
     * This function returns the current weight goal of the user.
     *
     * @return float
     */
    public function getWeightGoal(): float
    {
        $res = -1;
        $goal = $this->persistence->getUserWeightGoal($this->getUserid());
        if ($goal) {
            $res = floatval($goal);
        }
        return $res;
    }

    /**
     * getWaterGoal
     * This function returns the current water goal of the user.
     *
     * @return float
     */
    public function getWaterGoal(): float
    {
        $res = -1;
        $goal = $this->persistence->getUserWaterGoal($this->getUserid());
        if ($goal) {
            $res = floatval($goal);
        }
        return $res;
    }

    /**
     * getCalorieGoal
     * This function returns the current calorie goal of the user.
     *
     * @return float
     */
    public function getCalorieGoal(): float
    {
        $res = -1;
        $goal = $this->persistence->getUserCalorieGoal($this->getUserid());
        if ($goal) {
            $res = floatval($goal);
        }
        return $res;
    }

    /**
     * getActualWaterLevel
     * This function returns the current water level of the user.
     *
     * @return float
     */
    public function getActualWaterLevel(): float
    {
        $res = -1;
        $level = $this->persistence->getActualLevelIn($this->getUserid(), 'water', strval(date('Y-m-d')));
        if ($level) {
            $res = floatval($level);
        }
        return $res;
    }

    /**
     * getActualCalorieLevel
     * This function returns the current calorie level of the user.
     *
     * @return float
     */
    public function getActualCalorieLevel(): float
    {
        $res = -1;
        $level = $this->persistence->getActualLevelIn($this->getUserid(), 'calorie', strval(date('Y-m-d')));
        if ($level) {
            $res = floatval($level);
        }
        return $res;
    }

    /**
     * getActualWeight
     * This function returns the current weight of the user.
     *
     * @return float
     */
    public function getActualWeight(): float
    {
        $res = -1;
        $goal = $this->persistence->getUserActualWeight($this->getUserid());
        if ($goal) {
            $res = floatval($goal);
        }
        return $res;
    }
}
