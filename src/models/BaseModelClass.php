<?php

declare(strict_types=1);

namespace Managelife\Model;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * BaseModel
 * This is the parent class for all model classes.
 */
abstract class BaseModel
{
    /**
     * setSessionTokens
     * This function sets the right session token depending on user permissions.
     * @codeCoverageIgnore
     * @param  string $username
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    protected function setSessionTokens(string $username, UserHandlerPersistence $persistence): void
    {
        if ($persistence->isAdmin($username)) {
            $_SESSION['token'] = UserHandlerPersistence::ADMIN_TOKEN;
        } else {
            $_SESSION['token'] = UserHandlerPersistence::USER_TOKEN;
        }
    }

    /**
     * getCurrencies
     * This function returns all the available currencies in the application.
     *
     * @param  UserHandlerPersistence $persistence
     * @return array<string>
     */
    public function getCurrencies(UserHandlerPersistence $persistence): array
    {
        $currencies = [];
        if ($currencies = $persistence->getCurrencies()) {
            return $currencies;
        }
        return [];
    }
}
