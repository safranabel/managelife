<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Time;

use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Time\TimePersistence;
use Managelife\UserCheck\UserCheckModel;
use DateTime;

/**
 * TimeModel
 * This class is responsible for the logic during time management.
 */
class TimeModel extends UserCheckModel
{
    protected UserHandlerPersistence $persistence;
    /**
     * @var array<string>
     */
    private array $errorFields = ['NotValidated'];
    private bool $hasError = true;
    private string $date = '';
    private string $time = '';
    private string $title = '';
    private string $description = '';

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new TimePersistence();
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * hasError
     * This function returns whether there was an error during the validation.
     *
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->hasError;
    }

    /**
     * getToDos
     * This function returns the user's to-do list.
     *
     * @return array<int, array<string, string>>
     */
    public function getToDos(): array
    {
        $toDos = $this->persistence->getUserToDos($this->getUserid());
        if (!$toDos) {
            return [];
        }
        return $toDos;
    }

    /**
     * todoValid
     * This function validates the given to-do.
     *
     * @return bool
     */
    public function todoValid(): bool
    {
        $this->hasError = false;
        $this->errorFields = [];
        if ($_POST) {
            if (!isset($_POST['tododate']) || is_null($_POST['tododate'])) {
                $this->errorFields[] = 'DateError';
                $this->hasError = true;
            } elseif (
                !$this->validateDate($_POST['tododate'], 'Y-m-d') &&
                !$this->validateDate($_POST['tododate'], 'Y. m. d.') &&
                !$this->validateDate($_POST['tododate'], 'Y/m/d')
            ) {
                $this->errorFields[] = 'DateError';
                $this->hasError = true;
            } else {
                $format = 'Y-m-d';
                if ($this->validateDate($_POST['tododate'], 'Y. m. d.')) {
                    $format = 'Y. m. d.';
                } elseif ($this->validateDate($_POST['tododate'], 'Y/m/d')) {
                    $format = 'Y/m/d';
                }
                if ($d = DateTime::createFromFormat($format, $_POST['tododate'])) {
                    $this->date = $d->format('Y-m-d');
                }
            }


            if (!isset($_POST['todotime']) || is_null($_POST['todotime'])) {
                $this->errorFields[] = 'TimeError';
                $this->hasError = true;
            } elseif (!$this->validateTime($_POST['todotime'])) {
                $this->errorFields[] = 'TimeError';
                $this->hasError = true;
            } else {
                $this->time = $_POST['todotime'];
            }

            if (isset($_POST['tododesc'])  && strlen($_POST['tododesc']) > 200) {
                $this->errorFields[] = 'DescError';
                $this->hasError = true;
            } else {
                if (isset($_POST['tododesc'])  && strlen($_POST['tododesc']) > 0) {
                    $this->description = $_POST['tododesc'];
                }
            }

            if (!isset($_POST['todotitle']) || is_null($_POST['todotitle'])) {
                $this->errorFields[] = 'TitleError';
                $this->hasError = true;
            } elseif (strlen($_POST['todotitle']) > 50 || strlen($_POST['todotitle']) < 1) {
                $this->errorFields[] = 'TitleError';
                $this->hasError = true;
            } else {
                $this->title = $_POST['todotitle'];
            }
        } else {
            $this->errorFields[] = 'DataError';
            $this->hasError = true;
        }
        return !$this->hasError;
    }

    /**
     * saveToDo
     * This function saves the to-do.
     *
     * @return bool
     */
    public function saveToDo(): bool
    {
        if (!$this->hasError) {
            return $this->persistence->addToDo(
                $this->getUserid(),
                $this->date,
                $this->time,
                $this->title,
                $this->description
            );
        }
        return false;
    }

    /**
     * goBack
     * This function redirects the user to the time management page.
     *
     * @codeCoverageIgnore
     * @return void
     */
    public function goBack(): void
    {
        $url = '../time.php?';
        if ($this->hasError) {
            if (!in_array('DateError', $this->errorFields) && $this->date != '') {
                $url .= '&date=' . $this->date;
            }
            if (!in_array('TimeError', $this->errorFields) && $this->time != '') {
                $url .= '&time=' . $this->time;
            }
            if (!in_array('TitleError', $this->errorFields) && $this->title != '') {
                $url .= '&title=' . $this->title;
            }
            if (!in_array('DescError', $this->errorFields) && $this->description != '') {
                $url .= '&desc=' . $this->description;
            }
            $errors = '&errors=';
            foreach ($this->errorFields as $error) {
                $errors .= $error . ',';
            }
            $errors = substr($errors, 0, -1);
            $url .= $errors;
        } else {
            $url .= 'success=1';
        }
        header('Location: ' . $url);
    }

    /**
     * deleteTodo
     * This function deletes the given to-do.
     *
     * @return bool
     */
    public function deleteTodo(): bool
    {
        if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
            return false;
        }
        return $this->persistence->deleteTodo($this->getUserid(), intval($_GET['id']));
    }
}
