<?php

declare(strict_types=1);

namespace Managelife\Model;
namespace Managelife\Registration;

use Managelife\Model\BaseModel;
use Managelife\Persistence\UserHandlerPersistence;
use Managelife\Registration\RegistrationPersistence;
use Managelife\Validator\Validator;

/**
 * RegistrationModel
 * This class is responsible for the logic during the registration.
 */
class RegistrationModel extends BaseModel
{
    /**
     * Replace ThKww367lhTZ4gB3 with your wished registration key!
     */
    private const REG_KEY = 'ThKww367lhTZ4gB3';

    /**
     * @var array<string>
     */
    private array $errorFields;
    private bool $hasError;
    private UserHandlerPersistence $persistence;
    private string $username;
    private string $email;
    private string $forename;
    private string $surname;
    private string $passwordHash;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->errorFields = [];
        $this->hasError = true;
        $this->persistence = new RegistrationPersistence();
        $this->username = '';
        $this->email = '';
        $this->forename = '';
        $this->surname = '';
        $this->passwordHash = '';
    }

    /**
     * setPersistence
     * This function sets a new persistence for the object.
     *
     * @param  UserHandlerPersistence $persistence
     * @return void
     */
    public function setPersistence(UserHandlerPersistence $persistence): void
    {
        $this->persistence = $persistence;
    }

    /**
     * getErrorFields
     * This function returns the error codes for the errors from the validation.
     *
     * @return array<string>
     */
    public function getErrorFields(): array
    {
        return $this->errorFields;
    }

    /**
     * hasError
     * This function returns whether there was an error in the validation.
     *
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->hasError;
    }

    /**
     * validate
     * This function validates the registration attempt.
     *
     * @return void
     */
    public function validate(): void
    {
        $this->hasError = false;
        if ($_POST) {
            if (!isset($_POST['userid']) || is_null($_POST['userid'])) {
                $this->errorFields[] = 'username';
                $this->hasError = true;
            } else {
                if (str_contains($_POST['userid'], ' ') || $this->persistence->userExists($_POST['userid'])) {
                    $this->errorFields[] = 'username';
                    $this->hasError = true;
                } else {
                    $this->username = $_POST['userid'];
                }
            }

            if (!isset($_POST['email']) || is_null($_POST['email'])) {
                $this->errorFields[] = 'email';
                $this->hasError = true;
            } else {
                if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $this->errorFields[] = 'email';
                    $this->hasError = true;
                } elseif ($this->persistence->fieldExistsWithValue('email', $_POST['email'], 's')) {
                    $this->errorFields[] = 'email';
                    $this->hasError = true;
                } else {
                    $this->email = $_POST['email'];
                }
            }

            if (!isset($_POST['forename']) || is_null($_POST['forename'])) {
                $this->errorFields[] = 'forename';
                $this->hasError = true;
            } else {
                $this->forename = $_POST['forename'];
            }

            if (!isset($_POST['surname']) || is_null($_POST['surname'])) {
                $this->errorFields[] = 'surname';
                $this->hasError = true;
            } else {
                $this->surname = $_POST['surname'];
            }

            if (!Validator::validatePassword($_POST['password'])) {
                $this->errorFields[] = 'password';
                $this->hasError = true;
            } else {
                if (!isset($_POST['passagain']) || is_null($_POST['passagain'])) {
                    $this->errorFields[] = 'passagain';
                    $this->hasError = true;
                } elseif ($_POST['password'] != $_POST['passagain']) {
                    $this->errorFields[] = 'passagain';
                    $this->hasError = true;
                } else {
                    $this->passwordHash = hash('sha256', $_POST['password']);
                }
            }

            if (!isset($_POST['regkey']) || is_null($_POST['regkey'])) {
                $this->errorFields[] = 'regkey';
                $this->hasError = true;
            } else {
                if (self::REG_KEY != $_POST['regkey']) {
                    $this->errorFields[] = 'regkey';
                    $this->hasError = true;
                }
            }
        } else {
            $this->hasError = true;
        }
    }

    /**
     * save
     * This function saves the new user.
     *
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->hasError) {
            if (
                $this->persistence->addUser(
                    $this->username,
                    $this->email,
                    $this->forename,
                    $this->surname,
                    $this->passwordHash
                )
            ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * loginUser
     * This function logs the user in.
     *
     * @return void
     */
    public function loginUser(): void
    {
        $this->setSessionTokens($this->username, $this->persistence);
        $_SESSION['username'] = $this->username;
    }
}
