<?php

declare(strict_types=1);

namespace Managelife\UserIndex;

use DateTime;
use Managelife\UserIndex\UserModel;

/**
 * UserViewModel
 * This class is responsible for loading data directly to the view on the user's index page.
 */
class UserViewModel
{
    private UserModel $model;

    /**
     * __construct
     *
     * @param  UserModel $model
     * @return void
     */
    public function __construct(UserModel $model)
    {
        $this->model = $model;
    }

    /**
     * getOwnerForm
     * This function returns the user's forename in possessive form.
     *
     * @return string
     */
    public function getOwnerForm(): string
    {
        if (substr($this->model->getForename(), -1) == 's') {
            return $this->model->getForename() . "'";
        } else {
            return $this->model->getForename() . "'s";
        }
    }

    /**
     * getForename
     * This function returns the user's forename.
     *
     * @return string
     */
    public function getForename(): string
    {
        return $this->model->getForename();
    }

    /**
     * getCurrencyOptions
     * This function returns the available currencies in an option list.
     *
     * @param  string $active - Specifies the selected currency.
     * @return string
     */
    public function getCurrencyOptions(string $active = 'EUR'): string
    {
        $options = '';
        foreach ($this->model->getAllCurrencies() as $currency) {
            $options .= '<option' . (($currency == $active) ? ' selected' : '')  . '>' . $currency . '</option>';
        }
        return $options;
    }

    /**
     * getUserCategories
     * This function returns the available categories in an option list.
     *
     * @param  string $active - Specifies the selected currency.
     * @return string
     */
    public function getUserCategories(string $active = ''): string
    {
        $options = '';
        foreach ($this->model->getUserCategories() as $category) {
            $options .= '<option' . (($category == $active) ? ' selected' : '')  . '>' . $category . '</option>';
        }
        return $options;
    }

    /**
     * getDate
     * This function returns the current date.
     *
     * @return string
     */
    public function getDate(): string
    {
        $actualDate = new DateTime();
        return $actualDate->format('Y-m-d');
    }

    /**
     * getTime
     * This function returns the current time.
     *
     * @return string
     */
    public function getTime(): string
    {
        $actualDate = new DateTime();
        return $actualDate->format('H:i');
    }

    /**
     * getIncomesInTable
     * This function returns the summary table of incomes in the given month.
     *
     * @return string
     */
    public function getIncomesInTable(): string
    {
        return $this->buildSummaryTable(true, strval(date("Y")), strval(date("m")));
    }

    /**
     * getExpendituresInTable
     * This function returns the summary table of expenditures in the given month.
     *
     * @return string
     */
    public function getExpendituresInTable(): string
    {
        return $this->buildSummaryTable(false, strval(date("Y")), strval(date("m")));
    }

    /**
     * getMothlyBalance
     * This function returns the current monthly balance in every used currency.
     *
     * @return string
     */
    public function getMothlyBalance(): string
    {
        $result = '';
        foreach ($this->model->getUserCurrencies() as $currency) {
            $result .= $currency . ': ' . $this->model->getMonthlyBalance(new DateTime(), $currency) . ' | ';
        }
        $result = substr_replace($result, "", -3);
        return $result;
    }

    /**
     * getErrorList
     * This function returns the errors from the transaction adding attempts in showable form.
     *
     * @return string
     */
    public function getErrorList(): string
    {
        if (!isset($_GET['errors']) || $_GET['errors'] == '') {
            return '';
        }
        $ul = '<ul class="error">';
        $errors = explode(',', $_GET['errors']);
        foreach ($errors as $error) {
            $ul .= '<li>' . $this->getTextForError($error) . '</li>';
        }
        $ul .= '</ul>';
        return $ul;
    }

    /**
     * getHistoryTable
     * This function returns all transactions of the user in table form.
     *
     * @return string
     */
    public function getHistoryTable(): string
    {
        $table = '<table class="table table-dark table-striped table-hover">';
        $table .= '<tr>';
        $table .= '<th>Date</th> <th>Time</th> <th>Category</th> <th>Amount</th> <th>Comment</th>';
        $table .= '</tr>';
        foreach ($this->model->getUserTransactions() as $tran) {
            $table .= '<tr>';
            $table .= '<td>' . $tran['date'] . '</td>';
            $table .= '<td>' . $tran['time'] . '</td>';
            $table .= '<td>' . $tran['category'] . '</td>';
            $table .= '<td>'
                        . (($tran['income'] == '1') ? '+' : '-') . $tran['amount'] . ' ' . $tran['currency'] .
                      '</td>';
            $table .= '<td>' . $tran['comment'] . '</td>';
            $table .= '</tr>';
        }
        $table .= '</table>';
        return $table;
    }

    /**
     * successMessage
     * This function returns the success message if there was.
     *
     * @return string
     */
    public function successMessage(): string
    {
        if (isset($_GET['success']) && $_GET['success'] == '1') {
            return '<h3> &#9989; You have successfully registered your transaction!</h3>';
        }
        return '';
    }

    /**
     * getBalance
     * This function returns the all-time balance of the user in every used currency.
     *
     * @return string
     */
    public function getBalance(): string
    {
        $result = '';
        foreach ($this->model->getUserCurrencies() as $currency) {
            $result .= $currency . ': ' . $this->model->getBalance($currency) . ' | ';
        }
        $result = substr_replace($result, "", -3);
        return $result;
    }

    /**
     * buildSummaryTable
     * This function builds the summary table for the given month.
     *
     * @param  bool $income - It specifies whether the table summarises the incomes or the expenditures.
     * @param  string $year
     * @param  string $month
     * @return string
     */
    public function buildSummaryTable(bool $income, string $year, string $month): string
    {
        $categories = $this->model->getUserCategoriesWithIncome(intval($income));
        $currencies = $this->model->getUserCurrenciesWithIncome(intval($income));
        $table = '';
        $table .= '<table class="table table-dark table-striped  table-hover">';
        $thead = '';
        $thead .= '<tr>';
        foreach ($categories as $category) {
            $thead .= '<th>' . $category . '</th>';
        }
        $thead .= '</tr>';
        $tbody = '';
        $date = new DateTime($year . '-' . $month . '-01');
        $date->setDate(intval($date->format('Y')), intval($date->format('m')), 1);
        $startDate = strval($date->format('Y-m-d'));
        $date->modify('+1 month');
        $finalDate = strval($date->format('Y-m-d'));
        $year = intval(date('Y', intval(strtotime($startDate))));
        $month = intval(date('n', intval(strtotime($startDate))));
        foreach ($currencies as $currency) {
            $tbody .= '<tr>';
            foreach ($categories as $category) {
                $celltext = '';
                $actual = 0;
                $planned = $this->model->getPlan(
                    $year,
                    $month,
                    $income,
                    $category,
                    $currency
                );
                if ($income) {
                    $actual = $this->model->getIncome($startDate, $finalDate, $currency, $category);
                } else {
                    $actual = $this->model->getExpenditure($startDate, $finalDate, $currency, $category);
                }
                $celltext = '<span title="Actual value">' . $actual . '</span>' .
                            '<span title="slash"> / </span>' .
                            '<span title="Planned value">' . $planned . '</span> ' .
                            '<span title="currency">' . $currency . '</span>';
                $addClass = '';
                if (!$income) {
                    if ($planned > 0 && $actual / $planned > 1) {
                        $addClass = ' class="bg-danger-light text-dark"';
                    } elseif ($planned > 0 && $actual / $planned < 0.25) {
                        $addClass = ' class="bg-success-light text-dark"';
                    } elseif ($planned > 0 && $actual / $planned > 0.75) {
                        $addClass = ' class="bg-warning text-dark"';
                    } elseif ($planned <= 0 && $actual > 0) {
                        $addClass = ' class="bg-danger-light text-dark"';
                    }
                } else {
                    if ($planned > 0 && $actual / $planned >= 0.75) {
                        $addClass = ' class="bg-success-light text-dark"';
                    } elseif ($planned > 0 && $actual / $planned < 0.25) {
                        $addClass = ' class="bg-danger-light text-dark"';
                    } elseif ($planned > 0 && $actual / $planned < 0.75) {
                        $addClass = ' class="bg-warning text-dark"';
                    } elseif ($planned <= 0 && $actual > 0) {
                        $addClass = ' class="bg-success-light text-dark"';
                    }
                }

                $tbody .= '<td' . $addClass . '>' . $celltext . '</td>';
            }
            $tbody .= '</tr>';
        }
        $table .= $thead;
        $table .= $tbody;
        $table .= '</table>';
        return $table;
    }

    /**
     * getTextForError
     * This function returns the long description of the given error, that was specified by its code.
     *
     * @param  string $error
     * @return string
     */
    private function getTextForError(string $error): string
    {
        $result = '';
        switch ($error) {
            case 'DateError':
                $result = 'Date is in incorrect format or missing';
                break;
            case 'TimeError':
                $result = 'Time is in incorrect format or missing';
                break;
            case 'AmountError':
                $result = 'Amount is in incorrect format or missing';
                break;
            case 'CurrencyError':
                $result = 'Chosen currency is not exist';
                break;
            case 'CatError':
                $result = 'Chosen category is not exist';
                break;
            case 'NewCatError':
                $result = 'Your new category name is incorrect ' .
                '(length of a category should be between 3-99 character and should contain only letters)';
                break;
            case 'CommentError':
                $result = 'Your comment text is incorrect ' .
                '(length of a comment should be between 1-249 character)';
                break;
            case 'TypeError':
                $result = 'The type of the transaction is incorrect, it should be income or expenditure';
                break;
            case 'TitleError':
                $result = 'The title of the todo is incorrect, it should be shorter than 50 characters';
                break;
            case 'DescError':
                $result = 'The description of the todo is incorrect, it should be shorter than 200 characters';
                break;
            case 'DataError':
                $result = 'Some error occurred, please try again';
                break;
            default:
                $result = 'Some error occurred, please try again';
                break;
        }
        return $result;
    }
}
