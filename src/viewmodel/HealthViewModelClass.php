<?php

declare(strict_types=1);

namespace Managelife\Health;

use Managelife\Health\HealthModel;
use Managelife\UserIndex\UserViewModel;

/**
 * HealthViewModel
 * This class is responsible for loading data directly to the view on the health management page.
 */
class HealthViewModel extends UserViewModel
{
    protected HealthModel $model;

    /**
     * __construct
     *
     * @param  HealthModel $model
     * @return void
     */
    public function __construct(HealthModel $model)
    {
        $this->model = $model;
    }

    /**
     * getWeightGoal
     * This function returns the user's weight goal.
     *
     * @return string
     */
    public function getWeightGoal(): string
    {
        $out = "";
        $goal = $this->model->getWeightGoal();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getWaterGoal
     * This function returns the user's water goal.
     *
     * @return string
     */
    public function getWaterGoal(): string
    {
        $out = "";
        $goal = $this->model->getWaterGoal();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getCalorieGoal
     * This function returns the user's calorie goal.
     *
     * @return string
     */
    public function getCalorieGoal(): string
    {
        $out = "";
        $goal = $this->model->getCalorieGoal();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getActualWaterLevel
     * This function returns the user's current daily water level.
     *
     * @return string
     */
    public function getActualWaterLevel(): string
    {
        $out = "";
        $goal = $this->model->getActualWaterLevel();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getWaterProgress
     * This function returns the user's current daily water progress in percentage.
     *
     * @return string
     */
    public function getWaterProgress(): string
    {
        $res = "0";
        $actual = floatval($this->getActualWaterLevel());
        $goal = floatval($this->getWaterGoal());
        if ($goal > 0 && $actual / $goal < 1) {
            $res = strval(($actual / $goal) * 100);
        } elseif ($goal > 0 && $actual / $goal >= 1) {
            $res = "100";
        } else {
            $res = "0";
        }
        return $res;
    }

    /**
     * getActualCalorieLevel
     * This function returns the user's current daily calorie level.
     *
     * @return string
     */
    public function getActualCalorieLevel(): string
    {
        $out = "";
        $goal = $this->model->getActualCalorieLevel();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getCalorieProgress
     * This function returns the user's current daily calorie progress in percentage.
     *
     * @return string
     */
    public function getCalorieProgress(): string
    {
        $res = "0";
        $actual = floatval($this->getActualCalorieLevel());
        $goal = floatval($this->getCalorieGoal());
        if ($goal > 0 && $actual / $goal < 1) {
            $res = strval(($actual / $goal) * 100);
        } elseif ($goal > 0 && $actual / $goal >= 1) {
            $res = "100";
        } else {
            $res = "0";
        }
        return $res;
    }

    /**
     * getActualWeight
     * This function returns the user's current weight.
     *
     * @return string
     */
    public function getActualWeight(): string
    {
        $out = "";
        $goal = $this->model->getActualWeight();
        if ($goal > 0) {
            $out = strval($goal);
        }
        return $out;
    }

    /**
     * getWeightSuccesClass
     * This function returns a CSS class that was determined by how close the user is to their weight goal.
     *
     * @return string
     */
    public function getWeightSuccesClass(): string
    {
        $res = "";
        $actual = floatval($this->getActualWeight());
        $goal = floatval($this->getWeightGoal());
        if ($goal > 0 && abs($actual - $goal) < 3) {
            $res = "text-success";
        } elseif ($goal > 0 && abs($actual - $goal) < 5) {
            $res = "text-warning bg-dark";
        } elseif ($goal > 0) {
            $res = "text-danger";
        }
        return $res;
    }
}
