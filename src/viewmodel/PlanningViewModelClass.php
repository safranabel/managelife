<?php

declare(strict_types=1);

namespace Managelife\Planning;

use Managelife\Planning\PlanningModel;

/**
 * PlanningViewModel
 * This class is responsible for loading data directly to the view during financial planning.
 */
class PlanningViewModel
{
    protected PlanningModel $model;

    /**
     * __construct
     *
     * @param  PlanningModel $model
     * @return void
     */
    public function __construct(PlanningModel $model)
    {
        $this->model = $model;
    }

    /**
     * getYear
     * This function returns the year of the plans.
     *
     * @return string
     */
    public function getYear(): string
    {
        return strval($this->model->getYear());
    }

    /**
     * getMonth
     * This function returns the month of the plans.
     *
     * @return string
     */
    public function getMonth(): string
    {
        $m = $this->model->getMonth();
        if ($m < 10) {
            return '0' . strval($m);
        }
        return strval($m);
    }

    /**
     * getPrevYear
     * This function returns the year for the month before the current plans.
     *
     * @return string
     */
    public function getPrevYear(): string
    {
        $m = $this->model->getMonth();
        $y = $this->model->getYear();
        if ($m == 1) {
            return strval($y - 1);
        }
        return strval($y);
    }

    /**
     * getPrevMonth
     * This function returns the month before the current plans.
     *
     * @return string
     */
    public function getPrevMonth(): string
    {
        $m = $this->model->getMonth();
        if ($m == 1) {
            $m = 12;
        } else {
            $m -= 1;
        }
        if ($m < 10) {
            return '0' . strval($m);
        }
        return strval($m);
    }

    /**
     * getNextYear
     * This function returns the year for the month after the current plans.
     *
     * @return string
     */
    public function getNextYear(): string
    {
        $m = $this->model->getMonth();
        $y = $this->model->getYear();
        if ($m == 12) {
            return strval($y + 1);
        }
        return strval($y);
    }

    /**
     * getNextMonth
     * This function returns the month after the current plans.
     *
     * @return string
     */
    public function getNextMonth(): string
    {
        $m = $this->model->getMonth();
        if ($m == 12) {
            $m = 1;
        } else {
            $m += 1;
        }
        if ($m < 10) {
            return '0' . strval($m);
        }
        return strval($m);
    }
}
