<?php

declare(strict_types=1);

namespace Managelife\ChangePass;

/**
 * ChangePassViewModel
 * This class is responsible for loading data directly to the view during password changing.
 */
class ChangePassViewModel
{
    /**
     * getMessage
     * This function returns the given error/success message in showable format.
     *
     * @return string
     */
    public function getMessage(): string
    {
        if (isset($_GET['success']) && $_GET['success'] == '1') {
            $text = "<p>&#9989; We have successfully changed your password. </p> ";
            return $text;
        }
        if (!isset($_GET['errors']) || $_GET['errors'] == '') {
            return '';
        }
        $ul = '<ul class="error">';
        $errors = explode(',', $_GET['errors']);
        foreach ($errors as $error) {
            $ul .= '<li>' . $this->getTextForError($error) . '</li>';
        }
        $ul .= '</ul>';
        return $ul;
    }

    /**
     * getTextForError
     * This function returns the long description of the given error, that was specified by its code.
     *
     * @param  string $error
     * @return string
     */
    protected function getTextForError(string $error): string
    {
        $result = '';
        switch ($error) {
            case 'PasswordError':
                $result = 'Your given password is incorrect' .
                '(password should contain at least one number, one letter and 6 characters)';
                break;
            case 'PassAgainError':
                $result = 'The two passwords are not the same.';
                break;
            case 'OldPassError':
                $result = 'Your given old password is not correct.';
                break;
            default:
                $result = 'Some error occurred, please try again';
                break;
        }
        return $result;
    }
}
