<?php

declare(strict_types=1);

namespace Managelife\Registration;

use Managelife\Registration\RegistrationModel;

/**
 * RegistrationViewModel
 * This class is responsible for loading data directly to the view during registration.
 */
class RegistrationViewModel
{
    private RegistrationModel $model;

    /**
     * __construct
     *
     * @param  RegistrationModel $model
     * @return void
     */
    public function __construct(RegistrationModel $model)
    {
        $this->model = $model;
    }

    /**
     * usernameIncorrect
     * This function returns whether the given username was incorrect or taken.
     *
     * @return bool
     */
    public function usernameIncorrect(): bool
    {
        return in_array('username', $this->model->getErrorFields());
    }

    /**
     * emailIncorrect
     * This function returns whether the given email address was incorrect or taken.
     *
     * @return bool
     */
    public function emailIncorrect(): bool
    {
        return in_array('email', $this->model->getErrorFields());
    }

    /**
     * passwordIncorrect
     * This function returns whether the given password was incorrect.
     *
     * @return bool
     */
    public function passwordIncorrect(): bool
    {
        return in_array('password', $this->model->getErrorFields());
    }

    /**
     * passAgainIncorrect
     * This function returns whether the retyped password was incorrect.
     *
     * @return bool
     */
    public function passAgainIncorrect(): bool
    {
        return in_array('passagain', $this->model->getErrorFields());
    }

    /**
     * regKeyIncorrect
     * This function returns whether the given registration key was incorrect.
     *
     * @return bool
     */
    public function regKeyIncorrect(): bool
    {
        return in_array('regkey', $this->model->getErrorFields());
    }
}
