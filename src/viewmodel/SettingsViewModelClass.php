<?php

declare(strict_types=1);

namespace Managelife\Settings;

use Managelife\Notification\NotificationModel;
use Managelife\ChangePass\ChangePassViewModel;

/**
 * SettingsViewModel
 * This class is responsible for loading data directly to the view on the settings page.
 */
class SettingsViewModel extends ChangePassViewModel
{
    private NotificationModel $model;

    /**
     * __construct
     *
     * @param  NotificationModel $model
     * @return void
     */
    public function __construct(NotificationModel $model)
    {
        $this->model = $model;
    }

    /**
     * getTextForError
     * This function returns the long description of the given error, that was specified by its code.
     *
     * @param  string $error
     * @return string
     */
    protected function getTextForError(string $error): string
    {
        $result = '';
        switch ($error) {
            case 'PasswordError':
                $result = 'Your given password is incorrect' .
                '(password should contain at least one number, one letter and 6 characters)';
                break;
            case 'PassAgainError':
                $result = 'The two passwords are not the same.';
                break;
            case 'OldPassError':
                $result = 'Your given old password is not correct.';
                break;
            default:
                $result = 'Some error occurred, please try again';
                break;
        }
        return $result;
    }

    /**
     * selectNotifOpt
     * This function returns selected if the given option is the selected notification frequency for the user.
     *
     * @param  string $opt
     * @return string
     */
    public function selectNotifOpt(string $opt): string
    {
        if ($this->model->getSelectedNotificationOption() == $opt) {
            return 'selected';
        }
        return '';
    }
}
