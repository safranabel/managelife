<?php

declare(strict_types=1);

namespace Managelife\PassReset;

/**
 * PassResetViewModel
 * This class is responsible for loading data directly to the view during password resetting.
 */
class PassResetViewModel
{
    /**
     * getMessage
     * This function returns the error or success message in showable form.
     *
     * @return string
     */
    public function getMessage(): string
    {
        if (isset($_GET['success']) && $_GET['success'] == '1') {
            $text = "<p>&#9989; We have successfully sent the password reset email to your address. ";
            $text .= "<br>";
            $text .= "Do not forget to check your spam folder for the password reset email!</p>";
            return $text;
        }
        if (!isset($_GET['errors']) || $_GET['errors'] == '') {
            return '';
        }
        $ul = '<ul class="error">';
        $errors = explode(',', $_GET['errors']);
        foreach ($errors as $error) {
            $ul .= '<li>' . $this->getTextForError($error) . '</li>';
        }
        $ul .= '</ul>';
        return $ul;
    }

    /**
     * getTextForError
     * This function returns the long description of the given error, that was specified by its code.
     *
     * @param  string $error
     * @return string
     */
    private function getTextForError(string $error): string
    {
        $result = '';
        switch ($error) {
            case 'EmailError':
                $result = 'Your given email is incorrect or does not belong to anyone in our database';
                break;
            case 'ServerError':
                $result = 'A server error occurred, please try again later';
                break;
            default:
                $result = 'Some error occurred, please try again';
                break;
        }
        return $result;
    }
}
