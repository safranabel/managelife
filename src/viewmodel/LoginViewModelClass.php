<?php

declare(strict_types=1);

namespace Managelife\Login;

use Managelife\Login\LoginModel;

/**
 * LoginViewModel
 * This class is responsible for loading data directly to the view during login.
 */
class LoginViewModel
{
    private LoginModel $model;

    /**
     * __construct
     *
     * @param  LoginModel $model
     * @return void
     */
    public function __construct(LoginModel $model)
    {
        $this->model = $model;
    }

    /**
     * usernameOrEmailIncorrect
     * This function returns whether there was an error in the username/email address during validation.
     *
     * @return bool
     */
    public function usernameOrEmailIncorrect(): bool
    {
        return in_array('userid', $this->model->getErrorFields());
    }

    /**
     * passwordIncorrect
     * This function returns whether there was an error in the password during validation.
     *
     * @return bool
     */
    public function passwordIncorrect(): bool
    {
        return in_array('password', $this->model->getErrorFields());
    }
}
