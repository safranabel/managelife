<?php

declare(strict_types=1);

namespace Managelife\Time;

use Managelife\Time\TimeModel;
use Managelife\UserIndex\UserViewModel;

/**
 * TimeViewModel
 * This class is responsible for loading data directly to the view during time management.
 */
class TimeViewModel extends UserViewModel
{
    protected TimeModel $model;

    /**
     * __construct
     *
     * @param  TimeModel $model
     * @return void
     */
    public function __construct(TimeModel $model)
    {
        $this->model = $model;
    }

    /**
     * getToDoRows
     * This function returns the to-dos as table rows (<tr>).
     *
     * @return string
     */
    public function getToDoRows(): string
    {
        $toDos = $this->model->getToDos();
        $trs = "";
        foreach ($toDos as $key => $todo) {
            $trs .= '<tr>';
            $trs .= '<td>' . $todo['title'] . '</td>';
            $trs .= '<td>' . $todo['description'] . '</td>';
            $trs .= '<td>' . $todo['date'] . '</td>';
            $trs .= '<td>' . $todo['time'] . '</td>';
            $trs .= '<td>';
            $trs .= '<form action="./controllers/deletetodo.php?id=' . $todo['id'] . '" method="post">';
            $trs .= '<input type="submit" value="Delete" class="btn btn-danger">';
            $trs .= '</form>';
            $trs .= '</td>';
        }
        return $trs;
    }
}
