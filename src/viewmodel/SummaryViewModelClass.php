<?php

declare(strict_types=1);

namespace Managelife\Summaries;

use DateTime;
use Managelife\Planning\PlanningModel;
use Managelife\Planning\PlanningViewModel;
use Managelife\UserCheck\UserCheckModel;
use Managelife\UserIndex\UserModel;
use Managelife\UserIndex\UserViewModel;

/**
 * SummaryViewModel
 * This class is responsible for loading data directly to the view during checking summaries.
 */
class SummaryViewModel extends PlanningViewModel
{
    protected PlanningModel $model;
    private SummaryModel $summaryModel;
    private UserViewModel $userViewModel;

    /**
     * __construct
     *
     * @param  SummaryModel $model
     * @param  UserCheckModel $userCheckModel
     * @return void
     */
    public function __construct(SummaryModel $model, UserCheckModel $userCheckModel)
    {
        $UserModel = new UserModel();
        $UserModel->setUserData(
            $userCheckModel->getUserid(),
            $userCheckModel->getEmail(),
            $userCheckModel->getForename(),
            $userCheckModel->getSurname()
        );
        $this->userViewModel = new UserViewModel($UserModel);
        $this->model = $model;
        $this->summaryModel = $model;
    }

    /**
     * getNextYear
     * This function returns the year for the next month compared to the selected.
     *
     * @return string
     */
    public function getNextYear(): string
    {
        if (intval(date('Y')) <= intval(parent::getNextYear())) {
            return strval(date('Y'));
        }
        return parent::getNextYear();
    }

    /**
     * getNextMonth
     * This function returns the next month compared to the selected.
     *
     * @return string
     */
    public function getNextMonth(): string
    {
        if (intval(date('Y')) < intval(parent::getNextYear())) {
            return strval(date('m'));
        }
        if (intval(date('Y')) == intval(parent::getNextYear())) {
            if (intval(date('m')) <= intval(parent::getNextMonth())) {
                return strval(date('m'));
            }
        }
        return parent::getNextMonth();
    }

    /**
     * getIncomesInTable
     * This function returns the summary table for incomes.
     *
     * @codeCoverageIgnore
     * Covered in userViewModel
     * @return string
     */
    public function getIncomesInTable(): string
    {
        return $this->userViewModel->buildSummaryTable(true, $this->getYear(), $this->getMonth());
    }

    /**
     * getExpendituresInTable
     * This function returns the summary table for expenditures.
     *
     * @codeCoverageIgnore
     * Covered in userViewModel
     * @return string
     */
    public function getExpendituresInTable(): string
    {
        return $this->userViewModel->buildSummaryTable(false, $this->getYear(), $this->getMonth());
    }

    /**
     * getMothlyBalance
     * This function returns the balance of the chosen month.
     *
     * @return string
     */
    public function getMothlyBalance(): string
    {
        $result = '';
        $date = new DateTime($this->getYear() . '-' . $this->getMonth() . '-01');
        foreach ($this->summaryModel->getUserCurrencies() as $currency) {
            $result .= $currency . ': ' . $this->summaryModel->getMonthlyBalance($date, $currency) . ' | ';
        }
        $result = substr_replace($result, "", -3);
        return $result;
    }
}
