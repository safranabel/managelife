<?php

declare(strict_types=1);

if (!isset($changePass) || !$changePass) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

use Managelife\ChangePass\ChangePassViewModel;
use Managelife\PassReset\PassResetModel;

$model = new PassResetModel();
$viewModel = new ChangePassViewModel();
if ($_POST) {
    if ($model->validatePassChange()) {
        $model->saveNewPass();
    }
    $model->goChangePass();
} else {
    include './view/changepass.php';
}
