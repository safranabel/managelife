<?php

declare(strict_types=1);

session_start();

if (!isset($planning) || !$planning) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";


use Managelife\Planning\PlanningModel;
use Managelife\Planning\PlanningViewModel;

$model = new PlanningModel();
$viewModel = new PlanningViewModel($model);
include "./view/planning.php";
