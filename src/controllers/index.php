<?php

declare(strict_types=1);

session_start();

if (!isset($index) || !$index) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

use Managelife\UserCheck\UserCheckModel;
use Managelife\UserIndex\UserModel;
use Managelife\UserIndex\UserViewModel;
use Managelife\Notification\NotificationUpdateModel;

$userCheckModel = new UserCheckModel();
if (!isset($_SESSION['username']) || !isset($_SESSION['token'])) {
    $viewModel = true;
    include './view/unlogged.index.php';
} else {
    $userCheckModel->refreshUserData();
    if ($userCheckModel->isAdmin()) {
        include './view/admin.index.php';
    } elseif ($userCheckModel->userValid()) {
        $userModel = new UserModel();
        $userModel->setUserData(
            $userCheckModel->getUserid(),
            $userCheckModel->getEmail(),
            $userCheckModel->getForename(),
            $userCheckModel->getSurname()
        );
        $fromIndexController = true;
        $viewModel = new UserViewModel($userModel);
        NotificationUpdateModel::update();
        include './view/user.index.php';
    } else {
        session_unset();
        session_destroy();
        $viewModel = true;
        include './view/unlogged.index.php';
    }
}
