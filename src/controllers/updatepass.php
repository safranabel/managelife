<?php

declare(strict_types=1);

if (!$_POST) {
    die('Error: Failed authentication!');
}

session_start();

$prefix = '../';
include "../srcloader.php";

use Managelife\PassUpdate\PassUpdateModel;

$model = new PassUpdateModel();

$model->refreshUserData();

if ($model->userValid()) {
    if ($model->isValid()) {
        $model->saveNewPass();
    }
    $model->goBack();
} else {
    header("Location: ../settings.php?error=UserError");
}
