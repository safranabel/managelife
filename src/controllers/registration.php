<?php

declare(strict_types=1);

if (!isset($registration) || !$registration) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

use Managelife\Registration\RegistrationModel;
use Managelife\Registration\RegistrationViewModel;

$model = new RegistrationModel();
$viewModel = new RegistrationViewModel($model);
if ($_POST) {
    $model->validate();
    if (!$model->hasError()) {
        if ($model->save()) {
            session_start();
            $model->loginUser();
            header('Location: index.php');
        } else {
            include "./view/registration.php";
        }
    } else {
        include "./view/registration.php";
    }
} else {
    include "./view/registration.php";
}
