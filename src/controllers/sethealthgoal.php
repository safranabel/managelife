<?php

declare(strict_types=1);

session_start();

$prefix = '../';
require_once "../srcloader.php";

use Managelife\Health\HealthModel;

$model = new HealthModel();
$model->refreshUserData();

if ($model->userValid()) {
    $model->setHealthGoal();
    $model->goBackToHealth();
}
$model->goBackToHealth();
