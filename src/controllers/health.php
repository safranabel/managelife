<?php

declare(strict_types=1);

session_start();

if (!isset($health) || !$health) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";


use Managelife\Health\HealthModel;
use Managelife\Health\HealthViewModel;

$model = new HealthModel();
$model->refreshUserData();
if ($model->userValid()) {
    $viewModel = new HealthViewModel($model);
    include "./view/health.php";
}
