<?php

declare(strict_types=1);

session_start();

if (!isset($summaries) || !$summaries) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";


use Managelife\Summaries\SummaryModel;
use Managelife\Summaries\SummaryViewModel;
use Managelife\UserCheck\UserCheckModel;

$userCheckModel = new UserCheckModel();
$userCheckModel->refreshUserData();
if ($userCheckModel->userValid()) {
    $model = new SummaryModel($userCheckModel);
    $viewModel = new SummaryViewModel($model, $userCheckModel);
    include "./view/oldsummaries.php";
}
