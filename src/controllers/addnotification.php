<?php

declare(strict_types=1);

session_start();

$prefix = '../';
include "../srcloader.php";

use Managelife\Notification\NotificationModel;
use Managelife\Notification\NotificationUpdateModel;
$model = new NotificationModel();
$model->refreshUserData();
if ($model->userValid()) {
    if ($model->notifSettingValid()) {
        if ($model->saveNotifSetting()) {
            NotificationUpdateModel::update();
            header('Location: ../settings.php?notif=success');
        } else {
            header('Location: ../settings.php?notif=error');
        }
    } else {
        header('Location: ../settings.php?notif=error');
    }
} else {
    header('Location: ../settings.php');
}
