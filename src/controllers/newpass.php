<?php

declare(strict_types=1);

$prefix = '../';
include '../srcloader.php';

use Managelife\PassReset\PassResetModel;
$model = new PassResetModel();
if ($model->isValid()) {
    $model->sendMail();
}
$model->goBack();
