<?php

declare(strict_types=1);

session_start();

$prefix = '../';
include "../srcloader.php";

use Managelife\Transaction\TransactionModel;
$model = new TransactionModel();
$model->refreshUserData();
if ($model->userValid()) {
    if ($model->transactionValid()) {
        $model->saveTransaction();
        $model->goToIndex();
    } else {
        $model->goToIndex();
    }
} else {
    $model->goToIndex();
}
