<?php

declare(strict_types=1);

use Managelife\Settings\SettingsViewModel;
use Managelife\Notification\NotificationModel;

session_start();

if (!isset($settings) || !$settings) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

$model = new NotificationModel();
$model->refreshUserData();
if ($model->userValid()) {
    $viewModel = new SettingsViewModel($model);
    include './view/settings.php';
}
