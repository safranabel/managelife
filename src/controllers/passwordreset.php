<?php

declare(strict_types=1);

if (!isset($passreset) || !$passreset) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

use Managelife\PassReset\PassResetModel;
use Managelife\PassReset\PassResetViewModel;

$model = new PassResetModel();
$viewModel = new PassResetViewModel();
include './view/passwordreset.php';
