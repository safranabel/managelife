<?php

declare(strict_types=1);

session_start();

if (!isset($time) || !$time) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";


use Managelife\Time\TimeModel;
use Managelife\Time\TimeViewModel;

$model = new TimeModel();
$model->refreshUserData();
if ($model->userValid()) {
    $viewModel = new TimeViewModel($model);
    include "./view/time.php";
}
