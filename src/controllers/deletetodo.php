<?php

declare(strict_types=1);

session_start();

$prefix = '../';
include "../srcloader.php";

use Managelife\Time\TimeModel;
$model = new TimeModel();
$model->refreshUserData();
if ($model->userValid()) {
    $model->deleteTodo();
}
header('Location: ../time.php');
