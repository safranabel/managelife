<?php

declare(strict_types=1);

if (!isset($login) || !$login) {
    die('Error: Failed authentication!');
}

require_once "./srcloader.php";

use Managelife\Login\LoginModel;
use Managelife\Login\LoginViewModel;

$model = new LoginModel();
$viewModel = new LoginViewModel($model);
if ($_POST) {
    $model->validate();
    if (!$model->hasError()) {
        session_start();
        $model->loginUser();
        header('Location: index.php');
    } else {
        include "./view/login.php";
    }
} else {
    include "./view/login.php";
}
