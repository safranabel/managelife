<?php

declare(strict_types=1);

session_start();

$prefix = '../';
include "../srcloader.php";


use Managelife\Planning\PlanningModel;
use Managelife\Notification\NotificationUpdateModel;

$model = new PlanningModel();
$model->refreshUserData();
if ($model->userValid()) {
    if (isset($_GET["type"]) && $_GET["type"] == "fetch") {
        if (isset($_GET["year"]) && isset($_GET["month"]) && is_numeric($_GET["year"]) && is_numeric($_GET["month"])) {
            echo $model->getTableJSON();
        }
    } elseif (isset($_GET["type"]) && $_GET["type"] == "save") {
        echo $model->saveTable();
    }
} else {
    echo "{}";
}

NotificationUpdateModel::update();
