<?php

namespace Notification;

use Notification\NotificationPersistence;
use DateTime;
use Exception;

/**
 * NotificationModel
 * This class is responsible for logic in the notification service
 */
class NotificationModel
{
    private NotificationPersistence $persistence;
    /**
     * @var array<int>
     */
    private array $sent;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->persistence = new NotificationPersistence();
        $this->sent = [];
    }

    /**
     * sendToAll
     * This function sends emails to the users that should be notified.
     *
     * @return void
     */
    public function sendToAll(): void
    {
        $notifications = $this->persistence->getCurrentNotifications();
        foreach ($notifications as $index => $noti) {
            if (
                mail(
                    strval($noti['email']),
                    strval($noti['subject']),
                    strval($noti['text']),
                    ['From' => $noti['mailfrom']]
                )
            ) {
                $this->sent[] = intval($noti['id']);
            }
        }
        $this->persistence->deleteSent($this->sent);
    }

    /**
     * addNotification
     * This function saves the given notification.
     *
     * @return bool
     */
    public function addNotification(): bool
    {
        if (
            !isset($_POST['key']) ||
            !isset($_POST['email']) ||
            !isset($_POST['subject']) ||
            !isset($_POST['text']) ||
            !isset($_POST['date']) ||
            !isset($_POST['time'])
        ) {
            return false;
        }

        if (!$this->persistence->senderExists($_POST['key'])) {
            return false;
        }

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if (strlen($_POST['subject']) > 70) {
            return false;
        }

        if (strlen($_POST['text']) > 1000) {
            return false;
        }
        try {
            $d = new DateTime($_POST['date']);
        } catch (Exception $e) {
            return false;
        }

        $_POST['date'] = date_format($d, 'Y-m-d');

        if (strpos($_POST['time'], ':') == false) {
            return false;
        }

        $t = explode(':', $_POST['time']);
        if (count($t) !== 2) {
            return false;
        }
        if ($t[0] == "" || !is_numeric($t[0])) {
            return false;
        }
        if ($t[1] == "" || !is_numeric($t[1])) {
            return false;
        }

        if (intval($t[0]) < 0 || 23 < intval($t[0])) {
            return false;
        }

        if (intval($t[1]) < 0 || 59 < intval($t[1])) {
            return false;
        }

        return $this->persistence->addNotification(
            $_POST['email'],
            $_POST['subject'],
            $_POST['text'],
            $_POST['date'],
            $_POST['time'],
            $_POST['key']
        );
    }

    /**
     * deleteNotifications
     * This function deletes the specified notifications.
     *
     * @return bool
     */
    public function deleteNotifications(): bool
    {
        if (!isset($_POST['key']) || !isset($_POST['email'])) {
            return false;
        }
        if (!$this->persistence->senderExists($_POST['key'])) {
            return false;
        }
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return $this->persistence->deleteNotificationsWithEmailFromSender($_POST['email'], $_POST['key']);
    }
}
