<?php

require_once "./persistence.php";
require_once "./model.php";

use Notification\NotificationModel;

$model = new NotificationModel();
if ($model->addNotification()) {
    echo "SUCCESS";
} else {
    echo "FAILED";
}
