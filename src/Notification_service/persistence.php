<?php

namespace Notification;

use mysqli;

/**
 * NotificationPersistence
 * This class handles the direct connection to the notification database.
 */
class NotificationPersistence
{
    private const SERVER_NAME = '';
    private const DB_USER_NAME = '';
    private const DB_USER_PASSWORD = '';
    private const DB_NAME = '';

    /**
     * connect
     * This function builds up the connection with the database.
     *
     * @return mysqli
     */
    private function connect(): mysqli
    {
        $connection = new mysqli(
            NotificationPersistence::SERVER_NAME,
            NotificationPersistence::DB_USER_NAME,
            NotificationPersistence::DB_USER_PASSWORD,
            NotificationPersistence::DB_NAME
        );
        $connection->set_charset('utf8mb4');
        return $connection;
    }

    /**
     * getCurrentNotifications
     * This function returns the notifications that are currently waiting for sending.
     *
     * @return array<int, array<string, int|string>>
     */
    public function getCurrentNotifications(): array
    {
        $notifications = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        date_default_timezone_set("Europe/Budapest");
        $currentDate = strval(date("Y-m-d"));
        $currentTime = strval(date("H:i"));
        $query  = "SELECT notification.email as email, notification.subject as subject, notification.text as text, ";
        $query .= "service_users.email as mailfrom, notification.id as id ";
        $query .= "FROM notification INNER JOIN service_users ON notification.sender = service_users.id ";
        $query .= "WHERE (notification.send_date < '" . $currentDate . "') OR ";
        $query .= "(notification.send_date = '" . $currentDate;
        $query .= "' AND notification.send_time <= '" . $currentTime . "')";
        if ($stmt->prepare($query)) {
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                $i = 0;
                while ($resAssoc = $result->fetch_assoc()) {
                    $notifications[$i] = $resAssoc;
                    $i++;
                }
            }
            $stmt->close();
        }
        $con->close();
        return $notifications;
    }

    /**
     * deleteSent
     * This function deletes the notifications that are specified in $sent.
     *
     * @param  array<int> $sent
     * @return void
     */
    public function deleteSent(array $sent): void
    {
        foreach ($sent as $key => $value) {
            $this->deleteNotificationWithId($value);
        }
    }

    /**
     * deleteNotificationWithId
     * This function deletes the notification with the given ID.
     *
     * @param  int $id
     * @return bool
     */
    private function deleteNotificationWithId(int $id): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "DELETE FROM notification ";
        $query .= "WHERE id=?;";
        if ($stmt->prepare($query)) {
            $stmt->bind_param('i', $id);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * senderExists
     * This function returns whether a sender exists with the given key.
     *
     * @param  string $key
     * @return bool
     */
    public function senderExists(string $key): bool
    {
        $rownum = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT * FROM service_users WHERE user_key=?")) {
            $stmt->bind_param("s", $key);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->store_result();
            $rownum = $stmt->num_rows();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $rownum > 0;
    }

    /**
     * addNotification
     * This function adds a notification to the database.
     *
     * @param string $email
     * @param string $subject
     * @param string $text
     * @param string $date
     * @param string $time
     * @param string $key
     * @return bool
     */
    public function addNotification(
        string $email,
        string $subject,
        string $text,
        string $date,
        string $time,
        string $key
    ): bool {
        $uid = $this->getUserForKey($key);
        if ($uid <= 0) {
            return false;
        }
        $query = "INSERT INTO notification(email, subject, text, send_date, send_time, sender) ";
        $query .= "VALUES (?, ?, ?, ?, ?, ?);";
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare($query)) {
            $stmt->bind_param("sssssi", $email, $subject, $text, $date, $time, $uid);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserForKey
     * This function returns the user that uses the given key.
     *
     * @param  string $key
     * @return int
     */
    private function getUserForKey(string $key): int
    {
        $user = -1;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT id ";
        $query .= "FROM service_users ";
        $query .= "WHERE  user_key=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param('s', $key);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $user = $resAssoc['id'];
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
            $stmt->close();
        } else {
            return -1;
        }
        $con->close();
        return intval($user);
    }

    /**
     * deleteNotificationsWithEmailFromSender
     * This function deletes all notifications addressed to the given email.
     *
     * @param  string $email
     * @param  string $key
     * @return bool
     */
    public function deleteNotificationsWithEmailFromSender(string $email, string $key): bool
    {
        $uid = $this->getUserForKey($key);
        if ($uid <= 0) {
            return false;
        }
        $query = "DELETE FROM notification ";
        $query .= "WHERE email=? AND sender=?;";
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare($query)) {
            $stmt->bind_param("si", $email, $uid);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }
}
