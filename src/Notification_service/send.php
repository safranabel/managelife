<?php

require_once "./persistence.php";
require_once "./model.php";
use Notification\NotificationModel;

$model = new NotificationModel();
$model->sendToAll();
