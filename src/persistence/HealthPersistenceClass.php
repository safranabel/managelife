<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Health;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * HealthPersistence
 * This class represents the persistence layer of health features.
 */
class HealthPersistence extends UserHandlerPersistence
{
}
