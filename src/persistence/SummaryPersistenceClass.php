<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Summaries;

use Managelife\Persistence\BasePersistence;
use Managelife\Persistence\UserHandlerPersistence;

/**
 * SummaryPersistence
 * This class represents the persistence layer of summary features.
 */
class SummaryPersistence extends UserHandlerPersistence
{
}
