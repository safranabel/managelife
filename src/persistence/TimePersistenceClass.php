<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Time;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * TimePersistence
 * This class represents the persistence layer of time management features.
 */
class TimePersistence extends UserHandlerPersistence
{
}
