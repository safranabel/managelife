<?php

declare(strict_types=1);

namespace Managelife\Persistence;

use Managelife\Persistence\DBConnInfo;
use mysqli;

/**
 * BasePersistence
 * This class is a parent class for all classes in the persistence.
 * This class contains all basic methods and information that is used by all other classes in the persistence.
 */
abstract class BasePersistence
{
    /**
     * connect
     * This function sets up the connection between the PHP and the database.
     *
     * @return mysqli
     */
    protected function connect(): mysqli
    {
        $connection = new mysqli(
            DBConnInfo::getServername(),
            DBConnInfo::getUsername(),
            DBConnInfo::getPassword(),
            DBConnInfo::getDBName()
        );
        $connection->set_charset('utf8mb4');
        return $connection;
    }

    /**
     * getCurrencies
     * This function returns all the currencies that are available in the application.
     *
     * @return false|array<string>
     */
    public function getCurrencies(): false|array
    {
        $currencies = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT distinct name FROM currencies")) {
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $currencies[] = $resAssoc['name'];
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $currencies;
    }

    /**
     * getCurrencyId
     * This function returns the ID of the given currency.
     *
     * @param  string $currencyName
     * @return int|false
     */
    public function getCurrencyId(string $currencyName): int | false
    {
        $res = '';
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT id FROM currencies WHERE name=?")) {
            $stmt->bind_param("s", $currencyName);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                $resAssoc = $result->fetch_assoc();
                if ($resAssoc) {
                    $res = $resAssoc["id"];
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return intval($res);
    }
}
