<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Notification;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * NotificationPersistence
 * This class deals with the connection with the notification server.
 */
class NotificationPersistence extends UserHandlerPersistence
{
    /** Replace dadf14403b6ef507a... with your application key at the notification server! */
    public const NOTIFICATION_KEY = "dadf14403b6ef507a2085727b2b8af2317f5e5c6a7c427c192e3109d60f9d526";
    /** Replace https://www.safranabel.com/notifications/ with the link to the actual notification service! */
    public const NOTIFICATION_SERVICE_URL = "https://www.safranabel.com/notifications/";

    /**
     * addNotification
     * This function adds a notification to the notifications at the notification server.
     *
     * @param string $email
     * @param string $subject
     * @param string $text
     * @param string $date
     * @param string $time
     * @return bool
     */
    public function addNotification(
        string $email,
        string $subject,
        string $text,
        string $date,
        string $time
    ): bool {
        $url = NotificationPersistence::NOTIFICATION_SERVICE_URL . 'addnotification.php';
        $data = array(
            'email' => $email,
            'subject' => $subject,
            'text' => $text,
            'date' => $date,
            'time' => $time,
            'key' => NotificationPersistence::NOTIFICATION_KEY
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === false) {
            return false;
        }

        if ($result !== "SUCCESS") {
            return false;
        }
        return true;
    }

    /**
     * deleteNotifications
     * This function deletes all the notifications for the given email address on the notification server.
     *
     * @param  string $email
     * @return bool
     */
    public function deleteNotifications(string $email): bool
    {
        $url = NotificationPersistence::NOTIFICATION_SERVICE_URL . 'unsubscribe.php';
        $data = array(
            'email' => $email,
            'key' => NotificationPersistence::NOTIFICATION_KEY
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === false) {
            return false;
        }

        if ($result !== "SUCCESS") {
            return false;
        }
        return true;
    }
}
