<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Planning;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * PlanningPersistence
 * This class represents the persistence layer of financial planning features.
 */
class PlanningPersistence extends UserHandlerPersistence
{
}
