<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Registration;

use Managelife\Persistence\BasePersistence;
use Managelife\Persistence\UserHandlerPersistence;

/**
 * RegistrationPersistence
 * This class represents the persistence layer of registration.
 */
class RegistrationPersistence extends UserHandlerPersistence
{
}
