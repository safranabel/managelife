<?php

declare(strict_types=1);

namespace Managelife\Persistence;

/**
 * DBConnInfo
 * This class contains all the important information about the database.
 */
class DBConnInfo
{
    /** Replace localhost with the name of your server! */
    private const SERVER_NAME = "localhost";

    /** Replace root with the name of your database user! */
    private const DB_USER_NAME = "root";

    /** Type your database password between the quotation marks! */
    private const DB_USER_PASSWORD = "";

    /** Replace managelife with the name of your database! */
    private const DB_NAME = "managelife";

    /**
     * getServername
     * This function returns the name of the server.
     *
     * @return string
     */
    public static function getServername(): string
    {
        return DBConnInfo::SERVER_NAME;
    }

    /**
     * getUsername
     * This function returns the username of the server user.
     *
     * @return string
     */
    public static function getUsername(): string
    {
        return DBConnInfo::DB_USER_NAME;
    }

    /**
     * getPassword
     * This function returns the password of the server user.
     *
     * @return string
     */
    public static function getPassword(): string
    {
        return DBConnInfo::DB_USER_PASSWORD;
    }

    /**
     * getDBName
     * This function returns the name of the database.
     *
     * @return string
     */
    public static function getDBName(): string
    {
        return DBConnInfo::DB_NAME;
    }
}
