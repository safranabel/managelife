<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\UserCheck;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * UserCheckPersistence
 * This class represents the persistence layer for user validation.
 */
class UserCheckPersistence extends UserHandlerPersistence
{
}
