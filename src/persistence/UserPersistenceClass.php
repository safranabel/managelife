<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\UserIndex;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * UserPersistence
 * This class represents the persistence layer of main user features.
 */
class UserPersistence extends UserHandlerPersistence
{
}
