<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\Login;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * LoginPersistence
 * This class represents the persistence layer of login features.
 */
class LoginPersistence extends UserHandlerPersistence
{
}
