<?php

declare(strict_types=1);

namespace Managelife\Persistence;

use Managelife\Persistence\BasePersistence;

/**
 * UserHandlerPersistence
 * This class gets and sets all the user information in the database.
 * The functions return false or empty (if interpreted) in case of connection error.
 */
abstract class UserHandlerPersistence extends BasePersistence
{
    public const USER_TOKEN = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    public const ADMIN_TOKEN = '07909f5928f186da6c851a32067867313cef84bd80fe94c680f93accb8535297';

    /**
     * userExists
     * This function returns whether the given user exists in the database.
     *
     * @param  string $username
     * @return bool
     */
    public function userExists(string $username): bool
    {
        $rownum = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT * FROM users WHERE username=?")) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $stmt->store_result();
            $rownum = $stmt->num_rows();
            $stmt->close();
        }
        $con->close();
        return $rownum > 0;
    }

    /**
     * fieldExistsWithValue
     * This function returns whether the field exits with the given value.
     *
     * @param  string $fieldName
     * @param  string|bool $value
     * @param  string $valuetype
     * @return bool
     */
    public function fieldExistsWithValue(string $fieldName, string|bool $value, string $valuetype): bool
    {
        $rownum = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT * FROM users WHERE " . $fieldName . "=?")) {
            $stmt->bind_param($valuetype, $value);
            $stmt->execute();
            $stmt->store_result();
            $rownum = $stmt->num_rows();
            $stmt->close();
        }
        $con->close();
        return $rownum > 0;
    }

    /**
     * addUser
     * This function adds the user to the database.
     *
     * @param string $username
     * @param string $email
     * @param string $forename
     * @param string $surname
     * @param string $passwordHash
     * @return bool
     */
    public function addUser(
        string $username,
        string $email,
        string $forename,
        string $surname,
        string $passwordHash
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO users (username, email, forename, surname, password_hash) VALUES (?, ?, ?, ?, ?)";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("sssss", $username, $email, $forename, $surname, $passwordHash);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * isAdmin
     * This function returns whether the given user is an admin.
     *
     * @param  string $username
     * @return bool
     */
    public function isAdmin(string $username): bool
    {
        $rownum = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT * FROM users WHERE username=? AND admin=1")) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $stmt->store_result();
            $rownum = $stmt->num_rows();
            $stmt->close();
        }
        $con->close();
        return $rownum > 0;
    }

    /**
     * getUserWhereEmail
     * This class returns the email address of the given user.
     *
     * @param  string $email
     * @return string|false
     */
    public function getUserWhereEmail(string $email): string | false
    {
        $username = '';
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT username FROM users WHERE email=?")) {
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result != false) {
                $resAssoc = $result->fetch_assoc();
                if ($resAssoc != false) {
                    $username = $resAssoc['username'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $username;
    }

    /**
     * getFieldOfUser
     * This function returns the value of the specified field of the given user.
     *
     * @param  string $field
     * @param  string $username
     * @return string|false
     */
    public function getFieldOfUser(string $field, string $username): string | false
    {
        $res = '';
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT " . $field . " FROM users WHERE username=?")) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                $resAssoc = $result->fetch_assoc();
                if ($resAssoc) {
                    $res = $resAssoc[$field];
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return strval($res);
    }

    /**
     * getUserCategories
     * This function returns the categories of the given user.
     *
     * @param  string $username
     * @return array<string>
     */
    public function getUserCategories(string $username): array
    {
        $categories = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT distinct categories.name as category ";
        $query .= "FROM categories ";
        $query .= "WHERE categories.user_id=";
        $query .= "(SELECT id FROM users WHERE username=?)";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $categories[] = $resAssoc['category'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return $categories;
    }

    /**
     * addCategory
     * This function adds a category to the user in the database.
     *
     * @param  string $categoryname
     * @param  string $username
     * @return bool
     */
    public function addCategory(string $categoryname, string $username): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO categories " .
                 "(name, user_id) " .
                 "VALUES (?, ?)";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $stmt->bind_param("si", $categoryname, $user_id);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserCategoriesWhereIncome
     * This function returns the categories that the given user had expenditures/incomes in.
     * It returns the income categories if the income parameter is set to 1.
     * It returns the expenditure categories if the income parameter is set to 0.
     *
     * @param  string $username
     * @param  int $income
     * @return array<string>
     */
    public function getUserCategoriesWhereIncome(string $username, int $income): array
    {
        $categories = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT distinct categories.name as category ";
        $query .= "FROM transactions ";
        $query .= "INNER JOIN categories ON transactions.category = categories.id ";
        $query .= "INNER JOIN users ON transactions.user_id = users.id ";
        $query .= "WHERE users.username=? AND transactions.income=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("si", $username, $income);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $categories[] = $resAssoc['category'];
                }
            }
            $stmt->close();
        }
        $con->close();

        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT distinct categories.name as category ";
        $query .= "FROM plans ";
        $query .= "INNER JOIN categories ON plans.category = categories.id ";
        $query .= "INNER JOIN users ON plans.user_id = users.id ";
        $query .= "WHERE users.username=? AND plans.income=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("si", $username, $income);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $categories[] = $resAssoc['category'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return array_unique($categories);
    }

    /**
     * getUserCurrencies
     * This function returns the currencies that are used by the user.
     *
     * @param  string $username
     * @return array<string>
     */
    public function getUserCurrencies(string $username): array
    {
        $currencies = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT distinct currencies.name as currency ";
        $query .= "FROM transactions INNER JOIN currencies ON currencies.id=transactions.currency ";
        $query .= "WHERE transactions.user_id=(SELECT id FROM users WHERE users.username=?)";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $currencies[] = $resAssoc['currency'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return $currencies;
    }

    /**
     * getUserCurrenciesWhereIncome
     * This function returns the currencies that the given user had expenditures/incomes in.
     * It returns the income currencies if the income parameter is set to 1.
     * It returns the expenditure currencies if the income parameter is set to 0.
     *
     * @param  string $username
     * @param  int $income
     * @return array<string>
     */
    public function getUserCurrenciesWhereIncome(string $username, int $income): array
    {
        $currencies = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT distinct currencies.name as currency ";
        $query .= "FROM transactions INNER JOIN currencies ON transactions.currency = currencies.id ";
        $query .= "WHERE transactions.user_id=";
        $query .= "(SELECT id FROM users WHERE username=?) AND income=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("si", $username, $income);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $currencies[] = $resAssoc['currency'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return $currencies;
    }

    /**
     * getUserSumBetweenDatesWithCurrency
     * This function returns the sum of the given user's transaction in the given categories
     *  and currencies in the given period.
     * It returns the sum of the income transactions if the income parameter is set to 1.
     * It returns the sum of the expenditure transactions if the income parameter is set to 0.
     *
     * @param string $username
     * @param string $startDate
     * @param string $finalDate
     * @param string $currency
     * @param string $category
     * @param int $income
     * @return float
     */
    public function getUserSumBetweenDatesWithCurrency(
        string $username,
        string $startDate,
        string $finalDate,
        string $currency,
        string $category,
        int $income
    ): float {
        $amount = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query =   "SELECT SUM(amount) sumamount
                    FROM transactions 
                    WHERE user_id=? 
                    AND date BETWEEN ? AND ?
                    AND category = ? 
                    AND currency = ?
                    And income = ?";
        if ($stmt->prepare($query)) {
            $user_id = $this->getFieldOfUser("id", $username);
            $categoryId = $this->getCategoryId($category, intval($user_id));
            $currencyId = $this->getCurrencyId($currency);
            $stmt->bind_param(
                "issiii",
                $user_id,
                $startDate,
                $finalDate,
                $categoryId,
                $currencyId,
                $income
            );
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $amount = $resAssoc['sumamount'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return floatval($amount);
    }

    /**
     * getCategoryId
     * This function returns the ID of the given category.
     *
     * @param  string $categoryName
     * @param  int $user_id
     * @return int|false
     */
    public function getCategoryId(string $categoryName, int $user_id): int | false
    {
        $res = '';
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("SELECT id FROM categories WHERE user_id=? AND name=?")) {
            $stmt->bind_param("is", $user_id, $categoryName);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                $resAssoc = $result->fetch_assoc();
                if ($resAssoc) {
                    $res = $resAssoc["id"];
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return intval($res);
    }

    /**
     * addTransaction
     * This function adds the given transaction to the database.
     *
     * @param string $username
     * @param string $date
     * @param string $time
     * @param string $category
     * @param string $comment
     * @param int $income
     * @param float $amount
     * @param string $currency
     * @return bool
     */
    public function addTransaction(
        string $username,
        string $date,
        string $time,
        string $category,
        string $comment,
        int $income,
        float $amount,
        string $currency
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO transactions " .
                 "(user_id, date, time, category, comment, income, amount, currency) " .
                 "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $categoryId = -1;
            if (!($categoryId = $this->getCategoryId($category, intval($user_id)))) {
                return false;
            }
            $currencyId = -1;
            if (!($currencyId = $this->getCurrencyId($currency))) {
                return false;
            }
            $stmt->bind_param("issisisi", $user_id, $date, $time, $categoryId, $comment, $income, $amount, $currencyId);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserTransactions
     * This function returns the transactions of the user.
     *
     * @param  string $username
     * @return array<int, array<string, float|string>> | false
     */
    public function getUserTransactions(string $username): array | false
    {
        $transactions = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT transactions.date as date, transactions.time as time, ";
        $query .= "categories.name as category, transactions.amount as amount, ";
        $query .= "transactions.comment as comment, currencies.name as currency, ";
        $query .= "transactions.income as income ";
        $query .= "FROM transactions ";
        $query .= "INNER JOIN categories ON categories.id = transactions.category ";
        $query .= "INNER JOIN currencies ON currencies.id = transactions.currency ";
        $query .= "WHERE transactions.user_id=?";
        $query .= " ORDER BY date DESC, time DESC";

        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $stmt->bind_param("i", $user_id);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $transactions[] = $resAssoc;
                }
            }
            $stmt->close();
        }
        $con->close();
        return $transactions;
    }

    /**
     * getSUM
     * This function returns the sum of the user's transactions in the given currency.
     *
     * @param  string $username
     * @param  string $currency
     * @param  int $income
     * @return float|false
     */
    public function getSUM(string $username, string $currency, int $income): float | false
    {
        $amount = 0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query =   "SELECT SUM(amount) sumamount
                    FROM transactions
                    WHERE user_id=?
                    AND currency = ?
                    And income = ?";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $currencyId = -1;
            if (!($currencyId = $this->getCurrencyId($currency))) {
                return false;
            }
            $stmt->bind_param("isi", $user_id, $currencyId, $income);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $amount = $resAssoc['sumamount'];
                }
            }
            $stmt->close();
        }
        $con->close();
        return floatval($amount);
    }

    /**
     * updateUserFieldTo
     * This function updates the specified field of the user with the given value.
     *
     * @param string $username
     * @param string $field
     * @param bool|string $value
     * @param string $valuetype
     * @return bool
     */
    public function updateUserFieldTo(
        string $username,
        string $field,
        bool|string $value,
        string $valuetype
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("UPDATE users SET " . $field . "=? WHERE username=?")) {
            $stmt->bind_param($valuetype . 's', $value, $username);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * setUserFieldToNull
     * This function sets the specified field of the user to NULL.
     *
     * @param  string $username
     * @param  string $field
     * @return bool
     */
    public function setUserFieldToNull(string $username, string $field): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("UPDATE users SET " . $field . "=NULL WHERE username=?")) {
            $stmt->bind_param('s', $username);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserPlans
     * This function returns the financial plans of the given user.
     *
     * @param  string $username
     * @param  int $year
     * @param  int $month
     * @return array<string, array<string, array<string, float>>>|false
     */
    public function getUserPlans(string $username, int $year, int $month): array | false
    {
        $plans = [];
        $categories = $this->getUserCategories($username);
        foreach ($categories as $cat) {
            $plans[$cat] = ['Income' => [], 'Expenditure' => []];
        }
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT categories.name as category, plans.amount as amount, ";
        $query .= "currencies.name as currency, plans.income as income ";
        $query .= "FROM plans ";
        $query .= "INNER JOIN categories ON categories.id = plans.category ";
        $query .= "INNER JOIN currencies ON currencies.id = plans.currency ";
        $query .= "WHERE plans.user_id=? AND plans.year=? AND plans.month=? ";
        $query .= "ORDER BY categories.name DESC";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $stmt->bind_param("iii", $user_id, $year, $month);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $cat = $resAssoc['category'];
                    if (!array_key_exists($cat, $plans)) {
                        $plans[$cat] = ['Income' => [], 'Expenditure' => []];
                    }
                    if ($resAssoc['income'] == 1) {
                        $plans[$cat]['Income'][$resAssoc['currency']] = floatval($resAssoc['amount']);
                    } else {
                        $plans[$cat]['Expenditure'][$resAssoc['currency']] = floatval($resAssoc['amount']);
                    }
                }
            }
            $stmt->close();
        }
        $con->close();
        foreach ($plans as $cat => $value) {
            foreach ($value['Expenditure'] as $cur => $amo) {
                foreach ($plans as $c => $v) {
                    if (!array_key_exists($cur, $v['Expenditure'])) {
                        $plans[$c]['Expenditure'][$cur] = 0;
                    }
                }
            }
            foreach ($value['Income'] as $cur => $amo) {
                foreach ($plans as $c => $v) {
                    if (!array_key_exists($cur, $v['Income'])) {
                        $plans[$c]['Income'][$cur] = 0;
                    }
                }
            }
        }
        return $plans;
    }

    /**
     * saveUserPlans
     * This function saves the plans for the user for the given date.
     *
     * @param  string $username
     * @param  array<string, array<string, array<string, float>>> $table
     * @param  int $year
     * @param  int $month
     * @return bool
     */
    public function saveUserPlans(string $username, array $table, int $year, int $month): bool
    {
        if (!$this->deleteAllPlansWithZeroAmount()) {
            return false;
        }
        $oldTable = $this->getUserPlans($username, $year, $month);
        if ($oldTable === false) {
            return false;
        }
        foreach ($table as $category => $expinc) {
            if (!key_exists($category, $oldTable)) {
                if (!$this->addCategory($category, $username)) {
                    return false;
                }

                foreach ($expinc['Income'] as $currency => $amount) {
                    if (
                        !$this->addPlan(
                            $username,
                            $year,
                            $month,
                            1,
                            $category,
                            $amount,
                            $currency
                        )
                    ) {
                        return false;
                    }
                }

                foreach ($expinc['Expenditure'] as $currency => $amount) {
                    if (
                        !$this->addPlan(
                            $username,
                            $year,
                            $month,
                            0,
                            $category,
                            $amount,
                            $currency
                        )
                    ) {
                        return false;
                    }
                }
            } else {
                foreach ($table[$category]['Income'] as $currency => $amount) {
                    if (
                        !key_exists($currency, $oldTable[$category]['Income']) ||
                        intval($oldTable[$category]['Income'][$currency]) == 0
                    ) {
                        if (
                            !$this->addPlan(
                                $username,
                                $year,
                                $month,
                                1,
                                $category,
                                $amount,
                                $currency
                            )
                        ) {
                            return false;
                        }
                    } else {
                        if ($amount != $oldTable[$category]['Income'][$currency]) {
                            if (
                                !$this->updatePlanAmount(
                                    $username,
                                    $year,
                                    $month,
                                    1,
                                    $category,
                                    $currency,
                                    $amount
                                )
                            ) {
                                return false;
                            }
                        }
                    }
                }
                foreach ($table[$category]['Expenditure'] as $currency => $amount) {
                    if (
                        !key_exists($currency, $oldTable[$category]['Expenditure']) ||
                        intval($oldTable[$category]['Expenditure'][$currency]) == 0
                    ) {
                        if (
                            !$this->addPlan(
                                $username,
                                $year,
                                $month,
                                0,
                                $category,
                                $amount,
                                $currency
                            )
                        ) {
                            return false;
                        }
                    } else {
                        if ($amount != $oldTable[$category]['Expenditure'][$currency]) {
                            if (
                                !$this->updatePlanAmount(
                                    $username,
                                    $year,
                                    $month,
                                    0,
                                    $category,
                                    $currency,
                                    $amount
                                )
                            ) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        $this->deleteAllPlansWithZeroAmount();
        return true;
    }

    /**
     * addPlan
     * This function adds the given plan to the database.
     *
     * @param string $username
     * @param int $year
     * @param int $month
     * @param int $income
     * @param string $category
     * @param float $amount
     * @param string $currency
     * @return bool
     */
    private function addPlan(
        string $username,
        int $year,
        int $month,
        int $income,
        string $category,
        float $amount,
        string $currency
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query  = "INSERT INTO plans ";
        $query .= "(user_id, year, month, income, category, amount, currency) ";
        $query .= "VALUES (?, ?, ?, ?, ?, ?, ?)";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $categoryId = -1;
            if (!($categoryId = $this->getCategoryId($category, intval($user_id)))) {
                return false;
            }
            $currencyId = -1;
            if (!($currencyId = $this->getCurrencyId($currency))) {
                return false;
            }
            $stmt->bind_param("iiiiiii", $user_id, $year, $month, $income, $categoryId, $amount, $currencyId);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * updatePlanAmount
     * This function updates the amount of the specified plan in the database.
     *
     * @param string $username
     * @param int $year
     * @param int $month
     * @param int $income
     * @param string $category
     * @param string $currency
     * @param float $newAmount
     * @return bool
     */
    private function updatePlanAmount(
        string $username,
        int $year,
        int $month,
        int $income,
        string $category,
        string $currency,
        float $newAmount
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "UPDATE plans SET amount=? ";
        $query .= "WHERE user_id=? AND year=? AND month=? AND ";
        $query .= "income=? AND category=? AND currency=?";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $categoryId = -1;
            if (!($categoryId = $this->getCategoryId($category, intval($user_id)))) {
                return false;
            }
            $currencyId = -1;
            if (!($currencyId = $this->getCurrencyId($currency))) {
                return false;
            }
            $stmt->bind_param('iiiiiii', $newAmount, $user_id, $year, $month, $income, $categoryId, $currencyId);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * deleteAllPlansWithZeroAmount
     * This function deletes all the plans from the database where the amount is 0.
     *
     * @return bool
     */
    public function deleteAllPlansWithZeroAmount(): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "DELETE FROM plans ";
        $query .= "WHERE amount=0";
        if ($stmt->prepare($query)) {
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getPlanAmount
     * This function returns the amount of the specified plan.
     *
     * @param string $username
     * @param int $year
     * @param int $month
     * @param bool $income
     * @param string $category
     * @param string $currency
     * @return float|false
     */
    public function getPlanAmount(
        string $username,
        int $year,
        int $month,
        bool $income,
        string $category,
        string $currency
    ): float | false {
        $amount = 0;
        $plan = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT plans.amount as amount ";
        $query .= "FROM plans ";
        $query .= "INNER JOIN categories ON categories.id = plans.category ";
        $query .= "INNER JOIN currencies ON currencies.id = plans.currency ";
        $query .= "INNER JOIN users ON users.id = plans.user_id ";
        $query .= "WHERE users.username=? AND plans.year=? AND plans.month=? AND ";
        $query .= "plans.income=? AND categories.name=? AND currencies.name=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("siiiss", $username, $year, $month, $income, $category, $currency);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                while ($resAssoc = $result->fetch_assoc()) {
                    $plan[] = $resAssoc;
                }
                if ($plan == []) {
                    return false;
                } elseif (isset($plan[0]['amount'])) {
                    $amount = floatval($plan[0]['amount']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $amount;
    }

    /**
     * getUserToDos
     * This function returns the to-dos of the given user.
     *
     * @param  string $username
     * @return array<int, array<string, string>> | false
     */
    public function getUserToDos(string $username): array | false
    {
        $toDos = [];
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT todo.id as id, todo.date as date, todo.time as time, ";
        $query .= "todo.title as title, todo.description as description ";
        $query .= "FROM todo ";
        $query .= "INNER JOIN users ON users.id = todo.user_id ";
        $query .= "WHERE users.username=?";
        $query .= " ORDER BY date, time";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                $i = 0;
                while ($resAssoc = $result->fetch_assoc()) {
                    $toDos[$i] = $resAssoc;
                    $i++;
                }
            }
            $stmt->close();
        }
        $con->close();
        return $toDos;
    }

    /**
     * addToDo
     * This function adds the given to-do to the database.
     *
     * @param string $username
     * @param string $date
     * @param string $time
     * @param string $title
     * @param string $description
     * @return bool
     */
    public function addToDo(
        string $username,
        string $date,
        string $time,
        string $title,
        string $description
    ): bool {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO todo " .
                 "(user_id, date, time, title, description) " .
                 "VALUES (?, ?, ?, ?, ?)";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $stmt->bind_param("issss", $user_id, $date, $time, $title, $description);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * deleteTodo
     * This function deletes the specified to-do.
     *
     * @param  string $username
     * @param  int $id
     * @return bool
     */
    public function deleteTodo(string $username, int $id): bool
    {
        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        if ($user_id != $this->getUserForTodo($id)) {
            return false;
        }
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "DELETE FROM todo ";
        $query .= "WHERE id=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserForTodo
     * This function returns the ID of the owner of the to-do.
     *
     * @param  int $todoid
     * @return int|false
     */
    private function getUserForTodo(int $todoid): int | false
    {
        $user_id = -1;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT user_id FROM todo ";
        $query .= "WHERE id=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("i", $todoid);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $user_id = intval($resAssoc['user_id']);
                } else {
                    return false;
                }
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $user_id;
    }

    /**
     * userHasHealthSettings
     * This function returns whether the user already has health settings.
     *
     * @param  string $username
     * @return bool
     */
    public function userHasHealthSettings(string $username): bool
    {
        $has = false;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT health.id ";
        $query .= "FROM health INNER JOIN users ON users.id = health.user_id ";
        $query .= "WHERE users.username=?";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($fetched = $result->fetch_assoc()) {
                    $has = isset($fetched['id']);
                } else {
                    return false;
                }
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $has;
    }

    /**
     * addHelthSettingsToUser
     * This function adds health settings to the given user.
     *
     * @param  string $username
     * @return bool
     */
    public function addHelthSettingsToUser(string $username): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO health (user_id) VALUES (?)";
        if ($stmt->prepare($query)) {
            $user_id = -1;
            if (!($user_id = $this->getFieldOfUser("id", $username))) {
                return false;
            }
            $stmt->bind_param("i", $user_id);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * setWeightGoal
     * This function sets the user's weight goal.
     *
     * @param  string $username
     * @param  float $amount
     * @return bool
     */
    public function setWeightGoal(string $username, float $amount): bool
    {
        if (!$this->userHasHealthSettings($username)) {
            if (!$this->addHelthSettingsToUser($username)) {
                return false;
            }
        }

        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        if (!$this->setHealthField($user_id, 'weight_goal', $amount)) {
            return false;
        }
        return true;
    }

    /**
     * setWaterGoal
     * This function sets the user's water goal.
     *
     * @param  string $username
     * @param  float $amount
     * @return bool
     */
    public function setWaterGoal(string $username, float $amount): bool
    {
        if (!$this->userHasHealthSettings($username)) {
            if (!$this->addHelthSettingsToUser($username)) {
                return false;
            }
        }

        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        if (!$this->setHealthField($user_id, 'water_goal', $amount)) {
            return false;
        }
        return true;
    }

    /**
     * setCalorieGoal
     * This function sets the user's calorie goal.
     *
     * @param  string $username
     * @param  float $amount
     * @return bool
     */
    public function setCalorieGoal(string $username, float $amount): bool
    {
        if (!$this->userHasHealthSettings($username)) {
            if (!$this->addHelthSettingsToUser($username)) {
                return false;
            }
        }

        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        if (!$this->setHealthField($user_id, 'calorie_goal', $amount)) {
            return false;
        }
        return true;
    }



    /**
     * setHealthField
     * This function sets the given field to the given value in the health table.
     *
     * @param  string $user_id
     * @param  string $field
     * @param  float $value
     * @return bool
     */
    private function setHealthField(string $user_id, string $field, float $value): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("UPDATE health SET " . $field . "=? WHERE user_id=?")) {
            $strvalue = strval($value);
            $stmt->bind_param('si', $strvalue, $user_id);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * setActualWeight
     * This function sets the user's actual weight.
     *
     * @param  string $username
     * @param  float $weight
     * @return bool
     */
    public function setActualWeight(string $username, float $weight): bool
    {
        if (!$this->userHasHealthSettings($username)) {
            if (!$this->addHelthSettingsToUser($username)) {
                return false;
            }
        }

        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        if (!$this->setHealthField($user_id, 'actual_weight', $weight)) {
            return false;
        }
        return true;
    }

    /**
     * incrementWater
     * This function increments the user's daily water level with the given amount.
     *
     * @param  string $username
     * @param  float $amount
     * @param  string $date
     * @return bool
     */
    public function incrementWater(string $username, float $amount, string $date): bool
    {
        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        return $this->addOrIncrementHealthState(intval($user_id), $amount, $date, 'water');
    }

    /**
     * incrementCalorie
     * This function increments the user's daily calorie level with the given amount.
     *
     * @param  string $username
     * @param  float $amount
     * @param  string $date
     * @return bool
     */
    public function incrementCalorie(string $username, float $amount, string $date): bool
    {
        $user_id = -1;
        if (!($user_id = $this->getFieldOfUser("id", $username))) {
            return false;
        }
        return $this->addOrIncrementHealthState(intval($user_id), $amount, $date, 'calorie');
    }


    /**
     * deleteHealthStatesWithZero
     * This function deletes all the rows form the healthstates table where the amount is 0.
     *
     * @return bool
     */
    private function deleteHealthStatesWithZero(): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "DELETE FROM healthstates ";
        $query .= "WHERE amount=0";
        if ($stmt->prepare($query)) {
            $stmt->execute();
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * addOrIncrementHealthState
     * This function increments the health state with the given value if it exists.
     * And, it adds the health state if it does not exist.
     *
     * @param  int $user_id
     * @param  float $amount
     * @param  string $date
     * @param  string $type
     * @return bool
     */
    private function addOrIncrementHealthState(int $user_id, float $amount, string $date, string $type): bool
    {
        $this->deleteHealthStatesWithZero();
        $oldAmount = $this->getHealthStateAmount($user_id, $date, $type);
        if (!$oldAmount) {
            $oldAmount = 0.0;
            if (!$this->addHealthState($user_id, $date, $type, $amount)) {
                return false;
            }
        }
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($stmt->prepare("UPDATE healthstates SET amount=? WHERE user_id=? AND date=? AND type=?")) {
            $newAmount = $amount + $oldAmount;
            $stmt->bind_param('siss', $newAmount, $user_id, $date, $type);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getHealthStateAmount
     * This function returns the amount of the given health state.
     *
     * @param  int $user_id
     * @param  string $date
     * @param  string $type
     * @return float|false
     */
    private function getHealthStateAmount(int $user_id, string $date, string $type): float | false
    {
        $amount = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT amount ";
        $query .= "FROM healthstates ";
        $query .= "WHERE user_id=? AND date=? AND type=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("iss", $user_id, $date, $type);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $amount = floatval($resAssoc['amount']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $amount;
    }

    /**
     * addHealthState
     * This function adds health state to the user.
     *
     * @param  int $user_id
     * @param  string $date
     * @param  string $type
     * @param  float $amount
     * @return bool
     */
    private function addHealthState(int $user_id, string $date, string $type, float $amount): bool
    {
        if ($amount == 0) {
            return false;
        }
        $con = $this->connect();
        $stmt = $con->stmt_init();
        if ($con->errno) {
            return false;
        }
        $query = "INSERT INTO healthstates (user_id, date, type, amount) VALUES (?,?,?,?)";
        if ($stmt->prepare($query)) {
            $stmt->bind_param("isss", $user_id, $date, $type, $amount);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
    }

    /**
     * getUserWeightGoal
     * This function returns the user's weight goal.
     *
     * @param  string $username
     * @return float|false
     */
    public function getUserWeightGoal(string $username): float | false
    {
        $goal = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT health.weight_goal as goal ";
        $query .= "FROM health INNER JOIN users ON health.user_id=users.id ";
        $query .= "WHERE users.username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $goal = floatval($resAssoc['goal']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $goal;
    }

    /**
     * getUserWaterGoal
     * This function returns the user's water goal.
     *
     * @param  string $username
     * @return float|false
     */
    public function getUserWaterGoal(string $username): float | false
    {
        $goal = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT health.water_goal as goal ";
        $query .= "FROM health INNER JOIN users ON health.user_id=users.id ";
        $query .= "WHERE users.username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $goal = floatval($resAssoc['goal']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $goal;
    }

    /**
     * getUserCalorieGoal
     * This function returns the user's calorie goal.
     *
     * @param  string $username
     * @return float|false
     */
    public function getUserCalorieGoal(string $username): float | false
    {
        $goal = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT health.calorie_goal as goal ";
        $query .= "FROM health INNER JOIN users ON health.user_id=users.id ";
        $query .= "WHERE users.username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $goal = floatval($resAssoc['goal']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $goal;
    }

    /**
     * getActualLevelIn
     * This function returns the current level in the given type of health state on the specified date.
     *
     * @param  string $username
     * @param  string $type
     * @param  string $date
     * @return float|false
     */
    public function getActualLevelIn(string $username, string $type, string $date): float | false
    {
        $sum = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT SUM(healthstates.amount) as sum ";
        $query .= "FROM healthstates INNER JOIN users ON healthstates.user_id=users.id ";
        $query .= "WHERE users.username=? AND healthstates.type=? AND healthstates.date=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("sss", $username, $type, $date);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $sum = floatval($resAssoc['sum']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $sum;
    }

    /**
     * getUserActualWeight
     * This function returns the user's current weight.
     *
     * @param  string $username
     * @return float|false
     */
    public function getUserActualWeight(string $username): float|false
    {
        $weight = 0.0;
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT health.actual_weight as weight ";
        $query .= "FROM health INNER JOIN users ON health.user_id=users.id ";
        $query .= "WHERE users.username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $weight = floatval($resAssoc['weight']);
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $weight;
    }

    /**
     * getSelectedNotifOpt
     * This function returns the user's selected notification option.
     * Possible types: no, monthly, weekly, daily
     *
     * @param  string $username
     * @return string|false
     */
    public function getSelectedNotifOpt(string $username): string | false
    {
        $type = 'no';
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "SELECT notification as type ";
        $query .= "FROM users ";
        $query .= "WHERE username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("s", $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result) {
                if ($resAssoc = $result->fetch_assoc()) {
                    $type = $resAssoc['type'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return $type;
    }

    /**
     * setUserNotifTo
     * This function sets the user's selected notification frequency.
     *
     * @param  string $username
     * @param  string $type
     * @return bool
     */
    public function setUserNotifTo(string $username, string $type): bool
    {
        $con = $this->connect();
        $stmt = $con->stmt_init();
        $query  = "UPDATE users SET notification=? ";
        $query .= "WHERE username=?";

        if ($stmt->prepare($query)) {
            $stmt->bind_param("ss", $type, $username);
            if (!$stmt->execute()) {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }
        $con->close();
        return true;
        ;
    }
}
