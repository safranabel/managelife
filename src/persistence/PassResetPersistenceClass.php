<?php

declare(strict_types=1);

namespace Managelife\Persistence;
namespace Managelife\PassReset;

use Managelife\Persistence\UserHandlerPersistence;

/**
 * PassResetPersistence
 * This class represents the persistence layer of password changing/updating/resetting features.
 */
class PassResetPersistence extends UserHandlerPersistence
{
}
