<?php

declare(strict_types=1);

namespace Managelife\Persistence;

use Managelife\Persistence\UserHandlerPersistence;

class UserHandlerPersistenceMock extends UserHandlerPersistence
{
    public const USER_TOKEN = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    public const ADMIN_TOKEN = '07909f5928f186da6c851a32067867313cef84bd80fe94c680f93accb8535297';
    /** @var array<string, int|string> */
    private array $correctUser;
    /** @var array<string> */
    private array $currencies;
    /** @var array<string> */
    private array $userCurrencies;
    /** @var array<string> */
    private array $userCategories;
    /** @var array<string> */
    private array $incomeCategories;
    /** @var array<string> */
    private array $expendCategories;
    /** @var array<string, array<string, float|string>> */
    private array $transactions;
    private int $connectionLostOnCall;
    private int $callNum;
    private string $selectedNotifOption = "no";
    /** @var array<string, array<string, array<string, float>>> */
    private array $plans = [];
     /** @var array<int, array<string, string>> */
    private array $todos = [];
    private float $weightGoal = 80.5;
    private float $waterGoal = 3.5;
    private float $calorieGoal = 12000;
    private float $actualWaterLevel = 2.3;
    private float $actualCalorieLevel = 5500;
    private float $actualWeight = 79.5;
    private float $planAmount = 50.5;

    public function __construct()
    {
        $this->setToDefault();
    }
    public function setToDefault(): void
    {
        $this->correctUser = [
            'id' => 1,
            'username' => 'testUser_id',
            'email' => 'test@test.com',
            'password_hash' => hash('sha256', 'testPass123'),
            'forename' => 'Testforename',
            'surname' => 'Testsurname',
            'admin' => 0,
            'password_reset' => hash('sha256', 'resetkey123')
        ];

        $this->currencies = [
            'EUR',
            'GBP',
            'HKR',
            'HUF',
            'RUB',
            'USD'
        ];

        $this->userCurrencies = [
            'EUR',
            'HUF'
        ];

        $this->userCategories = [
            'Food',
            'Other',
            'Scholarship',
            'Work',
            'Taxes'
        ];

        $this->incomeCategories = [
            'Other',
            'Work',
            'Scholarship'
        ];

        $this->expendCategories = [
            'Food',
            'Other',
            'Taxes'
        ];

        $this->transactions = [
            'income' => [
                'date' => '2021-06-30',
                'time' => '12:15',
                'category' => $this->incomeCategories[0],
                'comment' => '-',
                'income' => 1,
                'user_id' => $this->correctUser['id'],
                'amount' => 12.50,
                'currency' => $this->userCurrencies[0]
            ],
            'expend' => [
                'date' => '2021-07-30',
                'time' => '12:16',
                'category' => $this->expendCategories[0],
                'comment' => '-',
                'income' => 0,
                'user_id' => $this->correctUser['id'],
                'amount' => 120,
                'currency' => $this->userCurrencies[0]
            ]
        ];

        $this->connectionLostOnCall = -1;
        $this->callNum = 0;
    }
    public function setIncomeAmount(float $value): void
    {
        $this->transactions['income']['amount'] = $value;
    }
    public function setExpenditureAmount(float $value): void
    {
        $this->transactions['expend']['amount'] = $value;
    }
    public function addPlan(
        string $expenditureOrIncome,
        string $category,
        string $currency,
        float $amount
    ): void {
        $this->plans[$expenditureOrIncome][$category][$currency] = $amount;
    }
    public function addToDoMock(
        int $id,
        string $title,
        string $desc,
        string $date,
        string $time
    ): void {
        $this->todos[$id] = [
            'id' => strval($id),
            'title' => $title,
            'description' => $desc,
            'date' => $date,
            'time' => $time

        ];
    }
    public function clearPlans(): void
    {
        $this->plans = [];
    }
    public function connectionLostOnCall(int $num): void
    {
        $this->callNum = 0;
        $this->connectionLostOnCall = $num;
    }
    public function setUserAdmin(int $admin): void
    {
        $this->correctUser['admin'] = $admin;
    }
    public function userExists(string $username): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $username == $this->correctUser['username'];
    }
    public function fieldExistsWithValue(string $fieldName, string|bool $value, string $valuetype): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;

        if (!array_key_exists($fieldName, $this->correctUser)) {
            return false;
        }

        return $this->correctUser[$fieldName] == $value;
    }
    public function addUser(
        string $username,
        string $email,
        string $forename,
        string $surname,
        string $passwordHash
    ): bool {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }
    public function isAdmin(string $username): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return boolval($this->correctUser['admin']);
    }
    public function getUserWhereEmail(string $email): string | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        if ($email != $this->correctUser['email']) {
            return false;
        }
        return strval($this->correctUser['username']);
    }
    public function getFieldOfUser(string $field, string $username): string | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        if ($this->connectionLostOnCall == $this->callNum) {
            return false;
        }
        ++$this->callNum;
        if (!array_key_exists($field, $this->correctUser)) {
            return false;
        }
        return strval($this->correctUser[$field]);
    }
    /** @return array<string>  */
    public function getUserCategories(string $username): array
    {
        return $this->userCategories;
    }
    /** @return array<string>  */
    public function getUserCategoriesWhereIncome(string $username, int $income): array
    {
        if (boolval($income)) {
            return $this->incomeCategories;
        }
        return $this->expendCategories;
    }
    /** @return array<string> */
    public function getUserCurrencies(string $username): array
    {
        return $this->userCurrencies;
    }
    /** @return array<string> */
    public function getUserCurrenciesWhereIncome(string $username, int $income): array
    {
        if (boolval($income)) {
            return [strval($this->transactions['income']['currency'])];
        }
        return [strval($this->transactions['expend']['currency'])];
    }
    public function getUserSumBetweenDatesWithCurrency(
        string $username,
        string $startDate,
        string $finalDate,
        string $currency,
        string $category,
        int $income
    ): float {
        if (boolval($income)) {
            return floatval($this->transactions['income']['amount']);
        }
        return floatval($this->transactions['expend']['amount']);
    }
    public function addTransaction(
        string $username,
        string $date,
        string $time,
        string $category,
        string $comment,
        int $income,
        float $amount,
        string $currency
    ): bool {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }
    /** @return array<int, array<string, float|string>> */
    public function getUserTransactions(string $username): array
    {
        ++$this->callNum;
        if ($this->connectionLostOnCall == $this->callNum) {
            return [];
        }
        return [
            $this->transactions['income'],
            $this->transactions['expend']
        ];
    }
    public function getSUM(string $username, string $currency, int $income): float
    {
        $sum = 0;
        foreach ($this->transactions as $key => $value) {
            if ($key == 'income' && boolval($income) && $value['currency'] == $currency) {
                $sum += $value['amount'];
            } elseif ($key == 'expend' && !boolval($income) && $value['currency'] == $currency) {
                $sum += $value['amount'];
            }
        }
        return floatval($sum);
    }
    public function updateUserFieldTo(
        string $username,
        string $field,
        bool|string $value,
        string $valuetype
    ): bool {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setUserFieldToNull(string $username, string $field): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    /** @return array<string> */
    public function getCurrencies(): false|array
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->currencies;
    }

    public function getPlanAmount(
        string $username,
        int $year,
        int $month,
        bool $income,
        string $category,
        string $currency
    ): float | false {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->planAmount;
    }

    public function setPlanAmount(float $value): void
    {
        $this->planAmount = $value;
    }

    public function addCategory(string $categoryname, string $username): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setWeightGoal(string $username, float $amount): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setWaterGoal(string $username, float $amount): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setCalorieGoal(string $username, float $amount): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setActualWeight(string $username, float $weight): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function incrementWater(string $username, float $amount, string $date): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function incrementCalorie(string $username, float $amount, string $date): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function getUserWeightGoal(string $username): float | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->weightGoal;
    }

    public function setActualWeightMock(float $value): void
    {
        $this->actualWeight = $value;
    }

    public function getUserWaterGoal(string $username): float | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->waterGoal;
    }

    public function setWaterGoalMock(float $value): void
    {
        $this->waterGoal = $value;
    }

    public function getUserCalorieGoal(string $username): float | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->calorieGoal;
    }

    public function setCalorieGoalMock(float $value): void
    {
        $this->calorieGoal = $value;
    }

    public function getActualLevelIn(string $username, string $type, string $date): float | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        if ($type == 'water') {
            return $this->actualWaterLevel;
        }
        if ($type == 'calorie') {
            return $this->actualCalorieLevel;
            ;
        }
        return false;
    }

    public function setActualWaterLevelMock(float $value): void
    {
        $this->actualWaterLevel = $value;
    }

    public function setActualCalorieLevelMock(float $value): void
    {
        $this->actualCalorieLevel = $value;
    }

    public function getUserActualWeight(string $username): float|false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->actualWeight;
    }

    public function setSelectedNotifOption(string $opt): void
    {
        $this->selectedNotifOption = $opt;
    }

    public function getSelectedNotifOpt(string $username): string | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->selectedNotifOption;
    }

    public function setUserNotifTo(string $username, string $type): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    /** @return array<string, array<string, array<string, float>>> | false */
    public function getUserPlans(string $username, int $year, int $month): array | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->plans;
    }

    /** @return array<int, array<string, string>> | false */
    public function getUserToDos(string $username): array | false
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return $this->todos;
    }
    public function addToDo(
        string $username,
        string $date,
        string $time,
        string $title,
        string $description
    ): bool {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function deleteTodo(string $username, int $id): bool
    {
        if ($this->connectionLostOnCall == $this->callNum) {
            ++$this->callNum;
            return false;
        }
        ++$this->callNum;
        return true;
    }

    public function setWeightGoalMock(float $value): void
    {
        $this->weightGoal = $value;
    }
}
