<?php

declare(strict_types=1);

namespace Managelife\Transaction;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use PHPUnit\Framework\TestCase;
use Managelife\Persistence\UserHandlerPersistenceMock;

/**
 * @covers \Managelife\Transaction\TransactionModel
 * @uses \Managelife\Model\BaseModel
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Validator\Validator
 */
final class TransactionModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private TransactionModel $model;
    public function setUp(): void
    {
        $this->model = new TransactionModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }
    public function testTransactionValidWithCorrectDataAndExistingTranCat(): void
    {
        $_POST['trandate'] = '2021-06-30';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }

    public function testTransactionValidWithCorrectDataAndNewTranCat(): void
    {
        $_POST['trandate'] = '2021-06-30';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }

    public function testTransactionValidWithCorrectDataAndExpanditure(): void
    {
        $_POST['trandate'] = '2021-06-30';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'out';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithCorrectDataDateType1(): void
    {
        $_POST['trandate'] = '2021. 06. 30.';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithCorrectDataDateType2(): void
    {
        $_POST['trandate'] = '2021/06/30';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithIncorrectDateType(): void
    {
        $_POST['trandate'] = '2021. 06/30';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithUnsetDate(): void
    {
        unset($_POST['trandate']);
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullDateType(): void
    {
        $_POST['trandate'] = null;
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidDate(): void
    {
        $_POST['trandate'] = '2021-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithValidDate(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidTime1(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:60';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidTime2(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12-00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidTime3(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '24:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidTime4(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = 'aa:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidTime5(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '23:bb';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoTime(): void
    {
        $_POST['trandate'] = '2024-02-29';
        unset($_POST['trantime']);
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullTime(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = null;
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNegativeAmount(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '-12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoAmount(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        unset($_POST['tranamount']);
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullAmount(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = null;
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCurrency1(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'huf';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCurrency2(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'BBB';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCurrency3(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HU';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCurrency4(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUFUSD';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCurrency5(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = ' ';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoCurrency(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        unset($_POST['trancurrency']);
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullCurrency(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = null;
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewTestCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCategory1(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'InvalidCat';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCategory2(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = '';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvalidCategory3(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789' .
                            '0123456789';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoCategory(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        unset($_POST['trancat']);
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullCategory(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = null;
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvlaidNewCategory1(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '        ';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvlaidNewCategory2(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789' .
                               '0123456789';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithInvlaidComment(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '0123456789' .
                                '0223456789' .
                                '0323456789' .
                                '0423456789' .
                                '0523456789' .
                                '0623456789' .
                                '0723456789' .
                                '0823456789' .
                                '0923456789' .
                                '1023456789' .
                                '1123456789' .
                                '1223456789' .
                                '1323456789' .
                                '1423456789' .
                                '1523456789' .
                                '1623456789' .
                                '1723456789' .
                                '1823456789' .
                                '1923456789' .
                                '2023456789' .
                                '2123456789' .
                                '2223456789' .
                                '2323456789' .
                                '2423456789' .
                                '2523456789a';
        $_POST['trantype'] = 'in';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoComment(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        unset($_POST['trancomment']);
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithNullComment(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = null;
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
    }
    public function testTransactionValidWithInvlaidTranType(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'invalid';

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoTranType(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        unset($_POST['trantype']);

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNullTranType(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = null;

        assertFalse($this->model->transactionValid());
    }
    public function testTransactionValidWithNoData(): void
    {
        $_POST = [];

        assertFalse($this->model->transactionValid());
    }

    public function testSaveTransactionAfterCorrectValidation(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
        assertTrue($this->model->saveTransaction());
    }
    public function testSaveTransactionAfterIncorrectValidation(): void
    {
        $_POST = [];

        assertFalse($this->model->transactionValid());
        assertFalse($this->model->saveTransaction());
    }
    public function testSaveTransactionFailedConnection(): void
    {
        $this->persistenceMock->connectionLostOnCall(1);

        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = '';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
        assertFalse($this->model->saveTransaction());
    }
    public function testSaveTransactionAfterNoValidation(): void
    {
        assertFalse($this->model->saveTransaction());
    }
    public function testSaveTransactionAfterCorrectValidationNewCat(): void
    {
        $_POST['trandate'] = '2024-02-29';
        $_POST['trantime'] = '12:00';
        $_POST['tranamount'] = '12000';
        $_POST['trancurrency'] = 'HUF';
        $_POST['trancat'] = 'Other';
        $_POST['trannewcat'] = 'NewCat';
        $_POST['trancomment'] = '-';
        $_POST['trantype'] = 'in';

        assertTrue($this->model->transactionValid());
        assertTrue($this->model->saveTransaction());
    }
}
