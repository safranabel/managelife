<?php

declare(strict_types=1);

namespace Managelife\Validator;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use PHPUnit\Framework\TestCase;
use Managelife\Persistence\UserHandlerPersistenceMock;
use Managelife\Validator\Validator;

/**
 * @covers \Managelife\Validator\Validator
 */
final class ValidatorTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    public function setUp(): void
    {
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->persistenceMock->setToDefault();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testValidatePasswordCorrect(): void
    {
        $password = 'ThisIsACorrectPassword1234';
        assertTrue(Validator::validatePassword($password));
    }

    public function testValidatePasswordShort(): void
    {
        $password = '123Tt';
        assertFalse(Validator::validatePassword($password));
    }

    public function testValidatePasswordNoNumeric(): void
    {
        $password = 'TestWrong';
        assertFalse(Validator::validatePassword($password));
    }

    public function testValidatePasswordNoAlphabetic(): void
    {
        $password = '123456';
        assertFalse(Validator::validatePassword($password));
    }

    public function testValidatePasswordNoCapitalButCorrect(): void
    {
        $password = 'test12';
        assertTrue(Validator::validatePassword($password));
    }

    public function testValidatePasswordNoLowercaseButCorrect(): void
    {
        $password = 'TEST12';
        assertTrue(Validator::validatePassword($password));
    }

    public function testValidatePasswordEmpty(): void
    {
        $password = null;
        assertFalse(Validator::validatePassword($password));
    }

    public function testValidateExistingUserNameCorrect(): void
    {
        $username = 'testUser_id';
        assertTrue(Validator::validateExistingUsername(
            $username,
            $this->persistenceMock
        ));
    }

    public function testValidateExistingUserNameInvalid(): void
    {
        $username = 'testUser id';
        assertFalse(Validator::validateExistingUsername(
            $username,
            $this->persistenceMock
        ));
    }

    public function testValidateExistingUserNameUnexist(): void
    {
        $username = 'unexisting_id';
        assertFalse(Validator::validateExistingUsername(
            $username,
            $this->persistenceMock
        ));
    }

    public function testValidateExistingUserNameEmpty(): void
    {
        $username = null;
        assertFalse(Validator::validateExistingUsername(
            $username,
            $this->persistenceMock
        ));
    }

    public function testValidateUserPasswordCorrect(): void
    {
        $username = 'testUser_id';
        $password = 'testPass123';
        assertTrue(
            Validator::validateUserPassword(
                $username,
                $password,
                $this->persistenceMock
            )
        );
    }

    public function testValidateUserPasswordWrongPass(): void
    {
        $username = 'testUser_id';
        $password = 'wrongPass123';
        assertFalse(
            Validator::validateUserPassword(
                $username,
                $password,
                $this->persistenceMock
            )
        );
    }

    public function testValidateUserPasswordWrongUser(): void
    {
        $username = 'NotAUser_id';
        $password = 'Pass123';
        assertFalse(
            Validator::validateUserPassword(
                $username,
                $password,
                $this->persistenceMock
            )
        );
    }

    public function testValidateUserPasswordNoUser(): void
    {
        $username = null;
        $password = 'Pass123';
        assertFalse(
            Validator::validateUserPassword(
                $username,
                $password,
                $this->persistenceMock
            )
        );
    }

    public function testValidateUserPasswordNoPass(): void
    {
        $username = 'testUser_id';
        $password = null;
        assertFalse(
            Validator::validateUserPassword(
                $username,
                $password,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCategoryNameCorrect(): void
    {
        $category = '    Other    ';
        assertTrue(
            Validator::validateCategoryName(
                $category
            )
        );
    }

    public function testValidateCategoryShort(): void
    {
        $category = '    Ot    ';
        assertFalse(
            Validator::validateCategoryName(
                $category
            )
        );
    }

    public function testValidateNoAplhabetic(): void
    {
        $category = '123456';
        assertFalse(
            Validator::validateCategoryName(
                $category
            )
        );
    }

    public function testValidateNoCategory(): void
    {
        $category = null;
        assertFalse(
            Validator::validateCategoryName(
                $category
            )
        );
    }

    public function testValidateCurrencyCorrect(): void
    {
        $currency = 'EUR';
        assertTrue(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCurrencyUnexist(): void
    {
        $currency = 'AAA';
        assertFalse(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCurrencyUnset(): void
    {
        $currency = null;
        assertFalse(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCurrencyShort(): void
    {
        $currency = '       T     ';
        assertFalse(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCurrencyNoAlphabetic(): void
    {
        $currency = '123';
        assertFalse(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }

    public function testValidateCurrencyConnectionFailure(): void
    {
        $this->persistenceMock->connectionLostOnCall(0);

        $currency = 'EUR';
        assertFalse(
            Validator::validateCurrency(
                $currency,
                $this->persistenceMock
            )
        );
    }
}
