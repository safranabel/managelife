<?php

declare(strict_types=1);

namespace Managelife\PassUpdate;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use PHPUnit\Framework\TestCase;
use Managelife\PassUpdate\PassUpdateModel;
use Managelife\Persistence\UserHandlerPersistenceMock;

/**
 * @covers \Managelife\PassUpdate\PassUpdateModel
 * @uses \Managelife\Model\BaseModel
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Validator\Validator
 */
final class PassUpdateModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private PassUpdateModel $model;
    public function setUp(): void
    {
        $this->model = new PassUpdateModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
    }
    public function testIsValidWithCorrectData(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertTrue($this->model->isValid());
    }
    public function testIsValidWithNoData(): void
    {
        $_POST = [];

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithShortNewPass(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'test1';
        $_POST['passagain'] = 'test1';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithNewPassWithNoAbc(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = '111111';
        $_POST['passagain'] = '111111';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithNewPassWithNoNumber(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'testTest';
        $_POST['passagain'] = 'testTest';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithNoNewPass(): void
    {
        $_POST['oldpass'] = 'testPass123';
        unset($_POST['newpass']);
        $_POST['passagain'] = 'newTestPass1234';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithNoPassAgain(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        unset($_POST['passagain']);

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithNoEqualPasses(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass1234';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithUnsetOldPass(): void
    {
        unset($_POST['oldpass']);
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertFalse($this->model->isValid());
    }
    public function testIsValidWithIncorrectOldPass(): void
    {
        $_POST['oldpass'] = 'testPass1234';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertFalse($this->model->isValid());
    }

    public function testSaveNewPassAfterCorrectValidation(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertTrue($this->model->isValid());
        assertTrue($this->model->saveNewPass());
    }

    public function testSaveNewPassAfterIncorrectValidation(): void
    {
        $_POST = [];

        assertFalse($this->model->isValid());
        assertFalse($this->model->saveNewPass());
    }

    public function testSaveNewPassAfterNoValidation(): void
    {
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertFalse($this->model->saveNewPass());
    }

    public function testSaveNewPassWithConnectionFail(): void
    {
        $this->persistenceMock->connectionLostOnCall(3);
        $_POST['oldpass'] = 'testPass123';
        $_POST['newpass'] = 'newTestPass123';
        $_POST['passagain'] = 'newTestPass123';

        assertTrue($this->model->isValid());
        assertFalse($this->model->saveNewPass());
    }
}
