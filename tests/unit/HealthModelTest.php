<?php

declare(strict_types=1);

namespace Managelife\Health;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Health\HealthModel;

/**
 * @covers \Managelife\Health\HealthModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 */
final class HealthModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private HealthModel $model;

    public function setUp(): void
    {
        $this->model = new HealthModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testHasError(): void
    {
        $this->setUp();
        assertTrue($this->model->hasError());
    }

    public function testSetHealthGoalCorrect(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = '80.5';
        assertTrue($this->model->setHealthGoal());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '3.5';
        assertTrue($this->model->setHealthGoal());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '12000';
        assertTrue($this->model->setHealthGoal());
        assertFalse($this->model->hasError());
    }

    public function testSetHealthGoalNoAmount(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST = [];
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = null;
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());
    }

    public function testSetHealthGoalNANAmount(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = 'str';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '0.h';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '0,str';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());
    }

    public function testSetHealthGoalNegativeAmount(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = '-80';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '-3.5';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '-12000';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());
    }

    public function testSetHealthGoalInvalidType(): void
    {
        $this->setUp();
        $_GET['type'] = 'w';
        $_POST['amount'] = '40';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        unset($_GET['type']);
        $_POST['amount'] = '40';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());
    }

    public function testSetHealthGoalConnectionError(): void
    {
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'weight';
        $_POST['amount'] = '80.5';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'water';
        $_POST['amount'] = '3.5';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());

        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '12000';
        assertFalse($this->model->setHealthGoal());
        assertTrue($this->model->hasError());
    }

    public function testChangeStateCorrect(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = '79.5';
        assertTrue($this->model->changeState());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '0.5';
        assertTrue($this->model->changeState());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '-0.5';
        assertTrue($this->model->changeState());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '1300';
        assertTrue($this->model->changeState());
        assertFalse($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '-1000';
        assertTrue($this->model->changeState());
        assertFalse($this->model->hasError());
    }

    public function testChangeStateNegativeWeight(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = '-79';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());
    }

    public function testChangeStateNoAmount(): void
    {
        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST = [];
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = '';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = '';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'weight';
        $_POST['amount'] = null;
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        $_POST['amount'] = null;
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        $_POST['amount'] = null;
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'weight';
        unset($_POST['amount']);
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'water';
        unset($_POST['amount']);
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = 'calorie';
        unset($_POST['amount']);
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());
    }

    public function testChangeStateInvalidType(): void
    {
        $this->setUp();
        $_GET['type'] = 'w';
        $_POST['amount'] = '40';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = '';
        $_POST['amount'] = '40';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $_GET['type'] = null;
        $_POST['amount'] = '40';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        unset($_GET['type']);
        $_POST['amount'] = '40';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());
    }

    public function testChangeStateConnectionError(): void
    {
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'weight';
        $_POST['amount'] = '79';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'water';
        $_POST['amount'] = '1';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());

        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['type'] = 'calorie';
        $_POST['amount'] = '1000';
        assertFalse($this->model->changeState());
        assertTrue($this->model->hasError());
    }

    public function testGetWeightGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getWeightGoal(), 80.5);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getWeightGoal(), -1);
    }

    public function testGetWaterGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getWaterGoal(), 3.5);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getWaterGoal(), -1);
    }

    public function testGetCalorieGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getCalorieGoal(), 12000);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getCalorieGoal(), -1);
    }

    public function testGetActualWaterLevel(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getActualWaterLevel(), 2.3);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getActualWaterLevel(), -1);
    }

    public function testGetActualCalorieLevel(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getActualCalorieLevel(), 5500);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getActualCalorieLevel(), -1);
    }

    public function testGetActualWeight(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getActualWeight(), 79.5);

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getActualWeight(), -1);
    }
}
