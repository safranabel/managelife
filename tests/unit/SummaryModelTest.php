<?php

declare(strict_types=1);

namespace Managelife\Summaries;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use DateTime;
use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Summaries\SummaryModel;
use Managelife\UserCheck\UserCheckModel;

/**
 * @covers \Managelife\Summaries\SummaryModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\Planning\PlanningModel
 * @uses \Managelife\UserCheck\UserCheckModel
 */
final class SummaryModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private SummaryModel $model;
    private UserCheckModel $userCheck;

    public function setUp(): void
    {
        $this->userCheck = new UserCheckModel();
        $this->model = new SummaryModel($this->userCheck);
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->userCheck->setPersistence($this->persistenceMock);
        $this->userCheck->refreshUserData();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetUserCurrencies(): void
    {
        $this->setUp();
        assertEquals($this->model->getUserCurrencies(), ['EUR', 'HUF']);
    }

    public function testGetUserCategories(): void
    {
        $this->setUp();
        assertEquals($this->model->getUserCategories(), [
            'Food',
            'Other',
            'Scholarship',
            'Taxes',
            'Work'
        ]);
    }

    public function testGetMonthlyBalance(): void
    {
        $this->setUp();
        // 5 categories * (12.50 - 120) = -537.5
        assertEquals($this->model->getMonthlyBalance(new DateTime("2022-05-01"), 'EUR'), -537.5);
    }

    public function testGetUserCategoriesWithIncome(): void
    {
        // in
        $this->setUp();
        assertEquals($this->model->getUserCategoriesWithIncome(1), [
            'Other',
            'Scholarship',
            'Work'
        ]);

        // ex
        $this->setUp();
        assertEquals($this->model->getUserCategoriesWithIncome(0), [
            'Food',
            'Other',
            'Taxes'
        ]);
    }

    public function testGetUserCurrenciesWithIncome(): void
    {
        // in
        $this->setUp();
        assertEquals($this->model->getUserCurrenciesWithIncome(1), ['EUR']);

        // ex
        $this->setUp();
        assertEquals($this->model->getUserCurrenciesWithIncome(0), ['EUR']);
    }

    public function testGetPlan(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getPlan(
            2022,
            5,
            true,
            'CAT',
            'EUR'
        ), 50.5);

        // conection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getPlan(
            2022,
            5,
            true,
            'CAT',
            'EUR'
        ), 0);
    }

    public function testGetIncome(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getIncome(
            '2022-05-01',
            '2022-05-31',
            'EUR',
            'CAT'
        ), 12.5);
    }

    public function testGetExpenditure(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->model->getExpenditure(
            '2022-05-01',
            '2022-05-31',
            'EUR',
            'CAT'
        ), 120);
    }
}
