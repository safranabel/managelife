<?php

declare(strict_types=1);

namespace Managelife\Login;

session_start();

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

require_once "./testmanagelifeloader.php";

use PHPUnit\Framework\TestCase;
use Managelife\Login\LoginModel;
use Managelife\Persistence\UserHandlerPersistenceMock;

/**
 * @covers \Managelife\Login\LoginModel
 * @uses \Managelife\Model\BaseModel
 */
final class LoginModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private LoginModel $model;
    public function setUp(): void
    {
        $this->model = new LoginModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
    }
    public function testValidateWithCorrectData(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue(!$this->model->hasError(), 'There should be no error with correct data');
        assertEquals($this->model->getErrorFields(), []);
    }
    public function testWithNoPassword(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = '';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with no password');
        assertEquals($this->model->getErrorFields(), ['password']);
    }
    public function testWithNoUser(): void
    {
        $_POST['userid'] = '';
        $_POST['password'] = 'testPassword1234';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with no user');
        assertEquals($this->model->getErrorFields(), ['userid']);
    }
    public function testWithInvalidUser(): void
    {
        $_POST['userid'] = 'testtesttes';
        $_POST['password'] = 'testPassword1234';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with invalid user');
        assertEquals($this->model->getErrorFields(), ['userid']);
    }
    public function testWithWrongPassword(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'wrongPass123';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with wrong password');
        assertEquals($this->model->getErrorFields(), ['password']);
    }
    public function testWithUset(): void
    {
        unset($_POST['userid']);
        $_POST['password'] = null;

        $this->model->validate();
        assertTrue($this->model->hasError());
    }
    public function testWithEmail(): void
    {
        $_POST['userid'] = 'test@test.com';
        $_POST['password'] = null;

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with null password');
        assertEquals($this->model->getErrorFields(), ['password']);
    }

    public function testWithWrongEmail(): void
    {
        $_POST['userid'] = 'test@test.hu';
        $_POST['password'] = 'pass12345';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with wrong email');
        assertEquals($this->model->getErrorFields(), ['userid']);
    }

    public function testUnsetPost(): void
    {
        $_POST = [];
        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with unset post');
    }

    public function testLoginUser(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue(!$this->model->hasError(), 'There should be no error with correct data');
        assertEquals($this->model->getErrorFields(), []);
        $this->model->loginUser();
        assertEquals($_SESSION['username'], 'testUser_id');
    }

    public function testUnstableConnection1(): void
    {
        $this->persistenceMock->connectionLostOnCall(4);

        $_POST['userid'] = 'test@test.com';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with unstable connection');
        assertEquals($this->model->getErrorFields(), ['userid']);
    }

    public function testUnstableConnection2(): void
    {
        $this->persistenceMock->connectionLostOnCall(3);

        $_POST['userid'] = 'test@test.com';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with unstable connection');
        assertEquals($this->model->getErrorFields(), ['userid']);
    }
}
