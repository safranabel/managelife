<?php

declare(strict_types=1);

namespace Managelife\PassReset;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

require_once "./testmanagelifeloader.php";

use PHPUnit\Framework\TestCase;
use Managelife\PassReset\PassResetModel;
use Managelife\Persistence\UserHandlerPersistenceMock;

/**
 * @covers \Managelife\PassReset\PassResetModel
 * @uses \Managelife\Model\BaseModel
 * @uses \Managelife\Validator\Validator
 */
final class PassResetModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private PassResetModel $model;
    public function setUp(): void
    {
        $this->model = new PassResetModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
    }
    public function testIsValidWithCorrectData(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['email'] = 'test@test.com';
        $this->model->isValid();
        assertTrue(!$this->model->hasError());
    }
    public function testIsValidWithNonUserEmail(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['email'] = 'nonusertest@test.com';
        $this->model->isValid();
        assertTrue($this->model->hasError());
    }
    public function testIsValidWithInvalidEmail(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['email'] = 'nonusertest test.com';
        $this->model->isValid();
        assertTrue($this->model->hasError());
    }
    public function testIsValidWithNoEmail(): void
    {
        assertTrue(!$this->model->hasError());
        unset($_POST['email']);
        $_POST['test'] = 'test';
        $this->model->isValid();
        assertTrue($this->model->hasError());
    }
    public function testIsValidWithNoData(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST = [];
        $this->model->isValid();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeNodata(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST = [];
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeShortPass(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test1';
        $_POST['passagain'] = 'test1';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeUnequalPasses(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test11';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeUnsetPassAgain(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        unset($_POST['passagain']);
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeNoUser(): void
    {
        assertTrue(!$this->model->hasError());
        unset($_POST['username']);
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeNoPassKey(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        unset($_POST['passkey']);
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeInvalidType(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey124';
        $_POST['type'] = 'invalid';
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassChangeUnsetType(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey124';
        unset($_POST['type']);
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
    }
    public function testValidatePassCorrectData(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue(!$this->model->hasError());
    }
    public function testSaveNewPassAfterIncorrectValidation(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST = [];
        $this->model->validatePassChange();
        assertTrue($this->model->hasError());
        assertTrue(!$this->model->saveNewPass());
    }
    public function testSaveNewPassAfterCorrectValidation(): void
    {
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue(!$this->model->hasError());
        assertTrue($this->model->saveNewPass());
    }
    public function testSaveNewPassConnactionfail1(): void
    {
        $this->persistenceMock->connectionLostOnCall(2);
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue(!$this->model->hasError());
        assertTrue(!$this->model->saveNewPass());
    }
    public function testSaveNewPassConnactionfail2(): void
    {
        $this->persistenceMock->connectionLostOnCall(3);
        assertTrue(!$this->model->hasError());
        $_POST['username'] = 'testUser_id';
        $_POST['newpass'] = 'test12';
        $_POST['passagain'] = 'test12';
        $_POST['passkey'] = 'resetkey123';
        $_POST['type'] = 'forgotten';
        $this->model->validatePassChange();
        assertTrue(!$this->model->hasError());
        assertTrue(!$this->model->saveNewPass());
    }
}
