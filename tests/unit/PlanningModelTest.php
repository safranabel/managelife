<?php

declare(strict_types=1);

namespace Managelife\Planning;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Planning\PlanningModel;

/**
 * @covers \Managelife\Planning\PlanningModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 */
final class PlanningModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private PlanningModel $model;

    public function setUp(): void
    {
        $this->model = new PlanningModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetTableJSON(): void
    {
        // normal empty
        $this->setUp();
        $_GET['year'] = '2022';
        $_GET['month'] = '5';
        assertEquals($this->model->getTableJSON(), "{}");

        // normal with plans
        $this->setUp();
        $this->persistenceMock->addPlan('Expenditure', 'CAT', 'HUF', 1200);
        $this->persistenceMock->addPlan('Expenditure', 'CATT', 'EUR', 50.5);
        $this->persistenceMock->addPlan('Income', 'CATEGORY', 'HUF', 1000);
        $this->persistenceMock->addPlan('Income', 'CATTEGORY', 'EUR', 12.5);
        $_GET['year'] = '2022';
        $_GET['month'] = '5';
        $req = "";
        $req .= '{';
        $req .=     '"Expenditure":{';
        $req .=         '"CAT":{';
        $req .=             '"HUF":1200';
        $req .=         '},';
        $req .=         '"CATT":{';
        $req .=             '"EUR":50.5';
        $req .=         '}';
        $req .=     '},';
        $req .=     '"Income":{';
        $req .=         '"CATEGORY":{';
        $req .=             '"HUF":1000';
        $req .=        '},';
        $req .=        '"CATTEGORY":{';
        $req .=            '"EUR":12.5';
        $req .=        '}';
        $req .=    '}';
        $req .= '}';
        assertEquals($this->model->getTableJSON(), $req);

        // no year
        $this->setUp();
        unset($_GET['year']);
        $_GET['month'] = '5';
        assertEquals($this->model->getTableJSON(), "{}");

        // no month
        $this->setUp();
        $_GET['year'] = '2022';
        unset($_GET['month']);
        assertEquals($this->model->getTableJSON(), "{}");

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['year'] = '2022';
        $_GET['month'] = '5';
        assertEquals($this->model->getTableJSON(), "{}");
    }

    public function testgetYear(): void
    {
        // normal get is set
        $this->setUp();
        $_GET['year'] = '2022';
        assertEquals($this->model->getYear(), "2022");

        // normal get is not set
        $this->setUp();
        unset($_GET['year']);
        assertEquals($this->model->getYear(), strval(date('Y')));
    }

    public function testgetMonth(): void
    {
        // normal get is set
        $this->setUp();
        $_GET['month'] = '5';
        assertEquals($this->model->getMonth(), "5");

        // normal get is not set
        $this->setUp();
        unset($_GET['month']);
        assertEquals($this->model->getMonth(), strval(date('m')));
    }
}
