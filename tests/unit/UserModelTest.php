<?php

declare(strict_types=1);

namespace Managelife\UserIndex;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use DateTime;
use PHPUnit\Framework\TestCase;
use Managelife\Persistence\UserHandlerPersistenceMock;
use Managelife\UserIndex\UserModel;

/**
 * @covers \Managelife\UserIndex\UserModel
 * @covers \Managelife\Model\BaseModel
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Validator\Validator
 */
final class UserModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private UserModel $model;
    public function setUp(): void
    {
        $this->model = new UserModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetAllCurrenciesCorrect(): void
    {
        assertEquals($this->model->getAllCurrencies(), [
            'EUR',
            'GBP',
            'HKR',
            'HUF',
            'RUB',
            'USD'
        ]);
    }
    public function testBaseModelGetAllCurrenciesPersistenceFailure(): void
    {
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getAllCurrencies(), []);
    }
    public function testSetUserData(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getForename(), 'Testforename');
    }
    public function testGetUserCategories(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCategories(), [
            'Food',
            'Other',
            'Scholarship',
            'Taxes',
            'Work'
        ]);
    }
    public function testGetUserCurrencies(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCurrencies(), [
            'EUR',
            'HUF'
        ]);
    }
    public function testGetUserCategoriesWithIncomeTrue(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCategoriesWithIncome(1), [
            'Other',
            'Scholarship',
            'Work'
        ]);
    }
    public function testGetUserCategoriesWithIncomeFalse(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCategoriesWithIncome(0), [
            'Food',
            'Other',
            'Taxes'
        ]);
    }
    public function testGetUserCurrenciesWithIncomeTrue(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCurrenciesWithIncome(1), [
            'EUR'
        ]);
    }
    public function testGetUserCurrenciesWithIncomeFalse(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserCurrenciesWithIncome(0), [
            'EUR'
        ]);
    }
    public function testGetIncome(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getIncome('2021-06-01', '2021-06-30', 'EUR', 'Work'), 12.50);
    }
    public function testGetExpenditure(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getExpenditure('2021-06-01', '2021-06-30', 'EUR', 'Food'), 120);
    }
    public function testGetMonthlyBalance(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getMonthlyBalance(new DateTime('2021-06-01'), 'EUR'), -107.50 * 5);
    }
    public function testGetUserTransactions(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserTransactions(), [
            [
                'date' => '2021-06-30',
                'time' => '12:15',
                'category' => 'Other',
                'comment' => '-',
                'income' => 1,
                'user_id' => 1,
                'amount' => 12.50,
                'currency' => 'EUR'
            ],
            [
                'date' => '2021-07-30',
                'time' => '12:16',
                'category' => 'Food',
                'comment' => '-',
                'income' => 0,
                'user_id' => 1,
                'amount' => 120,
                'currency' => 'EUR'
            ]
        ]);
    }

    public function testGetUserTransactionsConnectionError(): void
    {
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(1);
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getUserTransactions(), []);
    }

    public function testGetBalance(): void
    {
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getBalance('EUR'), -107.50);
    }

    public function testGetEmail(): void
    {
        $this->setUp();
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getEmail(), 'test@test.com');
    }

    public function testGetForename(): void
    {
        $this->setUp();
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getForename(), 'Testforename');
    }

    public function testGetSurname(): void
    {
        $this->setUp();
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getSurname(), 'Testsurname');
    }

    public function testGetPlan(): void
    {
        $this->setUp();
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getPlan(
            2022,
            5,
            true,
            "CAT",
            "EUR"
        ), 50.5);
    }

    public function testGetPlanConnectionError(): void
    {
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );

        assertEquals($this->model->getPlan(
            2022,
            5,
            true,
            "CAT",
            "EUR"
        ), 0);
    }
}
