<?php

declare(strict_types=1);

namespace Managelife\Notification;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Notification\NotificationModel;

/**
 * @covers \Managelife\Notification\NotificationModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 */
final class NotificationModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private NotificationModel $model;

    public function setUp(): void
    {
        $this->model = new NotificationModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetSelectedNotifOpt(): void
    {
        // normal
        $this->setUp();
        $this->persistenceMock->setSelectedNotifOption("weekly");
        assertEquals($this->model->getSelectedNotificationOption(), "weekly");

        // unidentified value
        $this->setUp();
        $this->persistenceMock->setSelectedNotifOption("unreal");
        assertEquals($this->model->getSelectedNotificationOption(), "no");

        // connection error
        $this->setUp();
        $this->persistenceMock->setSelectedNotifOption("weekly");
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getSelectedNotificationOption(), "no");
    }

    public function testNotifSettingValid(): void
    {
        // normal daily
        $this->setUp();
        $_POST['type'] = 'd';
        assertTrue($this->model->notifSettingValid());

        // normal weekly
        $this->setUp();
        $_POST['type'] = 'w';
        assertTrue($this->model->notifSettingValid());

        // normal monthly
        $this->setUp();
        $_POST['type'] = 'm';
        assertTrue($this->model->notifSettingValid());

        // normal no
        $this->setUp();
        $_POST['type'] = 'n';
        assertTrue($this->model->notifSettingValid());

        // Empty post
        $this->setUp();
        $_POST = [];
        assertFalse($this->model->notifSettingValid());

        // Invalid type
        $this->setUp();
        $_POST['type'] = 'i';
        assertFalse($this->model->notifSettingValid());
    }

    public function testSaveNotifSetting(): void
    {
        // normal daily
        $this->setUp();
        $_POST['type'] = 'd';
        assertTrue($this->model->saveNotifSetting());

        // normal weekly
        $this->setUp();
        $_POST['type'] = 'w';
        assertTrue($this->model->saveNotifSetting());

        // normal monthly
        $this->setUp();
        $_POST['type'] = 'm';
        assertTrue($this->model->saveNotifSetting());

        // normal no
        $this->setUp();
        $_POST['type'] = 'n';
        assertTrue($this->model->saveNotifSetting());

        // Empty post
        $this->setUp();
        $_POST = [];
        assertFalse($this->model->saveNotifSetting());

        // Invalid type
        $this->setUp();
        $_POST['type'] = 'i';
        assertFalse($this->model->saveNotifSetting());

        // Connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_POST['type'] = 'n';
        assertFalse($this->model->saveNotifSetting());
    }
}
