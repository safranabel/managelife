<?php

declare(strict_types=1);

namespace Managelife\UserCheck;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\UserCheck\UserCheckModel;

/**
 * @covers \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Model\BaseModel
 */
final class UserCheckModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private UserCheckModel $model;
    public function setUp(): void
    {
        $this->model = new UserCheckModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
    }

    public function testRefreshUserDataWithCorrectData(): void
    {
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';

        $this->model->refreshUserData();
        assertTrue($this->model->userValid());
        assertFalse($this->model->isAdmin());
        assertEquals($this->model->getUserid(), 'testUser_id');
        assertEquals($this->model->getEmail(), 'test@test.com');
        assertEquals($this->model->getForename(), 'Testforename');
        assertEquals($this->model->getSurname(), 'Testsurname');
    }

    public function testRefreshUserDataWithCorrectAdminData(): void
    {
        $this->persistenceMock->setUserAdmin(1);
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '07909f5928f186da6c851a32067867313cef84bd80fe94c680f93accb8535297';

        $this->model->refreshUserData();
        assertTrue($this->model->userValid());
        assertTrue($this->model->isAdmin());
        assertEquals($this->model->getUserid(), 'testUser_id');
        assertEquals($this->model->getEmail(), 'test@test.com');
        assertEquals($this->model->getForename(), 'Testforename');
        assertEquals($this->model->getSurname(), 'Testsurname');
    }

    public function testWithoutRefreshUserData(): void
    {
        $this->persistenceMock->setUserAdmin(1);
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '07909f5928f186da6c851a32067867313cef84bd80fe94c680f93accb8535297';

        assertFalse($this->model->userValid());
        assertFalse($this->model->isAdmin());
        assertEquals($this->model->getUserid(), '');
        assertEquals($this->model->getEmail(), '');
        assertEquals($this->model->getForename(), '');
        assertEquals($this->model->getSurname(), '');
    }

    public function testRefreshUserDataWithoutSessionToken(): void
    {
        $this->persistenceMock->setUserAdmin(1);
        $_SESSION['username'] = 'testUser_id';
        unset($_SESSION['token']);

        $this->model->refreshUserData();

        assertFalse($this->model->userValid());
        assertFalse($this->model->isAdmin());
        assertEquals($this->model->getUserid(), '');
        assertEquals($this->model->getEmail(), '');
        assertEquals($this->model->getForename(), '');
        assertEquals($this->model->getSurname(), '');
    }

    public function testRefreshUserDataWithoutSessionUser(): void
    {
        $this->persistenceMock->setUserAdmin(1);
        unset($_SESSION['username']);
        $_SESSION['token'] = '07909f5928f186da6c851a32067867313cef84bd80fe94c680f93accb8535297';

        $this->model->refreshUserData();

        assertFalse($this->model->userValid());
        assertFalse($this->model->isAdmin());
        assertEquals($this->model->getUserid(), '');
        assertEquals($this->model->getEmail(), '');
        assertEquals($this->model->getForename(), '');
        assertEquals($this->model->getSurname(), '');
    }

    public function testValidateDate(): void
    {
        // correct form yyyy-mm-dd
        assertTrue($this->model->validateDate("2022-05-01"));

        // correct form yyyy. mm. dd.
        assertTrue($this->model->validateDate("2022. 05. 01.", "Y. m. d."));

        // correct form yyyy/mm/dd
        assertTrue($this->model->validateDate("2022/05/01", "Y/m/d"));

        // invalid date (13-01)
        assertFalse($this->model->validateDate("2022-13-01"));

        // invalid date (05-35)
        assertFalse($this->model->validateDate("2022-05-35"));

        // invalid date
        assertFalse($this->model->validateDate("20220501"));

        // invalid date (incorrect form)
        assertFalse($this->model->validateDate("2022-05-01", "Y/m/d"));
    }

    public function testValidateTime(): void
    {
        // correct form hh:mm
        assertTrue($this->model->validateTime("02:00"));

        // correct form hh:mm adternoon
        assertTrue($this->model->validateTime("15:30"));

        // invalid form hh.mm
        assertFalse($this->model->validateTime("12.00"));

        // invalid hour
        assertFalse($this->model->validateTime("24:00"));

        // invalid hour txt
        assertFalse($this->model->validateTime("hh:00"));

        // invalid min
        assertFalse($this->model->validateTime("12:60"));

        // invalid min txt
        assertFalse($this->model->validateTime("12:mm"));
    }
}
