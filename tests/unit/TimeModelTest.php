<?php

declare(strict_types=1);

namespace Managelife\Time;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Time\TimeModel;

/**
 * @covers \Managelife\Time\TimeModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 */
final class TimeModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private TimeModel $model;

    public function setUp(): void
    {
        $this->model = new TimeModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $_POST = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testHasError(): void
    {
        $this->setUp();
        assertTrue($this->model->hasError());
    }

    public function testGetToDos(): void
    {
        // empty
        $this->setUp();
        assertEquals($this->model->getToDos(), []);

        // normal, added
        $this->setUp();
        $this->persistenceMock->addToDoMock(
            1,
            "MyTodo",
            "My todo todo todo",
            "2022-05-01",
            "23:59"
        );
        assertEquals($this->model->getToDos(), [1 => [
            'id' => 1,
            'title' => "MyTodo",
            'description' => "My todo todo todo",
            'date' => "2022-05-01",
            'time' => '23:59'
        ]]);

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->model->getToDos(), []);
    }

    public function testTodoValid(): void
    {
        // correct todo, Y-m-d
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = "MyTodo Todo todo";
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        assertTrue($this->model->todoValid());
        assertFalse($this->model->hasError());

        // correct todo, no description, Y. m. d.
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        unset($_POST['tododesc']);
        $_POST['tododate'] = "2022. 05. 01.";
        $_POST['todotime'] = "23:59";
        assertTrue($this->model->todoValid());
        assertFalse($this->model->hasError());

        // correct todo, no description, Y/m/d
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = null;
        $_POST['tododate'] = "2022/05/01";
        $_POST['todotime'] = "23:59";
        assertTrue($this->model->todoValid());
        assertFalse($this->model->hasError());

        // Invalid date, too big month
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-13-01";
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid date, too big day
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-02-31";
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid date, text
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "randtext";
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());


        // Invalid time, too big hour
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "24:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid time, too big min
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "22:60";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid time, text
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "txt";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, no title
        $this->setUp();
        $_POST['todotitle'] = null;
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, no title
        $this->setUp();
        $_POST['todotitle'] = '';
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, no date
        $this->setUp();
        $_POST['todotitle'] = 'My Title';
        $_POST['tododesc'] = 'D';
        unset($_POST['tododate']);
        $_POST['todotime'] = "23:59";
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, no time
        $this->setUp();
        $_POST['todotitle'] = 'My Title';
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        unset($_POST['todotime']);
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, no data
        $this->setUp();
        $_POST = [];
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());

        // Invalid, too long description
        $this->setUp();
        $_POST['todotitle'] = 'My Title';
        $_POST['tododesc'] = 'D';
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        $desc = "";
        for ($i = 0; $i < 201; $i++) {
            $desc .= 'A';
        }
        $_POST['tododesc'] = $desc;
        assertFalse($this->model->todoValid());
        assertTrue($this->model->hasError());
    }

    public function testSaveToDo(): void
    {
        // normal, after validate
        $this->setUp();
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = "MyTodo Todo todo";
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        assertTrue($this->model->todoValid());
        assertFalse($this->model->hasError());
        assertTrue($this->model->saveToDo());


        // no validation
        $this->setUp();
        assertTrue($this->model->hasError());
        assertFalse($this->model->saveToDo());

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_POST['todotitle'] = "MyTodo";
        $_POST['tododesc'] = "MyTodo Todo todo";
        $_POST['tododate'] = "2022-05-01";
        $_POST['todotime'] = "23:59";
        assertTrue($this->model->todoValid());
        assertFalse($this->model->hasError());
        assertFalse($this->model->saveToDo());
    }

    public function testdDeleteTodo(): void
    {
        // normal, after validate
        $this->setUp();
        $_GET['id'] = '1';
        assertTrue($this->model->deleteTodo());

        // nan id
        $this->setUp();
        $_GET['id'] = 'nan';
        assertFalse($this->model->deleteTodo());

        // no id
        $this->setUp();
        unset($_GET['id']);
        assertFalse($this->model->deleteTodo());

        // connection lost
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        $_GET['id'] = '1';
        assertFalse($this->model->deleteTodo());
    }
}
