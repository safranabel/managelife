<?php

declare(strict_types=1);

namespace Managelife\Registration;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Registration\RegistrationModel;

/**
 * @covers \Managelife\Registration\RegistrationModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\Model\BaseModel
 */
final class RegistrationModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private RegistrationModel $model;
    public function setUp(): void
    {
        $this->model = new RegistrationModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
    }

    public function testValidateWithCorrectData(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue(!$this->model->hasError(), 'There should be no error with correct data');
        assertEquals($this->model->getErrorFields(), [], 'There should be no error with correct data');
    }

    public function testValidateWithIncorrectUsername(): void
    {
        $_POST['userid'] = 'test userid';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with incorrect username');
        assertEquals($this->model->getErrorFields(), ['username'], 'When only the username is incorrect, then only the username field should be in the errors');
    }



    public function testValidateWithPassWithoutNumber(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'TestPass';
        $_POST['passagain'] = 'TestPass';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with incorrect password (not includes numbers)');
        assertEquals($this->model->getErrorFields(), ['password'], 'When only the password is incorrect, then only the password field should be in the errors');
    }



    public function testValidateWithPassWithoutAlphabetChar(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = '123456';
        $_POST['passagain'] = '123456';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with incorrect password (not includes alphabet chars)');
        assertEquals($this->model->getErrorFields(), ['password'], 'When only the password is incorrect, then only the password field should be in the errors');
    }

    public function testValidateWithShortPass(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test1';
        $_POST['passagain'] = 'Test1';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error with incorrect password (too short)');
        assertEquals($this->model->getErrorFields(), ['password'], 'When only the password is incorrect, then only the password field should be in the errors');
    }



    public function testValidateWithIncorrectPassAgain(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test13';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error when the passwords are not equal');
        assertEquals($this->model->getErrorFields(), ['passagain'], 'When only the passagain filed is incorrect, then only the passagain field should be in the errors');
    }



    public function testValidateWithIncorrectRegKey(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB4';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error when the registration key is incorrect');
        assertEquals($this->model->getErrorFields(), ['regkey'], 'When only the registration key is incorrect, then only the regkey field should be in the errors');
    }



    public function testValidateWithIncorrectEmailDotMissing(): void
    {
        $_POST['userid'] = 'testuserid';
        $_POST['email'] = 'test@testcom';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error when the e-mail is incorrect (not includes dot)');
        assertEquals($this->model->getErrorFields(), ['email'], 'When only the e-mail is incorrect, then only the e-mail field should be in the errors');
    }



    public function testValidateWithIncorrectEmailAtMissing(): void
    {
        $_POST['userid'] = 'testuserid';
        $_POST['email'] = 'testtest.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'There should be an error when the e-mail is incorrect (not includes at)');
        assertEquals($this->model->getErrorFields(), ['email'], 'When only the e-mail is incorrect, then only the e-mail field should be in the errors');
    }



    public function testValidateWithMultipleMistake(): void
    {
        $_POST['userid'] = 'test userid';
        $_POST['email'] = 'testtest.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test1';
        $_POST['passagain'] = 'Test1';
        $_POST['regkey'] = 'ThKww367lhTZ4gb3';

        $this->model->validate();
        assertTrue($this->model->hasError(), 'Multiple mistake');
        assertTrue(in_array('username', $this->model->getErrorFields()), 'Username should be included in the errors');
        assertTrue(in_array('email', $this->model->getErrorFields()), 'e-mail should be included in the errors');
        assertTrue(in_array('password', $this->model->getErrorFields()), 'Password should be included in the errors');
        assertTrue(in_array('regkey', $this->model->getErrorFields()), 'REgistration key should be included in the errors');
    }
    public function testValidateWithExistingData(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['email'] = 'test@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Existing data');
        assertTrue(in_array('username', $this->model->getErrorFields()), 'Username should be unique');
        assertTrue(in_array('email', $this->model->getErrorFields()), 'e-mail should be unique');
    }

    public function testValidateWithNullUsername(): void
    {
        $_POST['userid'] = null;
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Null data');
        assertTrue(in_array('username', $this->model->getErrorFields()), 'Username should be set');
    }

    public function testValidateWithUnsetEmail(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        unset($_POST['email']);
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Unset email');
        assertTrue(in_array('email', $this->model->getErrorFields()), 'Email should be set');
    }

    public function testValidateWithNullForename(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = null;
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Null data');
        assertTrue(in_array('forename', $this->model->getErrorFields()), 'Forename should be set');
    }

    public function testValidateWithNullSurname(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = null;
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Null data');
        assertTrue(in_array('surname', $this->model->getErrorFields()), 'Surname should be set');
    }

    public function testValidateWithUnsetPassagain(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        unset($_POST['passagain']);
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Null data');
        assertTrue(in_array('passagain', $this->model->getErrorFields()), 'Passagain should be set');
    }

    public function testValidateWithUnsetRegKey(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'TestSurname';
        $_POST['password'] = 'Test12';
        $_POST['passagain'] = 'Test12';
        unset($_POST['regkey']);


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Null data');
        assertTrue(in_array('regkey', $this->model->getErrorFields()), 'Regkey should be set');
    }

    public function testValidateWithEmptyPost(): void
    {
        $_POST = [];


        $this->model->validate();
        assertTrue($this->model->hasError(), 'Empty request');
    }

    public function testSaveAfterSuccessfulValidation(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->model->save());
    }

    public function testSaveAfterUnsuccessfulValidation(): void
    {
        $_POST['userid'] = 'testUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue(!$this->model->save());
    }

    public function testSaveWithUnstableConnection(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->persistenceMock->connectionLostOnCall(2);

        $this->model->validate();
        assertTrue(!$this->model->save());
    }

    public function testLoginUser(): void
    {
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue(!$this->model->hasError(), 'There should be no error with correct data');
        assertEquals($this->model->getErrorFields(), []);
        $this->model->loginUser();
        assertEquals($_SESSION['username'], 'newtestUser_id');
    }
}
