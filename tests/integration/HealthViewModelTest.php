<?php

declare(strict_types=1);

namespace Managelife\Health;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Health\HealthModel;
use Managelife\Health\HealthViewModel;

/**
 * @covers \Managelife\Health\HealthViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Health\HealthModel
 */
final class HealthViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private HealthModel $model;
    private HealthViewModel $vm;

    public function setUp(): void
    {
        $this->model = new HealthModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $this->vm = new HealthViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetWeightGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getWeightGoal(), "80.5");

        // negative
        $this->setUp();
        $this->persistenceMock->setWeightGoalMock(-5);
        assertEquals($this->vm->getWeightGoal(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getWeightGoal(), "");
    }

    public function testGetWaterGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getWaterGoal(), "3.5");

        // negative
        $this->setUp();
        $this->persistenceMock->setWaterGoalMock(-5);
        assertEquals($this->vm->getWaterGoal(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getWaterGoal(), "");
    }

    public function testGetCalorieGoal(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getCalorieGoal(), "12000");

        // negative
        $this->setUp();
        $this->persistenceMock->setCalorieGoalMock(-5);
        assertEquals($this->vm->getCalorieGoal(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getCalorieGoal(), "");
    }

    public function testGetActualWaterLevel(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getActualWaterLevel(), "2.3");

        // negative
        $this->setUp();
        $this->persistenceMock->setActualWaterLevelMock(-5);
        assertEquals($this->vm->getActualWaterLevel(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getActualWaterLevel(), "");
    }

    public function testGetActualCalorieLevel(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getActualCalorieLevel(), "5500");

        // negative
        $this->setUp();
        $this->persistenceMock->setActualCalorieLevelMock(-5);
        assertEquals($this->vm->getActualCalorieLevel(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getActualCalorieLevel(), "");
    }

    public function testGetWaterProgress(): void
    {
        // normal
        $this->setUp();
        $this->persistenceMock->setWaterGoalMock(5.0);
        $this->persistenceMock->setActualWaterLevelMock(2);
        assertEquals($this->vm->getWaterProgress(), "40");

        // normal
        $this->setUp();
        $this->persistenceMock->setWaterGoalMock(5.0);
        $this->persistenceMock->setActualWaterLevelMock(0);
        assertEquals($this->vm->getWaterProgress(), "0");

        // normal
        $this->setUp();
        $this->persistenceMock->setWaterGoalMock(5);
        $this->persistenceMock->setActualWaterLevelMock(5.3);
        assertEquals($this->vm->getWaterProgress(), "100");

         // normal
         $this->setUp();
         $this->persistenceMock->setWaterGoalMock(0);
         $this->persistenceMock->setActualWaterLevelMock(5.3);
         assertEquals($this->vm->getWaterProgress(), "0");

        // negative
        $this->setUp();
        $this->persistenceMock->setActualWaterLevelMock(-5);
        assertEquals($this->vm->getWaterProgress(), "0");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getWaterProgress(), "0");
    }

    public function testGetCalorieProgress(): void
    {
        // normal
        $this->setUp();
        $this->persistenceMock->setCalorieGoalMock(10000);
        $this->persistenceMock->setActualCalorieLevelMock(1000);
        assertEquals($this->vm->getCalorieProgress(), "10");

        // normal
        $this->setUp();
        $this->persistenceMock->setCalorieGoalMock(12000);
        $this->persistenceMock->setActualCalorieLevelMock(0);
        assertEquals($this->vm->getCalorieProgress(), "0");

        // normal
        $this->setUp();
        $this->persistenceMock->setCalorieGoalMock(12000);
        $this->persistenceMock->setActualCalorieLevelMock(12001);
        assertEquals($this->vm->getCalorieProgress(), "100");

         // normal
         $this->setUp();
         $this->persistenceMock->setCalorieGoalMock(0);
         $this->persistenceMock->setActualCalorieLevelMock(3000);
         assertEquals($this->vm->getCalorieProgress(), "0");

        // negative
        $this->setUp();
        $this->persistenceMock->setActualCalorieLevelMock(-5);
        assertEquals($this->vm->getCalorieProgress(), "0");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getCalorieProgress(), "0");
    }

    public function testGetActualWeight(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getActualWeight(), "79.5");

        // negative
        $this->setUp();
        $this->persistenceMock->setActualWeightMock(-5);
        assertEquals($this->vm->getActualWeight(), "");

        // connection error
        $this->setUp();
        $this->persistenceMock->connectionLostOnCall(0);
        assertEquals($this->vm->getActualWeight(), "");
    }

    public function testGetWeightSuccesClass(): void
    {
        // normal < 3
        $this->setUp();
        $this->persistenceMock->setWeightGoalMock(80);
        $this->persistenceMock->setActualWeightMock(82.9);
        assertEquals($this->vm->getWeightSuccesClass(), "text-success");

        // normal < 5
        $this->setUp();
        $this->persistenceMock->setWeightGoalMock(80);
        $this->persistenceMock->setActualWeightMock(84.9);
        assertEquals($this->vm->getWeightSuccesClass(), "text-warning bg-dark");

        // normal > 5
        $this->setUp();
        $this->persistenceMock->setWeightGoalMock(80);
        $this->persistenceMock->setActualWeightMock(74.9);
        assertEquals($this->vm->getWeightSuccesClass(), "text-danger");
    }
}
