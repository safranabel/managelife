<?php

declare(strict_types=1);

namespace Managelife\Settings;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Notification\NotificationModel;
use Managelife\Settings\SettingsViewModel;

/**
 * @covers \Managelife\Settings\SettingsViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\ChangePass\ChangePassViewModel
 * @uses \Managelife\Notification\NotificationModel
 */
final class SettingsViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private NotificationModel $model;
    private SettingsViewModel $vm;

    public function setUp(): void
    {
        $this->model = new NotificationModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $this->vm = new SettingsViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetMessage(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getMessage(), "");

        // PasswordError
        $this->setUp();
        $_GET['errors'] = "PasswordError";
        $req = '';
        $req .= '<ul class="error">';
        $req .= '<li>Your given password is incorrect(password should contain at least one number, one letter and 6 characters)</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getMessage(), $req);

        // PassAgainError
        $this->setUp();
        $_GET['errors'] = "PassAgainError";
        $req = '';
        $req .= '<ul class="error">';
        $req .= '<li>The two passwords are not the same.</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getMessage(), $req);

        // OldPassError
        $this->setUp();
        $_GET['errors'] = "OldPassError";
        $req = '';
        $req .= '<ul class="error">';
        $req .= '<li>Your given old password is not correct.</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getMessage(), $req);

        // Unknown
        $this->setUp();
        $_GET['errors'] = "unknown";
        $req = '';
        $req .= '<ul class="error">';
        $req .= '<li>Some error occurred, please try again</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getMessage(), $req);
    }

    public function testSelectNotifOpt(): void
    {
        // normal - weekly
        $this->setUp();
        $this->persistenceMock->setSelectedNotifOption('weekly');
        assertEquals($this->vm->selectNotifOpt('daily'), '');
        assertEquals($this->vm->selectNotifOpt('weekly'), 'selected');
        assertEquals($this->vm->selectNotifOpt('monthly'), '');
        assertEquals($this->vm->selectNotifOpt('no'), '');

        // normal - no
        $this->setUp();
        $this->persistenceMock->setSelectedNotifOption('no');
        assertEquals($this->vm->selectNotifOpt('daily'), '');
        assertEquals($this->vm->selectNotifOpt('weekly'), '');
        assertEquals($this->vm->selectNotifOpt('monthly'), '');
        assertEquals($this->vm->selectNotifOpt('no'), 'selected');
    }
}
