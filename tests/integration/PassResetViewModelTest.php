<?php

declare(strict_types=1);

namespace Managelife\PassReset;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\PassReset\PassResetModel;
use Managelife\PassReset\PassResetViewModel;

/**
 * @covers \Managelife\PassReset\PassResetViewModel
 * @uses \Managelife\PassReset\PassResetModel
 */
final class PassResetViewModelTest extends TestCase
{
    private PassResetViewModel $vm;

    public function setUp(): void
    {
        $this->vm = new PassResetViewModel();
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetMessage(): void
    {
        // normal - success
        $this->setUp();
        unset($_GET['errors']);
        $_GET['success'] = '1';
        $req  = "<p>&#9989; We have successfully sent the password reset email to your address. ";
        $req .= "<br>";
        $req .= "Do not forget to check your spam folder for the password reset email!</p>";
        assertEquals($this->vm->getMessage(), $req);

        // normal - EmailError
        $this->setUp();
        $_GET['errors'] = 'EmailError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Your given email is incorrect or does not belong to anyone in our database</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - ServerError
        $this->setUp();
        $_GET['errors'] = 'ServerError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>A server error occurred, please try again later</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - unknown error
        $this->setUp();
        $_GET['errors'] = 'unknown';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Some error occurred, please try again</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - combined
        $this->setUp();
        $_GET['errors'] = 'EmailError,ServerError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Your given email is incorrect or does not belong to anyone in our database</li>';
        $req .= '<li>A server error occurred, please try again later</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // no data in get
        $this->setUp();
        $_GET = [];
        assertEquals($this->vm->getMessage(), '');
    }
}
