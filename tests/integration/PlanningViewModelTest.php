<?php

declare(strict_types=1);

namespace Managelife\Planning;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Planning\PlanningModel;
use Managelife\Planning\PlanningViewModel;

/**
 * @covers \Managelife\Planning\PlanningViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Planning\PlanningModel
 */
final class PlanningViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private PlanningModel $model;
    private PlanningViewModel $vm;

    public function setUp(): void
    {
        $this->model = new PlanningModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $this->vm = new PlanningViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetYear(): void
    {
        // normal
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getYear(), "2021");
    }

    public function testGetMonth(): void
    {
        // normal - < 10
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getMonth(), "04");

        // normal - >= 10
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '11';
        assertEquals($this->vm->getMonth(), "11");
    }

    public function testGetPrevYear(): void
    {
        // normal
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getPrevYear(), "2021");

        // normal - January
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '1';
        assertEquals($this->vm->getPrevYear(), "2020");
    }

    public function testGetPrevMonth(): void
    {
        // normal
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getPrevMonth(), "03");

        // normal - January
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '1';
        assertEquals($this->vm->getPrevMonth(), "12");
    }

    public function testGetNextYear(): void
    {
        // normal
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getNextYear(), "2021");

        // normal - December
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '12';
        assertEquals($this->vm->getNextYear(), "2022");
    }

    public function testGetNextMonth(): void
    {
        // normal - < 10
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '4';
        assertEquals($this->vm->getNextMonth(), "05");

        // normal - >= 10
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '11';
        assertEquals($this->vm->getNextMonth(), "12");

        // normal - December
        $this->setUp();
        $_GET['year'] = '2021';
        $_GET['month'] = '12';
        assertEquals($this->vm->getNextMonth(), "01");
    }
}
