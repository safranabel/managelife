<?php

declare(strict_types=1);

namespace Managelife\Time;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Time\TimeModel;
use Managelife\Time\TimeViewModel;

/**
 * @covers \Managelife\Time\TimeViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Time\TimeModel
 */
final class TimeViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private TimeModel $model;
    private TimeViewModel $vm;

    public function setUp(): void
    {
        $this->model = new TimeModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->model->refreshUserData();
        $this->vm = new TimeViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetToDoRows(): void
    {
        // init - empty
        $this->setUp();
        assertEquals($this->vm->getToDoRows(), "");

        // with todos
        $this->setUp();
        $this->persistenceMock->addToDoMock(
            1,
            "MyTitle",
            "MyDesccription",
            "2022-05-01",
            "23:59"
        );
        $req = "";
        $req .= '<tr><td>MyTitle</td>';
        $req .= '<td>MyDesccription</td>';
        $req .= '<td>2022-05-01</td>';
        $req .= '<td>23:59</td>';
        $req .= '<td><form action="./controllers/deletetodo.php?id=1" method="post">';
        $req .= '<input type="submit" value="Delete" class="btn btn-danger"></form></td>';
        assertEquals($this->vm->getToDoRows(), $req);
    }
}
