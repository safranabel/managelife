<?php

declare(strict_types=1);

namespace Managelife\UserIndex;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\UserIndex\UserViewModel;
use Managelife\UserIndex\UserModel;

/**
 * @covers \Managelife\UserIndex\UserViewModel
 * @uses \Managelife\UserIndex\UserModel
 * @uses \Managelife\Model\BaseModel
 */
final class HealthViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private UserModel $model;
    private UserViewModel $vm;

    public function setUp(): void
    {
        $this->model = new UserModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->vm = new UserViewModel($this->model);
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforename',
            'Testsurname'
        );
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetErrorList(): void
    {
        // normal - no error
        $this->setUp();
        unset($_GET['errors']);
        assertEquals($this->vm->getErrorList(), "");

        // DateError
        $this->setUp();
        $_GET['errors'] = 'DateError';
        $req = "";
        $req .= '<ul class="error">';
        $req .= '<li>';
        $req .= 'Date is in incorrect format or missing';
        $req .= '</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getErrorList(), $req);

        // TimeError
        $this->setUp();
        $_GET['errors'] = 'TimeError';
        $req = "";
        $req .= '<ul class="error">';
        $req .= '<li>';
        $req .= 'Time is in incorrect format or missing';
        $req .= '</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getErrorList(), $req);

        // AmountError
        $this->setUp();
        $_GET['errors'] = 'AmountError';
        $req = "";
        $req .= '<ul class="error">';
        $req .= '<li>';
        $req .= 'Amount is in incorrect format or missing';
        $req .= '</li>';
        $req .= '</ul>';
        assertEquals($this->vm->getErrorList(), $req);

         // CurrencyError
         $this->setUp();
         $_GET['errors'] = 'CurrencyError';
         $req = "";
         $req .= '<ul class="error">';
         $req .= '<li>';
         $req .= 'Chosen currency is not exist';
         $req .= '</li>';
         $req .= '</ul>';
         assertEquals($this->vm->getErrorList(), $req);

         // CatError
         $this->setUp();
         $_GET['errors'] = 'CatError';
         $req = "";
         $req .= '<ul class="error">';
         $req .= '<li>';
         $req .= 'Chosen category is not exist';
         $req .= '</li>';
         $req .= '</ul>';
         assertEquals($this->vm->getErrorList(), $req);

         // NewCatError
         $this->setUp();
         $_GET['errors'] = 'NewCatError';
         $req = "";
         $req .= '<ul class="error">';
         $req .= '<li>';
         $req .= 'Your new category name is incorrect (length of a category should be between 3-99 character and should contain only letters)';
         $req .= '</li>';
         $req .= '</ul>';
         assertEquals($this->vm->getErrorList(), $req);

         // CommentError
         $this->setUp();
         $_GET['errors'] = 'CommentError';
         $req = "";
         $req .= '<ul class="error">';
         $req .= '<li>';
         $req .= 'Your comment text is incorrect (length of a comment should be between 1-249 character)';
         $req .= '</li>';
         $req .= '</ul>';
         assertEquals($this->vm->getErrorList(), $req);

          // TypeError
          $this->setUp();
          $_GET['errors'] = 'TypeError';
          $req = "";
          $req .= '<ul class="error">';
          $req .= '<li>';
          $req .= 'The type of the transaction is incorrect, it should be income or expenditure';
          $req .= '</li>';
          $req .= '</ul>';
          assertEquals($this->vm->getErrorList(), $req);

           // TitleError
           $this->setUp();
           $_GET['errors'] = 'TitleError';
           $req = "";
           $req .= '<ul class="error">';
           $req .= '<li>';
           $req .= 'The title of the todo is incorrect, it should be shorter than 50 characters';
           $req .= '</li>';
           $req .= '</ul>';
           assertEquals($this->vm->getErrorList(), $req);

           // DescError
           $this->setUp();
           $_GET['errors'] = 'DescError';
           $req = "";
           $req .= '<ul class="error">';
           $req .= '<li>';
           $req .= 'The description of the todo is incorrect, it should be shorter than 200 characters';
           $req .= '</li>';
           $req .= '</ul>';
           assertEquals($this->vm->getErrorList(), $req);

           // DataError
           $this->setUp();
           $_GET['errors'] = 'DataError';
           $req = "";
           $req .= '<ul class="error">';
           $req .= '<li>';
           $req .= 'Some error occurred, please try again';
           $req .= '</li>';
           $req .= '</ul>';
           assertEquals($this->vm->getErrorList(), $req);

           // Unknown
           $this->setUp();
           $_GET['errors'] = 'Unknown';
           $req = "";
           $req .= '<ul class="error">';
           $req .= '<li>';
           $req .= 'Some error occurred, please try again';
           $req .= '</li>';
           $req .= '</ul>';
           assertEquals($this->vm->getErrorList(), $req);

           // Combined
           $this->setUp();
           $_GET['errors'] = 'TypeError,CommentError';
           $req = "";
           $req .= '<ul class="error">';
           $req .= '<li>';
           $req .= 'The type of the transaction is incorrect, it should be income or expenditure';
           $req .= '</li>';
           $req .= '<li>';
           $req .= 'Your comment text is incorrect (length of a comment should be between 1-249 character)';
           $req .= '</li>';
           $req .= '</ul>';
           assertEquals($this->vm->getErrorList(), $req);
    }

    public function testGetOwnerForm(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getOwnerForm(), "Testforename's");

        // normal - ends with s
        $this->setUp();
        $this->model->setUserData(
            'testUser_id',
            'test@test.com',
            'Testforenames',
            'Testsurname'
        );
        assertEquals($this->vm->getOwnerForm(), "Testforenames'");
    }

    public function testGetForename(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getForename(), "Testforename");
    }

    public function testGetCurrencyOptions(): void
    {
        // normal
        $this->setUp();
        $req = "";
        $req .= '<option selected>EUR</option>';
        $req .= '<option>GBP</option>';
        $req .= '<option>HKR</option>';
        $req .= '<option>HUF</option>';
        $req .= '<option>RUB</option>';
        $req .= '<option>USD</option>';
        assertEquals($this->vm->getCurrencyOptions(), $req);

        // normal - selected HUF
        $this->setUp();
        $req = "";
        $req .= '<option>EUR</option>';
        $req .= '<option>GBP</option>';
        $req .= '<option>HKR</option>';
        $req .= '<option selected>HUF</option>';
        $req .= '<option>RUB</option>';
        $req .= '<option>USD</option>';
        assertEquals($this->vm->getCurrencyOptions('HUF'), $req);
    }

    public function testGetUserCategories(): void
    {
        // normal
        $this->setUp();
        $req = "";
        $req .= '<option>Food</option>';
        $req .= '<option>Other</option>';
        $req .= '<option>Scholarship</option>';
        $req .= '<option>Taxes</option>';
        $req .= '<option>Work</option>';
        assertEquals($this->vm->getUserCategories(), $req);

        // normal - selected HUF
        $this->setUp();
        $req = "";
        $req .= '<option>Food</option>';
        $req .= '<option>Other</option>';
        $req .= '<option>Scholarship</option>';
        $req .= '<option selected>Taxes</option>';
        $req .= '<option>Work</option>';
        assertEquals($this->vm->getUserCategories('Taxes'), $req);
    }

    public function testGetDate(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getDate(), strval(date("Y-m-d")));
    }

    public function testGetTime(): void
    {
        // normal
        $this->setUp();
        assertEquals($this->vm->getTime(), strval(date("H:i")));
    }

    public function testGetIncomesInTable(): void
    {
        // normal - bg-danger
        $this->setUp();
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Scholarship</th>';
        $req .=         '<th>Work</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">12.5</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">12.5</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">12.5</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getIncomesInTable(), $req);

        // normal - bg-success
        $this->setUp();
        $this->persistenceMock->setIncomeAmount(50);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Scholarship</th>';
        $req .=         '<th>Work</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">50</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">50</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">50</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getIncomesInTable(), $req);

        // normal - bg-warning
        $this->setUp();
        $this->persistenceMock->setIncomeAmount(30);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Scholarship</th>';
        $req .=         '<th>Work</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">30</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getIncomesInTable(), $req);

        // normal - bg-success 2
        $this->setUp();
        $this->persistenceMock->setIncomeAmount(30);
        $this->persistenceMock->setPlanAmount(0);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Scholarship</th>';
        $req .=         '<th>Work</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">30</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getIncomesInTable(), $req);
    }

    public function testGetExpendituresInTable(): void
    {
        // normal - bg-danger
        $this->setUp();
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Food</th>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Taxes</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">120</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">120</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">120</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getExpendituresInTable(), $req);

        // normal - bg-success
        $this->setUp();
        $this->persistenceMock->setExpenditureAmount(10);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Food</th>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Taxes</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">10</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">10</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-success-light text-dark">';
        $req .=             '<span title="Actual value">10</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getExpendituresInTable(), $req);

        // normal - bg-warning
        $this->setUp();
        $this->persistenceMock->setExpenditureAmount(45);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Food</th>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Taxes</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">45</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">45</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-warning text-dark">';
        $req .=             '<span title="Actual value">45</span><span title="slash"> / </span><span title="Planned value">50.5</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getExpendituresInTable(), $req);

        // normal - bg-danger 2
        $this->setUp();
        $this->persistenceMock->setExpenditureAmount(30);
        $this->persistenceMock->setPlanAmount(0);
        $req = "";
        $req .= '<table class="table table-dark table-striped  table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Food</th>';
        $req .=         '<th>Other</th>';
        $req .=         '<th>Taxes</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">30</span>';
        $req .=             '<span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=         '<td class="bg-danger-light text-dark">';
        $req .=             '<span title="Actual value">30</span><span title="slash"> / </span><span title="Planned value">0</span> ';
        $req .=             '<span title="currency">EUR</span>';
        $req .=         '</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getExpendituresInTable(), $req);
    }

    public function testGetMothlyBalance(): void
    {
        // normal
        $this->setUp();
        // 5 categories * (12.50 - 120) = -537.5
        assertEquals($this->vm->getMothlyBalance(), "EUR: -537.5 | HUF: -537.5");
    }

    public function testGetHistoryTable(): void
    {
        // normal
        $this->setUp();
        $req = "";
        $req .= '<table class="table table-dark table-striped table-hover">';
        $req .=     '<tr>';
        $req .=         '<th>Date</th> <th>Time</th> <th>Category</th> <th>Amount</th> <th>Comment</th>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td>2021-06-30</td>';
        $req .=         '<td>12:15</td>';
        $req .=         '<td>Other</td>';
        $req .=         '<td>+12.5 EUR</td>';
        $req .=         '<td>-</td>';
        $req .=     '</tr>';
        $req .=     '<tr>';
        $req .=         '<td>2021-07-30</td>';
        $req .=         '<td>12:16</td>';
        $req .=         '<td>Food</td>';
        $req .=         '<td>-120 EUR</td>';
        $req .=         '<td>-</td>';
        $req .=     '</tr>';
        $req .= '</table>';
        assertEquals($this->vm->getHistoryTable(), $req);
    }

    public function testSuccessMessage(): void
    {
        // normal - success set
        $this->setUp();
        $_GET['success'] = '1';
        $req = '<h3> &#9989; You have successfully registered your transaction!</h3>';
        assertEquals($this->vm->successMessage(), $req);

        // normal - success not set
        $this->setUp();
        unset($_GET['success']);
        assertEquals($this->vm->successMessage(), '');
    }

    public function testGetBalance(): void
    {
        // normal
        $this->setUp();
        // (12.50 - 120) = -107.5
        assertEquals($this->vm->getBalance(), "EUR: -107.5 | HUF: 0");
    }
}
