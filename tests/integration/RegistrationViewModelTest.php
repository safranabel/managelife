<?php

declare(strict_types=1);

namespace Managelife\Registration;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Registration\RegistrationModel;
use Managelife\Registration\RegistrationViewModel;

/**
 * @covers \Managelife\Registration\RegistrationViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\Registration\RegistrationModel
 */
final class RegistrationViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private RegistrationModel $model;
    private RegistrationViewModel $vm;

    public function setUp(): void
    {
        $this->model = new RegistrationModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->vm = new RegistrationViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testUsernameIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertFalse($this->vm->usernameIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'testUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->vm->usernameIncorrect());
    }

    public function testEmailIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertFalse($this->vm->emailIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'notanemail';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->vm->emailIncorrect());
    }

    public function testPasswordIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertFalse($this->vm->passwordIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass';
        $_POST['passagain'] = 'testPass';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->vm->passwordIncorrect());
    }

    public function testPassAgainIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertFalse($this->vm->passAgainIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass12';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertTrue($this->vm->passAgainIncorrect());
    }

    public function testRegKeyIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'ThKww367lhTZ4gB3';

        $this->model->validate();
        assertFalse($this->vm->regKeyIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'newtestUser_id';
        $_POST['email'] = 'newtest@test.com';
        $_POST['forename'] = 'Testforename';
        $_POST['surname'] = 'Testsurname';
        $_POST['password'] = 'testPass123';
        $_POST['passagain'] = 'testPass123';
        $_POST['regkey'] = 'incorrect';

        $this->model->validate();
        assertTrue($this->vm->regKeyIncorrect());
    }
}
