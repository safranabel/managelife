<?php

declare(strict_types=1);

namespace Managelife\ChangePass;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\PassReset\PassResetModel;
use Managelife\ChangePass\ChangePassViewModel;

/**
 * @covers \Managelife\ChangePass\ChangePassViewModel
 * @uses \Managelife\PassReset\PassResetModel
 */
final class ChangePassViewModelTest extends TestCase
{
    private ChangePassViewModel $vm;

    public function setUp(): void
    {
        $this->vm = new ChangePassViewModel();
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetMessage(): void
    {
        // normal - success
        $this->setUp();
        unset($_GET['errors']);
        $_GET['success'] = '1';
        assertEquals($this->vm->getMessage(), '<p>&#9989; We have successfully changed your password. </p> ');

        // normal - PasswordError
        $this->setUp();
        $_GET['errors'] = 'PasswordError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Your given password is incorrect(password should contain at least one number, one letter and 6 characters)</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - PassAgainError
        $this->setUp();
        $_GET['errors'] = 'PassAgainError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>The two passwords are not the same.</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - OldPassError
        $this->setUp();
        $_GET['errors'] = 'OldPassError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Your given old password is not correct.</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - unknown error
        $this->setUp();
        $_GET['errors'] = 'unknown';
        unset($_GET['success']);
        $req = '<ul class="error"><li>Some error occurred, please try again</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // normal - combined
        $this->setUp();
        $_GET['errors'] = 'PassAgainError,OldPassError';
        unset($_GET['success']);
        $req = '<ul class="error"><li>The two passwords are not the same.</li>';
        $req .= '<li>Your given old password is not correct.</li></ul>';
        assertEquals($this->vm->getMessage(), $req);

        // no data in get
        $this->setUp();
        $_GET = [];
        assertEquals($this->vm->getMessage(), '');
    }
}
