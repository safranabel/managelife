<?php

declare(strict_types=1);

namespace Managelife\Summary;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Summaries\SummaryModel;
use Managelife\Summaries\SummaryViewModel;
use Managelife\UserCheck\UserCheckModel;

/**
 * @covers \Managelife\Summaries\SummaryViewModel
 * @uses \Managelife\Validator\Validator
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\UserCheck\UserCheckModel
 * @uses \Managelife\UserIndex\UserViewModel
 * @uses \Managelife\UserIndex\UserModel
 * @uses \Managelife\Summaries\SummaryModel
 * @uses \Managelife\Planning\PlanningViewModel
 * @uses \Managelife\Planning\PlanningModel
 */
final class SummaryViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private UserCheckModel $userCheck;
    private SummaryModel $model;
    private SummaryViewModel $vm;

    public function setUp(): void
    {
        $this->userCheck = new UserCheckModel();
        $this->model = new SummaryModel($this->userCheck);
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->userCheck->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->userCheck->refreshUserData();
        $this->vm = new SummaryViewModel($this->model, $this->userCheck);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testGetNextYear(): void
    {
        // normal
        $this->setUp();
        $_GET['year'] = '2022';
        assertEquals($this->vm->getNextYear(), "2022");

        // earlier
        $this->setUp();
        $_GET['year'] = '1990';
        $_GET['month'] = '12';
        assertEquals($this->vm->getNextYear(), "1991");

        // Actual date
        $this->setUp();
        $_GET['year'] = strval(date('Y'));
        $_GET['month'] = strval(date('m'));
        assertEquals($this->vm->getNextYear(), strval(date('Y')));
    }

    public function testGetNextMonth(): void
    {
        // February
        $this->setUp();
        $_GET['year'] = '2022';
        $_GET['month'] = '2';
        assertEquals($this->vm->getNextMonth(), "03");

        // December
        $this->setUp();
        $_GET['year'] = '1990';
        $_GET['month'] = '12';
        assertEquals($this->vm->getNextMonth(), "01");

        // Actual date
        $this->setUp();
        $_GET['year'] = strval(date('Y'));
        $_GET['month'] = strval(date('m'));
        assertEquals($this->vm->getNextMonth(), strval(date('m')));

        // Next year
        $this->setUp();
        $_GET['year'] = strval(intval(date('Y')) + 1);
        $_GET['month'] = strval(date('m'));
        assertEquals($this->vm->getNextMonth(), strval(date('m')));
    }

    public function testGetMothlyBalance(): void
    {
        // February
        $this->setUp();
        $_GET['year'] = '2022';
        $_GET['month'] = '2';
        // 5 categories * (12.50 - 120) = -537.5
        assertEquals($this->vm->getMothlyBalance(), "EUR: -537.5 | HUF: -537.5");
    }
}
