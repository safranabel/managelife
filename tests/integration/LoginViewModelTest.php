<?php

declare(strict_types=1);

namespace Managelife\Login;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
use function PHPUnit\Framework\assertFalse;

require_once "./testmanagelifeloader.php";

use Managelife\Persistence\UserHandlerPersistenceMock;
use PHPUnit\Framework\TestCase;
use Managelife\Login\LoginModel;
use Managelife\Login\LoginViewModel;

/**
 * @covers \Managelife\Login\LoginViewModel
 * @uses \Managelife\Login\LoginModel
 */
final class LoginViewModelTest extends TestCase
{
    private UserHandlerPersistenceMock $persistenceMock;
    private LoginModel $model;
    private LoginViewModel $vm;

    public function setUp(): void
    {
        $this->model = new LoginModel();
        $this->persistenceMock = new UserHandlerPersistenceMock();
        $this->model->setPersistence($this->persistenceMock);
        $this->persistenceMock->setToDefault();
        $this->vm = new LoginViewModel($this->model);
        $_POST = [];
        $_GET = [];
        $_SESSION['username'] = 'testUser_id';
        $_SESSION['token'] = '9af0dfae4691096b834e56f7d615ebe9c51253d58ce4c1e7bda49ce65ebd54b7';
    }

    public function testUsernameOrEmailIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertFalse($this->vm->usernameOrEmailIncorrect());

        // incorrect - username
        $this->setUp();
        $_POST['userid'] = 'incorrect';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue($this->vm->usernameOrEmailIncorrect());

        // incorrect - email
        $this->setUp();
        $_POST['userid'] = 'incorrect@incorrect.com';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertTrue($this->vm->usernameOrEmailIncorrect());
    }

    public function testPasswordIncorrect(): void
    {
        // correct
        $this->setUp();
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'testPass123';

        $this->model->validate();
        assertFalse($this->vm->passwordIncorrect());

        // incorrect
        $this->setUp();
        $_POST['userid'] = 'testUser_id';
        $_POST['password'] = 'incorrect';

        $this->model->validate();
        assertTrue($this->vm->passwordIncorrect());
    }
}
